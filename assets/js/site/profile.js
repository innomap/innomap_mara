var formProfile = $('#form-profile');

$(function(){
	initAlert();
	initEditPassword();
	initImgFancybox();
	initValidator();

	if(formProfile.length > 0){
		initMaraCenterList();
	}
});

function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}
}

function initEditPassword(){
	formProfile.on('click','.btn-change-pass',function(e){
		e.preventDefault();
	    var elem = $(this);
	    var collapse = elem.parent().parent().parent().find('.collapse');
	    collapse.collapse('toggle');
	});
}

function initImgFancybox(){
	$('.fancyboxs').fancybox({
        padding: 0,
        openEffect: 'elastic',
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(50, 50, 50, 0.85)'
                }
            }
        }
    });
}

function initMaraCenterList(){	
	$.get(baseUrl+"accounts/get_mara_list",null, function(data){
		$("#mc-keyword").typeahead({ 
			source:data,
			autoSelect: true,
			items:15,
			afterSelect : function(item){
				$('#mc-id').val(item.id);
				formProfile.bootstrapValidator('revalidateField', "mara_center_keyword");
			}
		});
	},'json');
}

function initValidator(){
	formProfile.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			email: {
				validators: {
					notEmpty: {message: 'The Email is required'},
					emailAddress: {message: 'The value is not a valid email address'},
					remote: {
                        message: 'The email is not available',
                        url: baseUrl+'accounts/check_email_exist/',
                        data: function(validator) {
                            return {
                                email: validator.getFieldElements('email').val(),
                                id : userId
                            };
                        },
                        type: 'POST'
                    },
				}
			},
			password: {
				validators: {
					notEmpty: {message: 'The Password is required'}
				}
			},
			retype_password: {
				validators: {
					notEmpty: {message: 'The Re-type Password is required'},
					identical: {
	                    field: 'password',
	                    message: 'The Password and its re-type password are not the same'
	                }
				}
			},
			name: {
				validators: {
					notEmpty: {message: 'The Name is required'}
				}
			},
			mara_center_keyword: {
				validators: {
					notEmpty: {message: 'The Pusat Mara is required'},
					remote: {
                        message: 'The Pusat Mara is not available',
                        url: baseUrl+'accounts/check_mara_center_exist/',
                        data: function(validator) {
                            return {
                                keyword: validator.getFieldElements('mara_center_keyword').val()
                            };
                        },
                        type: 'POST'
                    },
				}
			},
			innovator_photo: {
				validators:{
					file : {
						extension: 'png,jpeg,jpg,gif,bmp',
                    	type: 'image/png,image/jpeg,image/gif,image/x-ms-bmp',
                    	message: 'Please choose image file'
					}
				}
			}
		}
	});
}
