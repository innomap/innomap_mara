var formRegister = $('#form-register'),
	formLogin = $('#form-login'),
	formForgotPassword = $("#form-forgot-password"),
	formCreatePassword = $("#form-create-password");

$(function(){
	initAlert();
	
	if(formRegister.length > 0){
		initMaraCenterList();
	}

	initValidator();
});
function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}
}
function initValidator(){
	//Registration Form
	formRegister.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			email: {
				validators: {
					notEmpty: {message: 'The Email is required'},
					emailAddress: {message: 'The value is not a valid email address'},
					remote: {
                        message: 'The email is not available',
                        url: baseUrl+'accounts/check_email_exist/',
                        data: function(validator) {
                            return {
                                email: validator.getFieldElements('email').val()
                            };
                        },
                        type: 'POST'
                    },
				}
			},
			password: {
				validators: {
					notEmpty: {message: 'The Password is required'}
				}
			},
			retype_password: {
				validators: {
					notEmpty: {message: 'The Re-type Password is required'},
					identical: {
	                    field: 'password',
	                    message: 'The Password and its re-type password are not the same'
	                }
				}
			},
			name: {
				validators: {
					notEmpty: {message: 'The Name is required'}
				}
			},
			mara_center_keyword: {
				validators: {
					notEmpty: {message: 'The Pusat Mara is required'},
					remote: {
                        message: 'The Pusat Mara is not available',
                        url: baseUrl+'accounts/check_mara_center_exist/',
                        data: function(validator) {
                            return {
                                keyword: validator.getFieldElements('mara_center_keyword').val()
                            };
                        },
                        type: 'POST'
                    },
				}
			},
			innovator_photo: {
				validators: {
					notEmpty: {message: 'The Photo is required'},
					file : {
						extension: 'png,jpeg,jpg,gif,bmp',
                    	type: 'image/png,image/jpeg,image/gif,image/x-ms-bmp',
                    	message: 'Please choose image file'
					}
				}
			}
		}
	});
	
	//Form Login
	formLogin.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			email: {
				validators: {
					notEmpty: {message: 'The Email is required'},
					emailAddress: {message: 'The value is not a valid email address'},
				}
			},
			password: {
				validators: {
					notEmpty: {message: 'The Password is required'}
				}
			},
		}
	});
	//Form Forgot Password
	formForgotPassword.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			email: {
				validators: {
					notEmpty: {message: 'The Email is required'},
					emailAddress: {message: 'The value is not a valid email address'},
				}
			}
		}
	});

	//Create Password Form
	formCreatePassword.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			password: {
				validators: {
					notEmpty: {message: 'The Password is required'}
				}
			},
			retype_password: {
				validators: {
					notEmpty: {message: 'The Re-type Password is required'},
					identical: {
	                    field: 'password',
	                    message: 'The Password and its re-type password are not the same'
	                }
				}
			},
		}
	});
}

function initMaraCenterList(){	
	$.get(baseUrl+"accounts/get_mara_list",null, function(data){
		$("#mc-keyword").typeahead({ 
			source:data,
			autoSelect: true,
			items : 15,
			afterSelect : function(item){
				$('#mc-id').val(item.id);
				formRegister.bootstrapValidator('revalidateField', "mara_center_keyword");
			}
		});
	},'json');
}