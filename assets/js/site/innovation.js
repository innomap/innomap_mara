var tableInnovation = $('#table-innovation'),
	formInnovation = $('#form-innovation'),
	formBusinessPlan = $('#form-business-plan');

$(function(){
	tableInnovation.dataTable();
	initImgFancybox();

	initAlert();
	initValidator();
	initDatepicker();
	initCategoryOpt();
	initAddPicture();
	initDeletePicture();
	initAddTeamMember();
	initDeleteTeamMember();

	initEdit();
	initView();
	initDelete();
	initDownloadPdf();

	initValidationStatus();
	initPopover();
	initBusinessPlan();
	initValidatorBusinessPlan();
	initViewBusinessPlan();
	initAddSupportingDoc();
	initDeleteSupportingDoc();
});

function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}

	if(alert_dialog != ''){
		bootbox.alert({
		   message: alert,
		   callback: function(){
		      window.location = alert_dialog;
		   }
		});
	}
}

function initValidator(){
	formInnovation.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		live:'disabled',
		fields: {
			innovation_name: {
				validators: {
					notEmpty: {message: 'The field is required'},
				}
			},
			inspiration: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			description: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			materials: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			how_to_use: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			special_achievement: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			created_date: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			discovered_date: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			selling_price: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			manufacturing_costs: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			'target[]': {
				validators: {
					choice: {
                        min: 1,
                        message: 'Please select at least 1 target'
                    }
				}
			},
			myipo_protection: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			has_validation: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
		}
	}).on('success.form.bv', function(e) {
        // Prevent form submission
        e.preventDefault();

        var $form        = $(e.target),
            validator    = $form.data('bootstrapValidator'),
            submitButton = validator.getSubmitButton();

        if(submitButton.context.name == "btn_send_for_approval"){
        	showSubmissionAlert();
        }else{
        	formInnovation.bootstrapValidator('defaultSubmit');
        }

    });

    $('#btn-save').on('click', function(){
		formInnovation.bootstrapValidator('enableFieldValidators', 'innovation_name', false);
		formInnovation.bootstrapValidator('enableFieldValidators', 'inspiration', false);
		formInnovation.bootstrapValidator('enableFieldValidators', 'description', false);
		formInnovation.bootstrapValidator('enableFieldValidators', 'created_date', false);
		formInnovation.bootstrapValidator('enableFieldValidators', 'discovered_date', false);
		formInnovation.bootstrapValidator('enableFieldValidators', 'has_validation', false);
		formInnovation.bootstrapValidator('enableFieldValidators', 'materials', false);
		formInnovation.bootstrapValidator('enableFieldValidators', 'how_to_use', false);
		formInnovation.bootstrapValidator('enableFieldValidators', 'special_achievement', false);
		formInnovation.bootstrapValidator('enableFieldValidators', 'selling_price', false);
		formInnovation.bootstrapValidator('enableFieldValidators', 'manufacturing_costs', false);
		formInnovation.bootstrapValidator('enableFieldValidators', 'target[]', false);
		formInnovation.bootstrapValidator('enableFieldValidators', 'myipo_protection', false);
	});
}

function showSubmissionAlert(){
	bootbox.dialog({
		message: lang_send_for_approval_confirm_message+" ?",
		title: lang_send_innovation_for_approval,
		onEscape: function(){},
		size: "large",
		buttons: {
			close: {
				label: lang_cancel,
				className: "btn-default flat",
				callback: function() {
					$(this).modal('hide');
					formInnovation.find('input[type=submit]').prop('disabled', false);
				}
			},
			danger: {
				label: lang_send_for_approval,
				className: "btn-success flat",
				callback: function() {
					formInnovation.bootstrapValidator('defaultSubmit');				}
			}
		}
	});
}

function initDatepicker(){
	$('.datepicker').datepicker({
		format: 'yyyy-mm-dd',
		autoClose : true,
		endDate: new Date()
	}).on('changeDate', function(e) {
		formInnovation.bootstrapValidator('revalidateField', e.currentTarget.name);
	});
}

function initCategoryOpt(){
	var category = $('.category-opt').val(),
		level = $('.category-produk-level-opt').val();

	toogleCategory(category, level, 0);
	
	formInnovation.on('change', '.category-produk-level-opt', function(){
		var level = $(this).val(),
			category = $('.category-opt').val();
		toogleCategory(category, level);
	});

	formInnovation.on('change', '.category-opt', function(){
		var category = $(this).val(),
			level = $('.category-produk-level-opt').val();
		toogleCategory(category, level);
	});
}

function toogleCategory(category, level){
	if(level == 0){
		$('.category-opt option:first').show();
		$('.category-opt').prop('disabled',false);
	}else{
		$('.category-opt option:last').prop('selected',true);
		$('.category-opt').prop('disabled',true);
	}
	
	category = $('.category-opt').val();
	if(level != 0){
		$('.field-instructor').show();
	}else{
		$('.field-instructor').hide();
	}

	formInnovation.find('input[name="category"]').val(category);

	if(category == 0){
		$('.category-service-area-opt').show();
		$('.category-produk-area-opt').hide();
	}else{
		$('.category-service-area-opt').hide();
		$('.category-produk-area-opt').show();
	}
}

function initAddPicture(){
	$('.add-picture').on('click', function(){
		var num = $(".picture-item").length;
		str = '<div class="col-md-11">'
		str += 		'<input type="file" class="form-control picture-item" name="picture_'+num+'">';
		str += '</div>';
		$(".picture-wrapper div.item").append(str);
	});
}

function initDeletePicture(){
	$('.delete-picture').on('click', function(){
		var currentId = $(this).data('id'),
			currentName = $(this).data('name');

		formInnovation.append('<input type="hidden" name="deleted_picture[]" value="'+currentId+'">');
		formInnovation.append('<input type="hidden" name="deleted_picture_name[]" value="'+currentName+'">');

		formBusinessPlan.append('<input type="hidden" name="deleted_picture[]" value="'+currentId+'">');
		formBusinessPlan.append('<input type="hidden" name="deleted_picture_name[]" value="'+currentName+'">');
		$(this).parent().remove();
	});
}

function initEdit(){
	tableInnovation.on('click','.btn-edit-innovation', function(e){
		currentId = $(this).data('id');
		window.location.href = baseUrl+'innovations/edit/'+currentId;
	});
}

function initDelete(){
	tableInnovation.on('click','.btn-delete-innovation', function(e){
		currentId = $(this).data('id');
		var name = $(this).data('name');
		bootbox.dialog({
			message: lang_delete_confirm_message+" <strong>"+name+"</strong> ?",
			title: lang_delete_innovation,
			onEscape: function(){},
			size: "small",
			buttons: {
				close: {
					label: lang_cancel,
					className: "btn-default flat",
					callback: function() {
						$(this).modal('hide');
					}
				},
				danger: {
					label: lang_delete,
					className: "btn-danger flat",
					callback: function() {
						window.location.href = baseUrl+'innovations/delete/'+currentId;
					}
				}
			}
		});
	});
}

function initImgFancybox(){
	$('.fancyboxs').fancybox({
        padding: 0,
        openEffect: 'elastic',
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(50, 50, 50, 0.85)'
                }
            }
        }
    });
}

function initView(){
	tableInnovation.on('click','.btn-view-innovation', function(e){
		currentId = $(this).data('id');
		window.location.href = baseUrl+'innovations/view/'+currentId;
	});

	if(typeof view_mode != 'undefined'){
		if(view_mode){
			disabledForm();
		}
	}
}

function disabledForm(){
	$('form').find("input[type=text]").prop('readonly', true);
	$('form').find("textarea, select, input[type=checkbox], input[type=radio]").prop('disabled', true);
	$('form').find("button, input[type=file], input[type=submit]").hide();
	$('.btn-download-wrap').hide();
	$('form').find('.popover-item').hide();
	$('form').find("button.btn-back").show();
}

function initAddTeamMember(){
	var item_num = formInnovation.find('.team-member-id').length;

	formInnovation.on('click','.add-team-member', function(){
		str =	'<input type="hidden" class="team-member-id" name="team_member_id[]" value="0">';
		str += '<div class="col-md-11 team-member-item">';
		str +=		'<input type="hidden" name="h_team_member_pic_'+item_num+'" value="">';
		str +=		'<input type="hidden" name="i_deleted_team_member_'+item_num+'">';
		str += 		'<div class="col-md-6">';
		str += 			'<input type="text" class="form-control" name="team_member_'+item_num+'" placeholder="Nama Ahli">';
		str +=		'</div>';
		str += 		'<div class="col-md-3">';
		str += 			'<input type="text" class="form-control" name="team_member_kp_'+item_num+'" placeholder="No KP">';
		str +=		'</div>';
		str += 		'<div class="col-md-3">';
		str += 			'<input type="text" class="form-control" name="team_member_telp_'+item_num+'" placeholder="No Telefon">';
		str +=		'</div>';
		str +=		'<div class="col-md-12 margin-tb-10">';
		str +=			'<div class="col-md-1">';
		str +=				'<label>Foto</label>';
		str +=			'</div>';
		str +=			'<div class="col-md-11 no-padding">';
		str +=				'<input type="file"class="form-control" name="team_member_pic_'+item_num+'">';
		str +=			'</div>';
		str +=		'</div>';
		str += '</div>';
		str += '<div class="col-md-1">';
		str +=		'<button type="button" class="btn btn-danger delete-team-member"><span class="fa fa-times"></span></button>';
		str += '</div>';
		$(".team-member-wrap").append(str);
	});
}

function initDeleteTeamMember(){
	formInnovation.on('click','.delete-team-member', function(){
		$(this).parent().prev('div').remove();
		$(this).parent().remove();
	});
}

function initValidationStatus(){
	var validationStatus = $('.has-validation').is(':checked');
	toogleValidationStatus(validationStatus);

	formInnovation.on('click','.has-validation', function(){
		toogleValidationStatus($(this).is(':checked'));
	});
}

function toogleValidationStatus(val){
	if(val){
		if(typeof view_mode != 'undefined'){
			if(view_mode){
				$('.validation-attachment').hide();
			}else{
				$('.validation-attachment').show();
			}
		}
	}else{
		$('.validation-attachment').hide();
	}
}

function initDownloadPdf(){
	tableInnovation.on('click','.btn-download-pdf', function(e){
		currentId = $(this).data('id');
		window.location.href = baseUrl+'innovations/generate_pdf/'+currentId;
	});
}

function initPopover(){
	$('.popover-item').popover({
		trigger:'hover',
		container:'body',
		placement: 'bottom'
	});
}

function initBusinessPlan(){
	tableInnovation.on('click','.btn-business-plan', function(e){
		currentId = $(this).data('id');
		window.location.href = baseUrl+'innovations/business_plan/'+currentId;
	});
}

function initValidatorBusinessPlan(){
	formBusinessPlan.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		live:'disabled',
		fields: {
			description: {
				validators: {
					notEmpty: {message: 'The field is required'},
				}
			},
			problem: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			target_user: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			target_buyer: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			manufacturing_cost: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			competitor: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			competitive_advantage: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			impact: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			business_capital: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
			source_of_income: {
				validators: {
					notEmpty: {message: 'The field is required'}
				}
			},
		}
	}).on('success.form.bv', function(e) {
        // Prevent form submission
        e.preventDefault();

        var $form        = $(e.target),
            validator    = $form.data('bootstrapValidator'),
            submitButton = validator.getSubmitButton();

        if(submitButton.context.name == "btn_send_for_approval"){
        	showSubmissionBusinessPlanAlert();
        }else{
        	formBusinessPlan.bootstrapValidator('defaultSubmit');
        }

    });

    $('#btn-save').on('click', function(){
		formBusinessPlan.bootstrapValidator('enableFieldValidators', 'description', false);
		formBusinessPlan.bootstrapValidator('enableFieldValidators', 'problem', false);
		formBusinessPlan.bootstrapValidator('enableFieldValidators', 'target_user', false);
		formBusinessPlan.bootstrapValidator('enableFieldValidators', 'target_buyer', false);
		formBusinessPlan.bootstrapValidator('enableFieldValidators', 'manufacturing_cost', false);
		formBusinessPlan.bootstrapValidator('enableFieldValidators', 'competitor', false);
		formBusinessPlan.bootstrapValidator('enableFieldValidators', 'competitive_advantage', false);
		formBusinessPlan.bootstrapValidator('enableFieldValidators', 'impact', false);
		formBusinessPlan.bootstrapValidator('enableFieldValidators', 'business_capital', false);
		formBusinessPlan.bootstrapValidator('enableFieldValidators', 'source_of_income', false);
	});
}

function showSubmissionBusinessPlanAlert(){
	bootbox.dialog({
		message: lang_send_for_approval_confirm_message+" ?",
		title: lang_send_innovation_for_approval,
		onEscape: function(){},
		size: "large",
		buttons: {
			close: {
				label: lang_cancel,
				className: "btn-default flat",
				callback: function() {
					$(this).modal('hide');
					formBusinessPlan.find('input[type=submit]').prop('disabled', false);
				}
			},
			danger: {
				label: lang_send_for_approval,
				className: "btn-success flat",
				callback: function() {
					formBusinessPlan.bootstrapValidator('defaultSubmit');				}
			}
		}
	});
}

function initViewBusinessPlan(){
	tableInnovation.on('click','.btn-view-business-plan', function(e){
		currentId = $(this).data('id');
		window.location.href = baseUrl+'innovations/view_business_plan/'+currentId;
	});

	if(typeof view_mode != 'undefined'){
		if(view_mode){
			disabledForm();
		}
	}
}

function initAddSupportingDoc(){
	$('.add-doc').on('click', function(){
		var num = $(".doc-item").length;
		str = '<div class="col-md-11">'
		str += 		'<input type="file" class="form-control doc-item" name="doc_'+num+'">';
		str += '</div>';
		$(".doc-wrapper div.item").append(str);
	});
}

function initDeleteSupportingDoc(){
	formBusinessPlan.on('click','.delete-doc', function(){
		var currentId = $(this).data('id'),
			currentName = $(this).data('name');

		formBusinessPlan.append('<input type="hidden" name="deleted_doc[]" value="'+currentId+'">');
		formBusinessPlan.append('<input type="hidden" name="deleted_doc_name[]" value="'+currentName+'">');
		$(this).parent().remove();
	});
}