var tableGroup = $('#table-group'),
	formGroup = $('#form-group');

$(function(){
	tableGroup.dataTable();

	initAlert();
	initValidator();
	initAdd();
	initEdit();
	initDelete();
});
function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}
}

function initAdd(){
	$('.btn-add-group').on('click', function(e){
		window.location.href = adminUrl+'group_experts/add';
	});

	formGroup.on('click','.btn-cancel', function(){
		window.location.href = adminUrl+'group_experts';
	});
}

function initValidator(){
	formGroup.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			name:{
				validators: {
					notEmpty: {message: 'The Name is required'},
				}
			},
			description:{
				validators: {
					notEmpty: {message: 'The Description is required'},
				}
			},
			'evaluator_ids[]': {
				validators: {
					choice: {
                        min: 1,
                        message: 'Please select at least 1 evaluator'
                    }
				}
			},
		}
	});
}

function initEdit(){
	$('.btn-edit-group').on('click', function(e){
		var currentId = $(this).data('id');
		window.location.href = adminUrl+'group_experts/edit/'+currentId;
	});
}

function initDelete(){
	tableGroup.on('click','.btn-delete-group', function(e){
		currentId = $(this).data('id');
		var name = $(this).data('name');
		bootbox.dialog({
			message: lang_delete_confirm_message+" <strong>"+name+"</strong> ?",
			title: lang_delete_group,
			onEscape: function(){},
			size: "small",
			buttons: {
				close: {
					label: lang_cancel,
					className: "btn-default flat",
					callback: function() {
						$(this).modal('hide');
					}
				},
				danger: {
					label: lang_delete,
					className: "btn-danger flat",
					callback: function() {
						window.location.href = adminUrl+'group_experts/delete/'+currentId;
					}
				}
			}
		});
	});
}