var tableBusinessPlan = $('#table-business-plan'),
	formBusinessPlan = $('#form-business-plan'),
	modalEval = $('#modal-bp-evaluation'),
	formEval = $('#form-bp-evaluation');

var scoreValidator = {
						validators: {
							notEmpty: {message: 'The Score is required'},
							numeric: {message: 'The value is not a number'},
						}
					};

$(function(){
	initDataTableAndFilter();
	initAlert();
	initView();
	initImgFancybox();
	initDownloadPdf();
	initEvaluation();
	initAddScore();
	initDeleteScore();
});

function initDataTableAndFilter(){
	var dt = tableBusinessPlan.dataTable({
		"order": [[ 0, "asc" ]]
	});
	$('#dt-filter-status').on('change', function(){
		var opt = $(this).val();
		if(opt == "All"){
			dt.fnFilterClear();
		}else{
			dt.fnFilter(opt, 5, true, false );
		}
	});
}

function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}
}

function initView(){
	tableBusinessPlan.on('click','.btn-view', function(e){
		currentId = $(this).data('id');
		window.location.href = adminUrl+'business_plans/view/'+currentId;
	});

	formBusinessPlan.on('click','.btn-cancel', function(){
		window.location.href = adminUrl+'business_plans';
	});

	if(typeof view_mode != 'undefined'){
		if(view_mode){
			disabledForm();
		}
	}
}

function disabledForm(){
	$('form').find("input[type=text]").prop('readonly', true);
	$('form').find("textarea, select, input[type=checkbox], input[type=radio]").prop('disabled', true);
	$('form').find("button, input[type=file], input[type=submit]").hide();
	$('.btn-download-wrap').hide();
	$('form').find('.popover-item').hide();
	$('form').find("button.btn-back").show();
}

function initDownloadPdf(){
	tableBusinessPlan.on('click','.btn-download-pdf', function(e){
		currentId = $(this).data('id');
		window.location.href = adminUrl+'business_plans/generate_business_plan_pdf/'+currentId;
	});
}

function initImgFancybox(){
	$('.fancyboxs').fancybox({
        padding: 0,
        openEffect: 'elastic',
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(50, 50, 50, 0.85)'
                }
            }
        }
    });
}

function initEvaluation(){
	tableBusinessPlan.on('click','.btn-bp-evaluation', function(e){
		formEval.prop('action', adminUrl+'business_plans/evaluation_handler');
		var currentId = $(this).data('id'),
			currentTitle = $(this).data('title'),
			currentFirstScore = $(this).data('first-score');
		$.get(adminUrl+'business_plans/evaluation/'+currentId, function(res){
			if(res.status == 1){
				var data = res.data;
				formEval.find('input[name=id]').val(currentId);
				formEval.find('input[name=innovation_product]').val(currentTitle);
				formEval.find('input[name=innovation_id]').val(data.innovation_id);
				formEval.find('input[name=first_evaluation_score]').val(currentFirstScore);
				formEval.find('input[name=score_3]').val(data.score_3);
				formEval.find('input[name=score_4]').val(data.score_4);
				formEval.find('.eval-detail-wrap').html(data.score_by_individual);

				var str = "";
				for(i=0;i<data.evaluations.length;i++){
					str += generateScoreElem(i, data.evaluations[i]['id'], data.evaluations[i]['score'], data.evaluations[i]['attachment'], "");
				}

				if(str != ""){
					$(".score-wrap").html(str);
				}

				var str1 = "";
				for(i=0;i<data.evaluations1.length;i++){
					str1 += generateScoreElem(i, data.evaluations1[i]['id'], data.evaluations1[i]['score'], data.evaluations1[i]['attachment'], 1);
				}

				if(str1 != ""){
					$(".score1-wrap").html(str1);
				}

				var str2 = "";
				for(i=0;i<data.evaluations2.length;i++){
					str2 += generateScoreElem(i, data.evaluations2[i]['id'], data.evaluations2[i]['score'], data.evaluations2[i]['attachment'], 2);
				}

				if(str2 != ""){
					$(".score2-wrap").html(str2);
				}

				if(data.view_mode == 1){
					formEval.find("button[type=submit], button.add-score, button.add-score1, button.add-score2, button.delete-score, button.delete-score1, button.delete-score2, input[type=file]").hide();
					formEval.find("input[type=text]").prop('readonly', true);
				}

				modalEval.modal();
			}
		}, 'json');
	});
	modalEval.on('hidden.bs.modal', function(){
		formEval.attr('action', adminUrl+'business_plans/evaluation_handler')
					.find('input[name=id]').val('');
		formEval.bootstrapValidator('resetForm', true);
		formEval.get(0).reset();
		formEval.find('.attachment a').remove();
	});

	//validation
	formEval.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			score_0: scoreValidator,
			score1_0: scoreValidator,
			score2_0: scoreValidator
		}
	});
}

function initAddScore(){
	formEval.on('click','.add-score', function(e){
		e.preventDefault();
		var item_num = formEval.find('.score-id').length;
		var str = generateScoreElem(item_num, 0, "", "", "");
									
		$(".score-wrap").append(str);

		formEval.bootstrapValidator('addField', 'score_'+item_num, scoreValidator);
		item_num++;
	});

	formEval.on('click','.add-score1', function(e){
		e.preventDefault();
		var item_num = formEval.find('.score1-id').length;
		var str = generateScoreElem(item_num, 0, "", "", 1);
									
		$(".score1-wrap").append(str);

		formEval.bootstrapValidator('addField', 'score1_'+item_num, scoreValidator);
		item_num++;
	});

	formEval.on('click','.add-score2', function(e){
		e.preventDefault();
		var item_num = formEval.find('.score2-id').length;
		var str = generateScoreElem(item_num, 0, "", "", 2);
									
		$(".score2-wrap").append(str);

		formEval.bootstrapValidator('addField', 'score2_'+item_num, scoreValidator);
		item_num++;
	});
}

function initDeleteScore(){
	formEval.on('click','.delete-score', function(){
		var id = $(this).data('id');
		formEval.append("<input type='hidden' name='deleted_score[]' value='"+id+"'>")
		$(this).parent().parent().remove();
	});

	formEval.on('click','.delete-score1', function(){
		var id = $(this).data('id');
		formEval.append("<input type='hidden' name='deleted_score1[]' value='"+id+"'>")
		$(this).parent().parent().remove();
	});

	formEval.on('click','.delete-score2', function(){
		var id = $(this).data('id');
		formEval.append("<input type='hidden' name='deleted_score2[]' value='"+id+"'>")
		$(this).parent().parent().remove();
	});
}

function generateScoreElem(item_num, id, score, filename, score_name){
	str =	'<input type="hidden" class="score'+score_name+'-id" name="score'+score_name+'_id[]" value="'+id+'">';
	str += '<div class="form-group col-md-12 no-padding">';
	str += 		'<div class="col-md-4 no-padding">';
	str +=			'<input type="text" name="score'+score_name+'_'+item_num+'" class="form-control" placeholder="Markah" value="'+score+'"/>';
	str +=		'</div>';
	str +=		'<div class="col-md-4">';
	str +=			'<input type="file" name="score'+score_name+'_attachment_'+item_num+'" class="form-control"/>';
	str +=		'</div>';
	if(filename != ""){
	str +=		'<div class="col-md-3">';
					var url = baseUrl+'assets/attachment/business_plan_evaluation'+score_name+'/'+filename;
	str +=			'<a href="'+url+'" target="_blank">Lihat Attachment</a>';
	str +=			'<input type="hidden" name="h'+score_name+'_attachment_'+item_num+'" value="'+filename+'" class="form-control"/>';
	str +=		'</div>';
	}
	if(item_num == 0){
	str += 		'<div class="col-md-1">';
	str +=			'<button type="button" class="btn btn-grey add-score'+score_name+'"><span class="fa fa-plus"></span></button>';
	str += 		'</div>';
	}else{
	str += 		'<div class="col-md-1">';
	str +=			'<button type="button" class="btn btn-danger delete-score'+score_name+'" data-id="'+id+'"><span class="fa fa-times"></span></button>';
	str += 		'</div>';	
	}
	str +=	'</div>';

	return str;
}