var tableApplication = $('#table-application'),
	formApplication = $('#form-application'),
	formEvaluation = $('#form-evaluation'),
	formInnovation = $('#form-innovation'),
	modalAssign = $('#modal-assign'),
	formAssign = $('#form-assign'),
	tableEvaluationList = $('#table-evaluation-list'),
	formEvaluationList = $('#form-evaluation-list'),
	modalEval = $('#modal-bp-evaluation'),
	formEval = $('#form-bp-evaluation');

var scoreValidator = {
						validators: {
							notEmpty: {message: 'The Score is required'},
							numeric: {message: 'The value is not a number'},
						}
					};

$(function(){
	initDataTableAndFilter();
	initAlert();
	initImgFancybox();

	initApproveEvaluation();
	initViewEvaluation();
	initViewEvaluationList();
	initViewInnovation();
	initAssignInnovation();
	initDownloadPdf();
	initBusinessPlan();
	initAddScore();
	initDeleteScore();
});

function initDataTableAndFilter(){
	var dt = tableApplication.dataTable({
		"order": [[ 1, "asc" ]]
	});
	$('#dt-filter-status').on('change', function(){
		var opt = $(this).val();
		if(opt == "All"){
			dt.fnFilterClear();
		}else{
			dt.fnFilter(opt, 5, true, false );
		}
	});
}

function initAlert(){
	if(alert != ''){
		$('.alert-info').removeClass('hide').hide().fadeIn(500, function(){
			$(this).delay(3000).fadeOut(500);	
		})
	}
}

function initViewEvaluation(){
	tableEvaluationList.on('click','.btn-view-evaluation', function(e){
		currentId = $(this).data('id');
		window.location.href = adminUrl+'applications/view_evaluation/'+currentId;
	});
	
	if(typeof view_mode != 'undefined'){
		if(view_mode == 1){
			calculateTotal();
			formEvaluation.find("input[type=text]").prop('readonly', true);
			formEvaluation.find("textarea").prop('disabled', true);
			formEvaluation.find("button,input[type=submit]").hide();
			formEvaluation.find("button.btn-cancel").show();
			formEvaluation.find(".popover-item").hide();
			formEvaluation.find(".point-input").prop('disabled',true);

			formEvaluation.on('click','.btn-cancel', function(e){
				window.history.back();
			});
		}
	}
}

function initViewEvaluationList(){
	tableApplication.on('click','.btn-view-evaluation-list', function(e){
		currentId = $(this).data('id');
		window.open(adminUrl+'applications/view_evaluation_list/'+currentId,'_blank');
	});

	formEvaluationList.on('click','.btn-cancel', function(){
		window.location.href = adminUrl+'applications';
	});
}

function initApproveEvaluation(){
	formApplication.on('submit', function(e){
		var ids = formApplication.find('input[type=checkbox]:checked').length,
			content = formApplication.find('input[type=checkbox]:checked'),
			currentForm = this;

		var btnSubmit = $("input[type=submit][clicked=true]").data('id');

		if(ids == 0){
			e.preventDefault();
			bootbox.alert('Please select at least an item.');
			return false;
		}

		var evaluated = 1;
		$(content).each(function(i){
			if($(this).data('is-evaluated') == 0){
				evaluated = 0;
			}
		});

		var hasEvaluation = $(this).data('has-evaluation');
		if(btnSubmit == "btn-proceed"){
			$('input[name=action]').val('proceed');
			
			if(hasEvaluation == 0){
				e.preventDefault();
				bootbox.alert('Application not evaluated yet.');
				return false;
			}else{
				e.preventDefault();
				bootbox.dialog({
					message: "Are you sure want to proceed to next round?",
					title: "Proceed to Next",
					onEscape: function(){},
					size: "small",
					buttons: {
						close: {
							label: "Cancel",
							className: "btn-default flat",
							callback: function() {
								$(this).modal('hide');
							}
						},
						danger: {
							label: "Proceed",
							className: "btn-success flat",
							callback: function() {
								currentForm.submit();
							}
						}
					}
				});

				return false;
			}
		}else if(btnSubmit == "btn-assign"){
		}
	});

	formApplication.find("input[type=submit]").click(function() {
	    $("input[type=submit]", $(this).parents("form")).removeAttr("clicked");
	    $(this).attr("clicked", "true");
	});
}

function initViewInnovation(){
	tableApplication.on('click','.btn-view-innovation', function(e){
		currentId = $(this).data('id');
		window.location.href = adminUrl+'applications/view_innovation/'+currentId;
	});

	if(typeof view_mode != 'undefined'){
		if(view_mode){
			formInnovation.find("input[type=text]").prop('readonly', true);
			formInnovation.find("textarea, select, input[type=checkbox], input[type=radio]").prop('disabled', true);
			formInnovation.find("button, input[type=file], input[type=submit]").hide();
			$('.btn-download-wrap').hide();
			formInnovation.find('.popover-item').hide();
			formInnovation.find("button.btn-back").show();

			initCategoryOpt();
		}
	}
}

function initCategoryOpt(){
	var category = $('.category-opt').val(),
		level = $('.category-produk-level-opt').val();

	toogleCategory(category, level);
	
	formInnovation.on('change', '.category-produk-level-opt', function(){
		var level = $(this).val(),
			category = $('.category-opt').val();
		toogleCategory(category, level);
	});

	formInnovation.on('change', '.category-opt', function(){
		var category = $(this).val(),
			level = $('.category-produk-level-opt').val();
		toogleCategory(category, level);
	});
}

function toogleCategory(category, level){
	if(level == 0){
		$('.category-opt option:first').show();
		$('.category-opt').prop('disabled',true);
	}else{
		$('.category-opt option:last').prop('selected',true);
		$('.category-opt').prop('disabled',true);
	}

	category = $('.category-opt').val();
	if(level != 0){
		$('.field-instructor').show();
	}else{
		$('.field-instructor').hide();
	}

	formInnovation.find('input[name="category"]').val(category);

	if(category == 0){
		$('.category-service-area-opt').show();
		$('.category-produk-area-opt').hide();
	}else{
		$('.category-service-area-opt').hide();
		$('.category-produk-area-opt').show();
	}
}

function initImgFancybox(){
	$('.fancyboxs').fancybox({
        padding: 0,
        openEffect: 'elastic',
        helpers: {
            overlay: {
                css: {
                    'background': 'rgba(50, 50, 50, 0.85)'
                }
            }
        }
    });
}

function initAssignInnovation(){	
	tableApplication.on('click','.btn-assign-innovation', function(){
		var currentId = $(this).data('id');
		modalAssign.find('input[name=innovation_id]').val(currentId);
		modalAssign.modal();

		modalAssign.on('hidden.bs.modal', function(){
			formAssign.bootstrapValidator('resetForm', true);
			formAssign.get(0).reset();
		});
	});
}

function check_value(input){
	if(isNaN(parseInt(input))){
		return 0;
	}else{
		return parseInt(input);
	}
}

function calculateTotal(){
	var sumTotal = 0;
	$('.focus-item').each(function(i){
		var percentage = $(this).data('percentage'),
			id = $(this).data('id'),
			total = 0, maxPoint = 0;

		$('.input-'+id+':checked').each(function(j){
			maxPoint += check_value($(this).data('maxpoint'));
		});

		$('.input-'+id+':checked').each(function(j){
			var point = check_value($(this).val());
			total += point;
		});

		var calTotal = total/maxPoint*percentage;
		$('#total-'+id).val(calTotal.toFixed(1));
		sumTotal += calTotal;
	});

	$('#total').val(sumTotal.toFixed(1));
	$('input[name=sum_total]').val(sumTotal.toFixed(1));
}

function initDownloadPdf(){
	tableApplication.on('click','.btn-download-pdf', function(e){
		currentId = $(this).data('id');
		window.location.href = adminUrl+'applications/generate_pdf/'+currentId;
	});
}

function initBusinessPlan(){
	tableApplication.on('click','.btn-business-plan-eval', function(e){
		currentId = $(this).data('id');
		formEval.prop('action', adminUrl+'business_plans/evaluation_handler');
		var currentId = $(this).data('id'),
			currentTitle = $(this).data('title');
		$.get(adminUrl+'business_plans/evaluation/'+currentId+'/1', function(res){
			if(res.status == 1){
				var data = res.data;
				formEval.find('input[name=id]').val(data.id);
				formEval.find('input[name=innovation_product]').val(currentTitle);
				formEval.find('input[name=innovation_id]').val(currentId);

				var str = "";
				for(i=0;i<data.evaluations.length;i++){
					str += generateScoreElem(i, data.evaluations[i]['id'], data.evaluations[i]['score'], data.evaluations[i]['attachment']);
				}

				if(str != ""){
					$(".score-wrap").html(str);
				}

				if(data.view_mode == 1){
					formEval.find("button[type=submit], button.add-score, button.delete-score, input[type=file]").hide();
					formEval.find("input[type=text]").prop('readonly', true);
				}

				modalEval.modal();
			}
		}, 'json');
	});
	modalEval.on('hidden.bs.modal', function(){
		formEval.attr('action', adminUrl+'business_plans/evaluation_handler')
					.find('input[name=id]').val('');
		formEval.bootstrapValidator('resetForm', true);
		formEval.get(0).reset();
		formEval.find('.attachment a').remove();
	});

	//validation
	formEval.bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			score_0: scoreValidator
		}
	});
}

function initAddScore(){
	formEval.on('click','.add-score', function(e){
		e.preventDefault();
		var item_num = formEval.find('.score-id').length;
		var str = generateScoreElem(item_num, 0, "", "");
									
		$(".score-wrap").append(str);

		formEval.bootstrapValidator('addField', 'score_'+item_num, scoreValidator);
		item_num++;
	});
}

function initDeleteScore(){
	formEval.on('click','.delete-score', function(){
		var id = $(this).data('id');
		formEval.append("<input type='hidden' name='deleted_score[]' value='"+id+"'>")
		$(this).parent().parent().remove();
	});
}

function generateScoreElem(item_num, id, score, filename){
	str =	'<input type="hidden" class="score-id" name="score_id[]" value="'+id+'">';
	str += '<div class="form-group col-md-12 no-padding">';
	str += 		'<div class="col-md-4 no-padding">';
	str +=			'<input type="text" name="score_'+item_num+'" class="form-control" placeholder="Markah" value="'+score+'"/>';
	str +=		'</div>';
	str +=		'<div class="col-md-4">';
	str +=			'<input type="file" name="score_attachment_'+item_num+'" class="form-control"/>';
	str +=		'</div>';
	if(filename != ""){
	str +=		'<div class="col-md-3">';
					var url = baseUrl+'assets/attachment/business_plan_evaluation/'+filename;
	str +=			'<a href="'+url+'" target="_blank">Lihat Attachment</a>';
	str +=			'<input type="hidden" name="h_attachment_'+item_num+'" value="'+filename+'" class="form-control"/>';
	str +=		'</div>';
	}
	if(item_num == 0){
	str += 		'<div class="col-md-1">';
	str +=			'<button type="button" class="btn btn-grey add-score"><span class="fa fa-plus"></span></button>';
	str += 		'</div>';
	}else{
	str += 		'<div class="col-md-1">';
	str +=			'<button type="button" class="btn btn-danger delete-score" data-id="'+id+'"><span class="fa fa-times"></span></button>';
	str += 		'</div>';	
	}
	str +=	'</div>';

	return str;
}