-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 05, 2016 at 05:38 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `innomap_v2_mara`
--

-- --------------------------------------------------------

--
-- Table structure for table `mara_innovation_business_plan`
--

CREATE TABLE IF NOT EXISTS `mara_innovation_business_plan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `innovation_id` int(11) NOT NULL,
  `description` mediumtext NOT NULL,
  `problem` mediumtext NOT NULL,
  `target_user` mediumtext NOT NULL,
  `target_buyer` mediumtext NOT NULL,
  `manufacturing_cost` mediumtext NOT NULL,
  `competitor` mediumtext NOT NULL,
  `competitive_advantage` mediumtext NOT NULL,
  `impact` mediumtext NOT NULL,
  `business_capital` mediumtext NOT NULL,
  `source_of_income` mediumtext NOT NULL,
  `url` mediumtext NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `innovation_id` (`innovation_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `mara_innovation_business_plan`
--
ALTER TABLE `mara_innovation_business_plan`
  ADD CONSTRAINT `mara_innovation_business_plan_ibfk_1` FOREIGN KEY (`innovation_id`) REFERENCES `innovation` (`innovation_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
