-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 10, 2016 at 04:13 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `innomap_v2_mara`
--

-- --------------------------------------------------------

--
-- Table structure for table `mara_evaluation_form`
--

CREATE TABLE IF NOT EXISTS `mara_evaluation_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` int(11) NOT NULL,
  `subcategory` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `mara_evaluation_form`
--

INSERT INTO `mara_evaluation_form` (`id`, `category`, `subcategory`) VALUES
(1, 0, 0),
(4, 0, 1),
(7, 0, 2),
(8, 1, 0),
(9, 1, 1),
(10, 1, 2);

DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `calculate_top_jooters_event` ON SCHEDULE EVERY 1 DAY STARTS '2013-04-10 00:00:01' ON COMPLETION NOT PRESERVE ENABLE DO BEGIN
						DECLARE done INT DEFAULT FALSE;
						DECLARE a INT;
						DECLARE cur1 CURSOR FOR SELECT user_id FROM user WHERE DATE(last_activity) > date(subdate(current_date,2));
						DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

						OPEN cur1;
						
						
						UPDATE user_interaction_point 
						SET day_7=day_6, day_6=day_5,day_5=day_4,day_4= day_3,
						day_3=day_2,day_2=day_1,day_1=0 WHERE 1;
	  
						read_loop: LOOP
							FETCH cur1 INTO a;
							IF done THEN
								LEAVE read_loop;
							END IF;
							
							
							UPDATE user_interaction_point
								SET user_interaction_point.day_1 = 
									(SELECT 
									((SELECT 
										COUNT(card.card_id) AS num_cards
										FROM card
										WHERE card.creator = a AND DATE(card.created_date) = date(subdate(current_date,1)) AND card.is_rehosted = 0) + 
									(SELECT 
										COUNT(card_view.card_id) AS num_views
										FROM card_view
										WHERE card_view.user_id = a AND DATE(card_view.view_time) = date(subdate(current_date,1)))
									+
									(SELECT 
										COUNT(conversation.card_id) AS num_comments
										FROM conversation
										WHERE conversation.user_id = a AND DATE(conversation.timestamp) = date(subdate(current_date,1)))
									+
									(SELECT 
										COUNT(card_follower.card_id) AS num_follows
										FROM card_follower
										WHERE card_follower.user_id = a AND DATE(card_follower.following_date) =date(subdate(current_date,1)))
									+
									(SELECT 
										COUNT(card_marking.card_id) AS num_likes
										FROM card_marking
										WHERE card_marking.user_id = a AND DATE(card_marking.timestamp) =date(subdate(current_date,1)) AND card_marking.type = " . CARD_MARK_LIKE . ")
									+
									(SELECT 
										COUNT(card.card_id) AS num_rehosts
										FROM card
										WHERE card.creator = a AND DATE(card.created_date) =date(subdate(current_date,1)) AND card.is_rehosted = 1)) 
									AS interaction_point),
									avg_interaction_point = 
									(((day_1 * pow(0.5,1/3)) + (day_2 * pow(0.5,2/3)) + 
									(day_3 * pow(0.5,1)) + (day_4 * pow(0.5,4/3)) + 
									(day_5 * pow(0.5,5/3)) + (day_6 * pow(0.5,2)) + 
									(day_7 * pow(0.5,7/3))) / 7)
								WHERE user_interaction_point.user_id = a; 
						END LOOP;

						CLOSE cur1;
					END$$

CREATE DEFINER=`root`@`localhost` EVENT `top_jooters_half_life` ON SCHEDULE EVERY 1 DAY STARTS '2013-06-11 00:00:01' ON COMPLETION NOT PRESERVE ENABLE DO BEGIN
						DECLARE done INT DEFAULT FALSE;
						DECLARE a, b, i INT;
						DECLARE total, avg DOUBLE;
						DECLARE ip TEXT;
						DECLARE cur1 CURSOR FOR SELECT user_id FROM user WHERE DATE(last_activity) > date(subdate(current_date,2));
						DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

						OPEN cur1;

						
						UPDATE user_interaction_point 
						SET interaction_points = CONCAT(0,';',SUBSTRING_INDEX(interaction_points,';',6))
						WHERE 1;

						
						read_loop: LOOP
							FETCH cur1 INTO a;
							IF done THEN
								LEAVE read_loop;
							END IF;
							
							SELECT 
								(1 * (SELECT 
									COUNT(card.card_id) AS num_cards
									FROM card
									WHERE card.creator = a AND DATE(card.created_date) = date(subdate(current_date,1)) AND card.is_rehosted = 0) 
								+ 
								1 * (SELECT 
									COUNT(card_view.card_id) AS num_views
									FROM card_view
									WHERE card_view.user_id = a AND DATE(card_view.view_time) = date(subdate(current_date,1)))
								+
								1 * (SELECT 
									COUNT(conversation.card_id) AS num_comments
									FROM conversation
									WHERE conversation.user_id = a AND DATE(conversation.timestamp) = date(subdate(current_date,1)))
								+
								1 * (SELECT 
									COUNT(card_follower.card_id) AS num_follows
									FROM card_follower
									WHERE card_follower.user_id = a AND DATE(card_follower.following_date) =date(subdate(current_date,1)))
								+
								1 * (SELECT 
									COUNT(card_marking.card_id) AS num_likes
									FROM card_marking
									WHERE card_marking.user_id = a AND DATE(card_marking.timestamp) =date(subdate(current_date,1)) AND card_marking.type = 0)
								+
								1 * (SELECT 
									COUNT(card.card_id) AS num_rehosts
									FROM card
									WHERE card.creator = a AND DATE(card.created_date) =date(subdate(current_date,1)) AND card.is_rehosted > 0)) INTO b;
							SELECT interaction_points INTO ip FROM user_interaction_point WHERE user_id = a;
							SET total = b * pow(0.5,1/3);
							SET i = 2;
							WHILE i <= 7 DO
								SET total = total + ((REPLACE(SUBSTRING(SUBSTRING_INDEX(ip, ';', i),
								LENGTH(SUBSTRING_INDEX(ip, ';', i -1)) + 1),
								';', '')) * pow(0.5,i/3));
								SET i = i + 1;
							END WHILE;
							SET avg = total / 7;
							UPDATE user_interaction_point
								SET interaction_points = CONCAT(b,';',SUBSTRING_INDEX(interaction_points,';',-6)),
									avg_interaction_point = avg
								WHERE user_interaction_point.user_id = a; 
						END LOOP;

						CLOSE cur1;
					END$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
