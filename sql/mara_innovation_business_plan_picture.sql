-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 05, 2016 at 05:38 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `innomap_v2_mara`
--

-- --------------------------------------------------------

--
-- Table structure for table `mara_innovation_business_plan_picture`
--

CREATE TABLE IF NOT EXISTS `mara_innovation_business_plan_picture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mara_innovation_business_plan_id` int(11) NOT NULL,
  `picture` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mara_business_plan_id` (`mara_innovation_business_plan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `mara_innovation_business_plan_picture`
--
ALTER TABLE `mara_innovation_business_plan_picture`
  ADD CONSTRAINT `mara_innovation_business_plan_picture_ibfk_1` FOREIGN KEY (`mara_innovation_business_plan_id`) REFERENCES `mara_innovation_business_plan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
