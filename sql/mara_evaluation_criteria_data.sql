-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 10, 2016 at 04:17 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `innomap_v2_mara`
--

--
-- Dumping data for table `mara_evaluation_criteria`
--

INSERT INTO `mara_evaluation_criteria` (`id`, `mara_evaluation_focus_id`, `question`, `description`, `max_point`) VALUES
(22, 1, 'Value added & Novelty', '-', 10),
(23, 7, 'Alignment to goal & philosophy', '-', 10),
(24, 8, 'High Impact', '-', 10),
(25, 9, 'Productivity Cost Revenue', '-', 10),
(26, 6, 'How easy to replicate? Clarity of Strategic Plan', '-', 10),
(27, 10, 'Addressable Market', '-', 10),
(28, 11, 'Size of impact to rural community', '-', 10),
(29, 12, 'Can Implement', '-', 10),
(30, 13, 'Theoritical Research Model, Clarity of Plan', '-', 10),
(31, 14, 'Alignment ti Institutional Goal', '--', 10),
(32, 15, 'Impact to targeted Group', '-', 10),
(33, 16, 'Teaching effectively Learning Productivity', '-', 10),
(34, 17, 'Novelty of work, Poc', '-', 10),
(35, 18, 'Quality of work, price', '-', 10),
(36, 19, 'Size of addressable market', '-', 20),
(37, 20, 'Productivity,Cost,Revenue', '-', 10),
(38, 21, 'Novelty of work, Poc', '-', 10),
(39, 22, 'Quality of work, price', '-', 10),
(40, 23, 'Size of addressable market', '-', 10),
(41, 24, 'Productivity,Cost,Revenue', '-', 10),
(42, 25, 'Novelty of work, Poc', '-', 10),
(43, 26, 'Quality of work, price', '-', 10),
(44, 27, 'Size of addressable market', '-', 10),
(45, 28, 'Productivity,Cost,Revenue', '-', 10);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
