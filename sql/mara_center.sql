-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 22, 2016 at 09:28 AM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `innomap_v2_mara`
--

-- --------------------------------------------------------

--
-- Table structure for table `mara_center`
--

CREATE TABLE IF NOT EXISTS `mara_center` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=512 ;

--
-- Dumping data for table `mara_center`
--

INSERT INTO `mara_center` (`id`, `name`) VALUES
(1, 'BAHAGIAN AUDIT DALAM  '),
(2, 'BAHAGIAN BINAAN DAN SELENGGARAAN  '),
(3, 'BAHAGIAN KEMAHIRAN DAN TEKNIKAL  '),
(4, 'BAHAGIAN PEMBANGUNAN USAHAWAN  '),
(5, 'BAHAGIAN PENDIDIKAN MENENGAH  '),
(6, 'BAHAGIAN PENDIDIKAN TINGGI  '),
(7, 'BAHAGIAN PENGURUSAN ASET DAN PEROLEHAN  '),
(8, 'BAHAGIAN PMBGNN INDUSTRI & INFRASTRUKTUR '),
(9, 'BAHAGIAN TEKNOLOGI MAKLUMAT  '),
(10, 'BAHAGIAN TEKNOLOGI PENDIDIKAN  '),
(11, 'BHG KAWALAN KREDIT  '),
(12, 'BHG KEWANGAN-CAW KEWANGAN & AKAUN  '),
(13, 'BHG KOMUNIKASI KORPORAT  '),
(14, 'BHG PELABURAN DAN PEMBANGUNAN SUBSIDIARI '),
(15, 'BHG PEMANTAUAN & INSPEKTORAT  '),
(16, 'BHG PEMB INFRASTRUKTUR PERNIAGAAN  '),
(17, 'BHG PEMBIAYAAN PERNIAGAAN  '),
(18, 'BHG PENGANJURAN PELAJARAN  '),
(19, 'BHG SUMBER MANUSIA  '),
(20, 'BHG UNDANG-UNDANG  '),
(21, 'BHGN DASAR & PERANCANGAN STRATEGIK  '),
(22, 'BHGN INDUSTRI PENGANGKUTAN  '),
(23, 'IKM ALOR SETAR, KEDAH  '),
(24, 'IKM BESERI, PERLIS  '),
(25, 'IKM BESUT, TERENGGANU  '),
(26, 'IKM BINTULU, SARAWAK  '),
(27, 'IKM JASIN, MELAKA  '),
(28, 'IKM JOHOR BAHRU, JOHOR  '),
(29, 'IKM KOTA KINABALU, SABAH  '),
(30, 'IKM KUALA LUMPUR, W.PERSEKUTUAN  '),
(31, 'IKM KUCHING, SARAWAK  '),
(32, 'IKM LUMUT, PERAK  '),
(33, 'IKM SIK, KEDAH  '),
(34, 'IKM SUNGAI PETANI, KEDAH  '),
(35, 'IKM TASYA PEKAN, PAHANG  '),
(36, 'INSTITUT KECERMELANGAN MARA (ILKM)  '),
(37, 'IPM W PERSEKUTUAN, KUALA LUMPUR  '),
(38, 'JAB.KETUA PENGARAH(TERMSK.U.PENYELARAS)  '),
(39, 'KKTM BALIK PULAU, PULAU PINANG  '),
(40, 'KKTM KEMAMAN, TERENGGANU  '),
(41, 'KKTM KUANTAN, PAHANG  '),
(42, 'KKTM LEDANG  '),
(43, 'KKTM LENGGONG, PERAK  '),
(44, 'KKTM MASJID TANAH,MELAKA  '),
(45, 'KKTM PASIR MAS, KELANTAN  '),
(46, 'KKTM PETALING JAYA, SELANGOR  '),
(47, 'KKTM REMBAU, N. SEMBILAN  '),
(48, 'KKTM SRI GADING, JOHOR  '),
(49, 'KOLEJ ANTARABANGSA MARA, KETENGAH  '),
(50, 'KOLEJ MARA BANTING, SELANGOR  '),
(51, 'KOLEJ MARA KUALA NERANG, KEDAH  '),
(52, 'KOLEJ MARA KULIM, KEDAH  '),
(53, 'KOLEJ MARA SEREMBAN, N. SEMBILAN  '),
(54, 'KOLEJ PROF.MARA BANDAR PENAWAR, JOHOR  '),
(55, 'KOLEJ PROF.MARA INDERA MAHKOTA, PAHANG  '),
(56, 'KOLEJ PROFESIONAL MARA AYER MOLEK,MELAKA '),
(57, 'KOLEJ PROFESIONAL MARA BANDAR MELAKA  '),
(58, 'KOLEJ PROFESIONAL MARA BERANANG,SELANGOR '),
(59, 'KOLEJ PROFESIONAL MARA SRI ISKANDAR  '),
(60, 'MARA JAPAN INDUSTRIAL INSTITUTE (MJII)  '),
(61, 'MRSM ALOR GAJAH, MELAKA  '),
(62, 'MRSM ARAU, PERLIS  '),
(63, 'MRSM BALIK PULAU, PULAU PINANG  '),
(64, 'MRSM BALING, KEDAH  '),
(65, 'MRSM BATU PAHAT, JOHOR  '),
(66, 'MRSM BENTONG, PAHANG  '),
(67, 'MRSM BESERI, PERLIS  '),
(68, 'MRSM BESUT, TERENGGANU  '),
(69, 'MRSM BETONG, SARAWAK  '),
(70, 'MRSM FELDA, TROLAK, PERAK  '),
(71, 'MRSM GEMENCHEH, N SEMBILAN  '),
(72, 'MRSM GRIK, PERAK  '),
(73, 'MRSM JELI, KELANTAN  '),
(74, 'MRSM JOHOR BAHRU, JOHOR  '),
(75, 'MRSM KEPALA BATAS, PULAU PINANG  '),
(76, 'MRSM KOTA KINABALU, SABAH  '),
(77, 'MRSM KOTA PUTRA, BESUT, TERENGGANU  '),
(78, 'MRSM KUALA BERANG, TERENGGANU  '),
(79, 'MRSM KUALA KANGSAR, PERAK  '),
(80, 'MRSM KUALA KLAWANG, N.SEMBILAN  '),
(81, 'MRSM KUALA KRAI, KELANTAN  '),
(82, 'MRSM KUALA KUBU, SELANGOR  '),
(83, 'MRSM KUALA TERENGGANU, TERENGGANU  '),
(84, 'MRSM KUANTAN, PAHANG  '),
(85, 'MRSM KUBANG PASU, KEDAH  '),
(86, 'MRSM KUCHING, SARAWAK  '),
(87, 'MRSM LANGKAWI, KEDAH  '),
(88, 'MRSM LENGGONG, PERAK  '),
(89, 'MRSM MERBOK, KEDAH  '),
(90, 'MRSM MERSING, JOHOR  '),
(91, 'MRSM MUADZAM SHAH, PAHANG  '),
(92, 'MRSM MUAR, JOHOR  '),
(93, 'MRSM MUKAH, SARAWAK  '),
(94, 'MRSM PARIT, PERAK  '),
(95, 'MRSM PASIR SALAK, PERAK  '),
(96, 'MRSM PASIR TUMBUH, KELANTAN  '),
(97, 'MRSM PDRM-KULIM, KEDAH  '),
(98, 'MRSM PENDANG, KEDAH  '),
(99, 'MRSM PENGKALAN CHEPA, KELANTAN  '),
(100, 'MRSM PENGKALAN HULU, PERAK  '),
(101, 'MRSM PONTIAN, JOHOR  '),
(102, 'MRSM SANDAKAN, SABAH  '),
(103, 'MRSM SERTING, N. SEMBILAN  '),
(104, 'MRSM SUNGAI BESAR,SELANGOR  '),
(105, 'MRSM TAIPING, PERAK  '),
(106, 'MRSM TAWAU, SABAH  '),
(107, 'MRSM TERENDAK, MELAKA  '),
(108, 'MRSM TRANSKERIAN, PULAU PINANG  '),
(109, 'MRSM TUMPAT, KELANTAN  '),
(110, 'MRSM TUN ABDUL RAZAK, PEKAN,PAHANG  '),
(111, 'MRSM TUN GHAFAR BABA  '),
(112, 'MRSM TUN GHAZALI SHAFIE  '),
(113, 'PEJABAT MARA AUSTRALIA  '),
(114, 'PEJABAT MARA INDONESIA  '),
(115, 'PEJABAT MARA IRELAND  '),
(116, 'PEJABAT MARA JERMAN  '),
(117, 'PEJABAT MARA LONDON  '),
(118, 'PEJABAT MARA MESIR  '),
(119, 'PEJABAT MARA NEGERI JOHOR  '),
(120, 'PEJABAT MARA NEGERI KEDAH  '),
(121, '0016 PEJABAT MARA NEGERI KELANTAN  '),
(122, 'PEJABAT MARA NEGERI MELAKA  '),
(123, 'PEJABAT MARA NEGERI N. SEMBILAN  '),
(124, 'PEJABAT MARA NEGERI PAHANG  '),
(125, 'PEJABAT MARA NEGERI PERAK  '),
(126, 'PEJABAT MARA NEGERI PERLIS  '),
(127, 'PEJABAT MARA NEGERI PULAU PINANG  '),
(128, 'PEJABAT MARA NEGERI SABAH  '),
(129, 'PEJABAT MARA NEGERI SARAWAK  '),
(130, 'PEJABAT MARA NEGERI SELANGOR  '),
(131, 'PEJABAT MARA NEGERI TERENGGANU  '),
(132, 'PEJABAT MARA NEGERI W PERSEKUTUAN - KL  '),
(133, 'PEJABAT MARA RUSIA  '),
(134, 'PEJABAT MARA WASHINGTON  '),
(135, 'PUSAT KEPIMPINAN PELAJAR(PUSKEP)LENGGONG '),
(136, 'UNIT INTEGRITI MARA  '),
(137, 'UNIT PENYELIDIKAN DAN INOVASI  '),
(138, 'UNIT URUSETIA MAJLIS(TERMSK P.PENGERUSI) '),
(139, 'WILAYAH TENGAH KENDERAAN (K.L)  '),
(140, 'WILAYAH UTARA KENDERAAN (KEDAH) '),
(141, 'KOLEJ UNIVERSITI POLY-TECH MARA KUALA LUMPUR'),
(142, 'KOLEJ POLY-TECH MARA BANGI'),
(143, 'KOLEJ POLY-TECH MARA KUANTAN'),
(144, 'KOLEJ POLY-TECH MARA BATU PAHAT'),
(145, 'KOLEJ POLY-TECH MARA IPOH'),
(146, 'KOLEJ POLY-TECH MARA ALOR SETAR'),
(147, 'KOLEJ POLY-TECH MARA KOTA BAHRU'),
(148, 'KOLEJ POLY-TECH MARA SEMPORNA'),
(149, 'KOLEJ POLY-TECH MARA ? KESEDAR'),
(150, 'GERMAN MALAYSIA INSTITUTE'),
(151, 'Universiti Kuala Lumpur Malaysia France Institute (UniKL MFI)'),
(152, 'Universiti Kuala Lumpur British Malaysian Institute (UniKL BMI)'),
(153, 'Universiti Kuala Lumpur Malaysian Institute of Aviation Technology (UniKL MIAT)'),
(154, 'Universiti Kuala Lumpur Malaysian Spanish Institute (UniKL MSI)'),
(155, 'Universiti Kuala Lumpur Malaysian Institute of Information Technology (UniKL MIIT)'),
(156, 'Universiti Kuala Lumpur Malaysian Institute of Chemical and Bio-Engineering Technology (UniKL MICET)'),
(157, 'ÿUniversiti Kuala Lumpur Malaysian Institute of Marine Engineering Technology (UniKL MIMET)'),
(158, 'Universiti Kuala Lumpur Royal College of Medicine Perak (UniKL RCMP)'),
(159, 'Universiti Kuala Lumpur Institute of Product Designÿ and Manufacturing (UniKL IPROM)'),
(160, 'Universiti Kuala Lumpurÿ Malaysian Institute of Industrial Technology (UniKL MITEC)'),
(161, 'Universiti Kuala Lumpur Institute of Medical Science Technology (UniKL MESTECH)'),
(162, 'Universiti Kuala Lumpur Business School (UniKL Business School)'),
(163, 'Institute of Postgraduate Studiesÿ Universiti Kuala Lumpur City Campus (IPS, UniKL City Campus)'),
(164, 'Universiti Kuala Lumpur International College (UniKL ICOLE)'),
(165, 'GiatMARA Arau'),
(166, 'GiatMARA Kangar'),
(167, 'GiatMARA Padang Besar'),
(168, 'GiatMARA Alor Setar'),
(169, 'GiatMARA Baling'),
(170, 'GiatMARA Jerai'),
(171, 'GiatMARA Jerlun'),
(172, 'GiatMARA Kuala Kedah'),
(173, 'GiatMARA Kubang Pasu'),
(174, 'GiatMARA Kulim/Bandar Baharu'),
(175, 'GiatMARA Langkawi'),
(176, 'GiatMARA Merbok'),
(177, 'GiatMARA Padang Serai'),
(178, 'GiatMARA Padang Terap'),
(179, 'GiatMARA Pendang'),
(180, 'GiatMARA Pokok Sena'),
(181, 'GiatMARA Sik'),
(182, 'GiatMARA Sungai Petani'),
(183, 'GiatMARA Bagan'),
(184, 'GiatMARABalik Pulau'),
(185, 'GiatMARA Batu Kawan'),
(186, 'GiatMARA Bayan Lepas'),
(187, 'GiatMARA Bukit Bendera'),
(188, 'GiatMARA Bukit Gelugor'),
(189, 'GiatMARA Bukit Mertajam'),
(190, 'GiatMARA Jelutong'),
(191, 'GiatMARA Kepala Batas'),
(192, 'GiatMARA Nibong Tebal'),
(193, 'GiatMARA Permatang Pauh'),
(194, 'GiatMARA PRIMA Tasek Gelugor'),
(195, 'GiatMARA Sungai Bakap - Polis'),
(196, 'GiatMARA Tanjung'),
(197, 'GiatMARA Bagan Datoh'),
(198, 'GiatMARA Bagan Serai'),
(199, 'GiatMARA Batu Gajah'),
(200, 'GiatMARA Beruas'),
(201, 'GiatMARA Bukit Gantang'),
(202, 'GiatMARA Gerik'),
(203, 'GiatMARA Gopeng'),
(204, 'GiatMARA Ipoh'),
(205, 'GiatMARA Ipoh Timur'),
(206, 'GiatMARA Kampar'),
(207, 'GiatMARA Kuala Kangsar'),
(208, 'GiatMARA Larut'),
(209, 'GiatMARA Lenggong'),
(210, 'GiatMARA Lumut'),
(211, 'GiatMARA Padang Rengas'),
(212, 'GiatMARA Parit'),
(213, 'GiatMARA Parit Buntar'),
(214, 'GiatMARA Pasir Salak'),
(215, 'GiatMARA Sungai Siput'),
(216, 'GiatMARA Taiping'),
(217, 'GiatMARA Tambun'),
(218, 'GiatMARA Tanjung Malim'),
(219, 'GiatMARA Tapah'),
(220, 'GiatMARA Teluk Intan'),
(221, 'GiatMARA Ampang Jaya'),
(222, 'GiatMARA Bangi'),
(223, 'GiatMARA Gombak'),
(224, 'GiatMARA Hulu Langat'),
(225, 'GiatMARA Hulu Selangor'),
(226, 'GiatMARA Kapar'),
(227, 'GiatMARA Kelana Jaya'),
(228, 'GiatMARA Klang'),
(229, 'GiatMARA Kota Raja'),
(230, 'GiatMARA Kuala Langat'),
(231, 'GiatMARA Kuala Selangor'),
(232, 'GiatMARA Pandan'),
(233, 'GiatMARA Petaling Jaya'),
(234, 'Petaling Jaya Utara'),
(235, 'GiatMARA Puchong'),
(236, 'GiatMARA Sabak Bernam'),
(237, 'GiatMARA Selayang'),
(238, 'GiatMARA Sepang'),
(239, 'GiatMARA Serdang'),
(240, 'GiatMARA Shah Alam'),
(241, 'GiatMARA Subang'),
(242, 'GiatMARA Sungai Besar'),
(243, 'GiatMARA Tanjung Karang'),
(244, 'GiatMARA Bandar Tun Razak'),
(245, 'GiatMARA Batu'),
(246, 'GiatMARA Cheras'),
(247, 'GiatMARA Kepong'),
(248, 'GiatMARA Kuala Lumpur'),
(249, 'GiatMARA Lembah Pantai'),
(250, 'GiatMARA Putrajaya'),
(251, 'GiatMARA Segambut'),
(252, 'GiatMARA Seputeh'),
(253, 'GiatMARA Setiawangsa'),
(254, 'GiatMARA Titiwangsa'),
(255, 'GiatMARA Wangsa Maju'),
(256, 'GiatMARA Jelebu'),
(257, 'GiatMARA Jempol'),
(258, 'GiatMARA Kuala Pilah'),
(259, 'GiatMARA Rasah'),
(260, 'GiatMARA Rembau'),
(261, 'GiatMARA Seremban'),
(262, 'GiatMARA Tampin'),
(263, 'GiatMARA Telok Kemang'),
(264, 'GiatMARA Alor Gajah'),
(265, 'GiatMARA Bukit Katil'),
(266, 'GiatMARA Jasin'),
(267, 'GiatMARA Kota Melaka'),
(268, 'GiatMARA Masjid Tanah'),
(269, 'GiatMARA Simpang Ampat'),
(270, 'GiatMARA Tangga Batu'),
(271, 'GiatMARA Ayer Hitam'),
(272, 'GiatMARA Bakri'),
(273, 'GiatMARA Batu Pahat'),
(274, 'GiatMARA Gelang Patah'),
(275, 'GiatMARA Johor Bahru'),
(276, 'GiatMARA Kluang'),
(277, 'GiatMARA Komuniti Ledang'),
(278, 'GiatMARA Kota Tinggi'),
(279, 'GiatMARA Kulai'),
(280, 'GiatMARA Labis'),
(281, 'GiatMARA Mersing'),
(282, 'GiatMARA Muar'),
(283, 'GiatMARA Pagoh'),
(284, 'GiatMARA Parit Sulong'),
(285, 'GiatMARA Pasir Gudang'),
(286, 'GiatMARA Pengerang'),
(287, 'GiatMARA Pontian'),
(288, 'GiatMARA Segamat'),
(289, 'GiatMARA Sekijang'),
(290, 'GiatMARA Sembrong'),
(291, 'GiatMARA Simpang Renggam'),
(292, 'GiatMARA Sri Gading'),
(293, 'GiatMARA Tanjong Piai'),
(294, 'GiatMARA Tebrau'),
(295, 'GiatMARA Tenggara'),
(296, 'GiatMARA Bentong'),
(297, 'GiatMARA Bera'),
(298, 'GiatMARA Cameron Highlands'),
(299, 'GiatMARA Indera Mahkota'),
(300, 'GiatMARA Jengka'),
(301, 'GiatMARA Jerantut'),
(302, 'GiatMARA Kuala Krau'),
(303, 'GiatMARA Kuala Lipis'),
(304, 'GiatMARA Kuantan'),
(305, 'GiatMARA Maran'),
(306, 'GiatMARA Muadzam Shah'),
(307, 'GiatMARA Paya Besar'),
(308, 'GiatMARA Pekan'),
(309, 'GiatMARA Raub'),
(310, 'GiatMARA Rompin'),
(311, 'GiatMARA Temerloh'),
(312, 'GiatMARA Batu Rakit'),
(313, 'GiatMARA Besut'),
(314, 'GiatMARA Dungun'),
(315, 'GiatMARA Hulu Terengganu'),
(316, 'GiatMARA Kemaman'),
(317, 'GiatMARA Ketengah'),
(318, 'GiatMARA Kuala Nerus'),
(319, 'GiatMARA Kuala Terengganu'),
(320, 'GiatMARA Marang'),
(321, 'GiatMARA Setiu'),
(322, 'GiatMARA Bachok'),
(323, 'GiatMARA Gua Musang'),
(324, 'GiatMARA Jeli'),
(325, 'GiatMARA Ketereh'),
(326, 'GiatMARA Kota Bharu'),
(327, 'GiatMARA Kuala Krai'),
(328, 'GiatMARA Kubang Kerian'),
(329, 'GiatMARA Machang'),
(330, 'GiatMARA Nilam Puri'),
(331, 'GiatMARA Pasir Mas'),
(332, 'GiatMARA Pasir Puteh'),
(333, 'GiatMARA Pengkalan Chepa'),
(334, 'GiatMARA Rantau Panjang'),
(335, 'GiatMARA Tanah Merah'),
(336, 'GiatMARA Tumpat'),
(337, 'GiatMARA Batu Sapi'),
(338, 'GiatMARA Beaufort'),
(339, 'GiatMARA Beluran'),
(340, 'GiatMARA Gaya'),
(341, 'GiatMARA Keningau'),
(342, 'GiatMARA Kimanis'),
(343, 'GiatMARA Kinabatangan'),
(344, 'GiatMARA Kota Belud'),
(345, 'GiatMARA Kota Kinabalu'),
(346, 'GiatMARA Kota Marudu'),
(347, 'GiatMARA Kudat'),
(348, 'GiatMARA Labuan'),
(349, 'GiatMARA Libaran'),
(350, 'GiatMARA Limbawang'),
(351, 'GiatMARA Papar'),
(352, 'GiatMARA Penampang'),
(353, 'GiatMARA Pensiangan'),
(354, 'GiatMARA Putatan'),
(355, 'GiatMARA Ranau'),
(356, 'GiatMARA Sandakan'),
(357, 'GiatMARA Semporna'),
(358, 'GiatMARA Sepanggar'),
(359, 'GiatMARA Silam'),
(360, 'GiatMARA Sipitang'),
(361, 'GiatMARA Tawau'),
(362, 'GiatMARA Tenom'),
(363, 'GiatMARA Tuaran'),
(364, 'GiatMARA Bandar Kuching'),
(365, 'GiatMARA Baram'),
(366, 'GiatMARA Batang Lupar'),
(367, 'GiatMARA Batang Sadong'),
(368, 'GiatMARA Betong'),
(369, 'GiatMARA Bintulu'),
(370, 'GiatMARA Hulu Rejang'),
(371, 'GiatMARA Igan'),
(372, 'GiatMARA Julau'),
(373, 'GiatMARA Kanowit'),
(374, 'GiatMARA Kapit'),
(375, 'GiatMARA Kota Samarahan'),
(376, 'GiatMARA Lanang'),
(377, 'GiatMARA Lawas'),
(378, 'GiatMARA Limbang'),
(379, 'GiatMARA Lubok Antu'),
(380, 'GiatMARA Mambong'),
(381, 'GiatMARA Mas Gading'),
(382, 'GiatMARA Miri'),
(383, 'GiatMARA Mukah'),
(384, 'GiatMARA Petra Jaya'),
(385, 'GiatMARA Santubong'),
(386, 'GiatMARA Saratok'),
(387, 'GiatMARA Sarikei'),
(388, 'GiatMARA Selangau'),
(389, 'GiatMARA Serian'),
(390, 'GiatMARA Sibu'),
(391, 'GiatMARA Sibuti'),
(392, 'GiatMARA Sri Aman'),
(393, 'GiatMARA Stampin'),
(394, 'GiatMARA Tanjung Manis');

DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `calculate_top_jooters_event` ON SCHEDULE EVERY 1 DAY STARTS '2013-04-10 00:00:01' ON COMPLETION NOT PRESERVE ENABLE DO BEGIN
						DECLARE done INT DEFAULT FALSE;
						DECLARE a INT;
						DECLARE cur1 CURSOR FOR SELECT user_id FROM user WHERE DATE(last_activity) > date(subdate(current_date,2));
						DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

						OPEN cur1;
						
						
						UPDATE user_interaction_point 
						SET day_7=day_6, day_6=day_5,day_5=day_4,day_4= day_3,
						day_3=day_2,day_2=day_1,day_1=0 WHERE 1;
	  
						read_loop: LOOP
							FETCH cur1 INTO a;
							IF done THEN
								LEAVE read_loop;
							END IF;
							
							
							UPDATE user_interaction_point
								SET user_interaction_point.day_1 = 
									(SELECT 
									((SELECT 
										COUNT(card.card_id) AS num_cards
										FROM card
										WHERE card.creator = a AND DATE(card.created_date) = date(subdate(current_date,1)) AND card.is_rehosted = 0) + 
									(SELECT 
										COUNT(card_view.card_id) AS num_views
										FROM card_view
										WHERE card_view.user_id = a AND DATE(card_view.view_time) = date(subdate(current_date,1)))
									+
									(SELECT 
										COUNT(conversation.card_id) AS num_comments
										FROM conversation
										WHERE conversation.user_id = a AND DATE(conversation.timestamp) = date(subdate(current_date,1)))
									+
									(SELECT 
										COUNT(card_follower.card_id) AS num_follows
										FROM card_follower
										WHERE card_follower.user_id = a AND DATE(card_follower.following_date) =date(subdate(current_date,1)))
									+
									(SELECT 
										COUNT(card_marking.card_id) AS num_likes
										FROM card_marking
										WHERE card_marking.user_id = a AND DATE(card_marking.timestamp) =date(subdate(current_date,1)) AND card_marking.type = " . CARD_MARK_LIKE . ")
									+
									(SELECT 
										COUNT(card.card_id) AS num_rehosts
										FROM card
										WHERE card.creator = a AND DATE(card.created_date) =date(subdate(current_date,1)) AND card.is_rehosted = 1)) 
									AS interaction_point),
									avg_interaction_point = 
									(((day_1 * pow(0.5,1/3)) + (day_2 * pow(0.5,2/3)) + 
									(day_3 * pow(0.5,1)) + (day_4 * pow(0.5,4/3)) + 
									(day_5 * pow(0.5,5/3)) + (day_6 * pow(0.5,2)) + 
									(day_7 * pow(0.5,7/3))) / 7)
								WHERE user_interaction_point.user_id = a; 
						END LOOP;

						CLOSE cur1;
					END$$

CREATE DEFINER=`root`@`localhost` EVENT `top_jooters_half_life` ON SCHEDULE EVERY 1 DAY STARTS '2013-06-11 00:00:01' ON COMPLETION NOT PRESERVE ENABLE DO BEGIN
						DECLARE done INT DEFAULT FALSE;
						DECLARE a, b, i INT;
						DECLARE total, avg DOUBLE;
						DECLARE ip TEXT;
						DECLARE cur1 CURSOR FOR SELECT user_id FROM user WHERE DATE(last_activity) > date(subdate(current_date,2));
						DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

						OPEN cur1;

						
						UPDATE user_interaction_point 
						SET interaction_points = CONCAT(0,';',SUBSTRING_INDEX(interaction_points,';',6))
						WHERE 1;

						
						read_loop: LOOP
							FETCH cur1 INTO a;
							IF done THEN
								LEAVE read_loop;
							END IF;
							
							SELECT 
								(1 * (SELECT 
									COUNT(card.card_id) AS num_cards
									FROM card
									WHERE card.creator = a AND DATE(card.created_date) = date(subdate(current_date,1)) AND card.is_rehosted = 0) 
								+ 
								1 * (SELECT 
									COUNT(card_view.card_id) AS num_views
									FROM card_view
									WHERE card_view.user_id = a AND DATE(card_view.view_time) = date(subdate(current_date,1)))
								+
								1 * (SELECT 
									COUNT(conversation.card_id) AS num_comments
									FROM conversation
									WHERE conversation.user_id = a AND DATE(conversation.timestamp) = date(subdate(current_date,1)))
								+
								1 * (SELECT 
									COUNT(card_follower.card_id) AS num_follows
									FROM card_follower
									WHERE card_follower.user_id = a AND DATE(card_follower.following_date) =date(subdate(current_date,1)))
								+
								1 * (SELECT 
									COUNT(card_marking.card_id) AS num_likes
									FROM card_marking
									WHERE card_marking.user_id = a AND DATE(card_marking.timestamp) =date(subdate(current_date,1)) AND card_marking.type = 0)
								+
								1 * (SELECT 
									COUNT(card.card_id) AS num_rehosts
									FROM card
									WHERE card.creator = a AND DATE(card.created_date) =date(subdate(current_date,1)) AND card.is_rehosted > 0)) INTO b;
							SELECT interaction_points INTO ip FROM user_interaction_point WHERE user_id = a;
							SET total = b * pow(0.5,1/3);
							SET i = 2;
							WHILE i <= 7 DO
								SET total = total + ((REPLACE(SUBSTRING(SUBSTRING_INDEX(ip, ';', i),
								LENGTH(SUBSTRING_INDEX(ip, ';', i -1)) + 1),
								';', '')) * pow(0.5,i/3));
								SET i = i + 1;
							END WHILE;
							SET avg = total / 7;
							UPDATE user_interaction_point
								SET interaction_points = CONCAT(b,';',SUBSTRING_INDEX(interaction_points,';',-6)),
									avg_interaction_point = avg
								WHERE user_interaction_point.user_id = a; 
						END LOOP;

						CLOSE cur1;
					END$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
