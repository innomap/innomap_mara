-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 10, 2016 at 04:16 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `innomap_v2_mara`
--

--
-- Dumping data for table `mara_evaluation_focus`
--

INSERT INTO `mara_evaluation_focus` (`id`, `mara_evaluation_form_id`, `label`, `percentage`) VALUES
(1, 1, 'Innovativeness', 30),
(6, 4, 'Innovativeness', 30),
(7, 1, 'Relevance', 10),
(8, 1, 'Significance', 30),
(9, 1, 'Effectiveness', 30),
(10, 4, 'Relevance', 10),
(11, 4, 'Significance', 30),
(12, 4, 'Effectiveness', 30),
(13, 7, 'Innovativeness', 30),
(14, 7, 'Relevance', 10),
(15, 7, 'Significance', 30),
(16, 7, 'Effectiveness', 30),
(17, 8, 'Innovativeness', 25),
(18, 8, 'Quality', 25),
(19, 8, 'Significance', 20),
(20, 8, 'Effectiveness', 30),
(21, 9, 'Innovativeness', 25),
(22, 9, 'Quality', 25),
(23, 9, 'Significance', 20),
(24, 9, 'Effectiveness', 30),
(25, 10, 'Innovativeness', 25),
(26, 10, 'Quality', 25),
(27, 10, 'Significance', 30),
(28, 10, 'Effectiveness', 20);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
