<?php

	$lang['email'] = 'Email';
	$lang['password'] = 'Password';
	$lang['retype_password'] = 'Retype Password';
	$lang['name'] = 'Name';
	$lang['mara_center'] = 'Mara Center';
	$lang['already_have_account'] = 'Already have account ?';
	$lang['register'] = 'Register';
	$lang['username'] = 'Username';
	$lang['registration_form'] = "Registration Form";
	$lang['staff_id'] = "Staff Id";
	$lang['registration'] = "Registration";
	$lang['mara_address'] = "Mara Address";
	$lang['mara_telp_no'] = "Mara Telp No";
	$lang['mara_fax_no'] = "Mara Fax No";
	$lang['no_kp'] = "No KP";
	$lang['telp_no'] = "Telephone No";
	$lang['fax_no'] = "Fax No";
	$lang['photo'] = "Photo";
	$lang['forgot_password'] = "Forgot Password?";
	$lang['reset_password'] = "Reset Password";
	$lang['send_email'] = "Send Email";
	$lang['forgot_password_msg'] = "Please provide the email address that you used when you signed up for your Maratex2016 account. We will send you an email that will allow you to reset your password.";
	$lang['page_not_found_msg'] = "Sorry, this page is no longer available.";
	$lang['save'] = "Save";
	$lang['edit_profile'] = "Edit Profile";
?>