<?php

	$lang['action'] = "Action";
	$lang['edit'] = "Edit";
	$lang['delete'] = "Delete";
	$lang['save'] = "Save";
	$lang['cancel'] = "Cancel";
	$lang['delete_evaluation_focus'] = "Delete Evaluation Focus";
	$lang['delete_confirm_message'] = "Are you sure want to delete ";
	$lang['new_evaluation_focus'] = "New Evaluation Focus";
	$lang['percentage'] = "Percentage";
	$lang['evaluation_focus'] = "Evaluation Focus";
	$lang['criteria'] = "Criteria";
	$lang['question'] = "Question";
	$lang['max_point'] = "Max Point";
	$lang['category'] = "Category";
	$lang['subcategory'] = "Subcategory";
	$lang['new_evaluation_form'] = "New Evaluation Form";
	$lang['this_data'] = "This data";
	$lang['evaluation_form'] = "Evaluation Form";
	$lang['guidelines'] = "Guidelines";
?>