<?php
	
	$lang['round'] = "Round";
	$lang['new_round'] = "Add Round";
	$lang['title'] = "Title";
	$lang['deadline'] = "Deadline";
	$lang['action'] = "Action";
	$lang['edit'] = "Edit";
	$lang['delete'] = "Delete";
	$lang['save'] = "Save";
	$lang['cancel'] = "Cancel";
	$lang['delete_round'] = "Delete Round";
	$lang['delete_confirm_message'] = "Are you sure want to delete ";
?>