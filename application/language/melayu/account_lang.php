<?php

	$lang['email'] = 'Emel';
	$lang['password'] = 'Kata Laluan';
	$lang['retype_password'] = 'Ulangi Kata Laluan';
	$lang['name'] = 'Nama';
	$lang['mara_center'] = 'Pusat Mara';
	$lang['already_have_account'] = 'Sudah mempunyai Akaun ?';
	$lang['register'] = 'Daftar';
	$lang['username'] = 'Username';
	$lang['registration_form'] = "Pendaftaran Sistem";
	$lang['staff_id'] = "No Gaji";
	$lang['registration'] = "Pendaftaran";
	$lang['mara_address'] = "Alamat Pusat";
	$lang['mara_telp_no'] = "No Telefon Pusat";
	$lang['mara_fax_no'] = "No Faks Pusat";
	$lang['no_kp'] = "No KP";
	$lang['telp_no'] = "No Telefon";
	$lang['fax_no'] = "No Faks";
	$lang['photo'] = "Foto Ketua Projek";
	$lang['forgot_password'] = "Lupa Kata Laluan?";
	$lang['reset_password'] = "Reset Kata Laluan";
	$lang['send_email'] = "Hantar Emel";
	$lang['forgot_password_msg'] = "Sila berikan alamat e-mel yang anda gunakan semasa anda mendaftar untuk akaun Maratex2016 anda. Kami akan menghantar e-mel yang akan membolehkan anda untuk me-reset kata laluan anda.";
	$lang['page_not_found_msg'] = "Maaf, halaman ini tidak lagi boleh diakses.";
	$lang['save'] = "Simpan";
	$lang['edit_profile'] = "Edit Profil";
?>