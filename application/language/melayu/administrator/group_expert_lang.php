<?php 

	$lang['action'] = "Tindakan";
	$lang['edit'] = "Ubah";
	$lang['delete'] = "Padam";
	$lang['save'] = "Simpan";
	$lang['cancel'] = "Batal";
	$lang['delete_group'] = "Padam Kumpulan";
	$lang['delete_confirm_message'] = "Anda yakin akan memadam ";
	$lang['new_group'] = "Tambah Kumpulan";
	$lang['group_name'] = "Nama Kumpulan";
	$lang['group_description'] = "Deskripsi Kumpulan";
	$lang['form_group'] = "Form Group";
	$lang['select_evaluator'] = "Pilih Evaluator";
?>