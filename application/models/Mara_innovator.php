<?php

require_once(APPPATH . 'models/General_model.php');
class Mara_innovator extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "mara_innovator";
		$this->primary_field = "id";
	}

	function insert_innovator($data){
		return $this->db->insert($this->table_name, $data);
	}

	function get_innovator_name($user_id){
		$innovator = $this->mara_innovator->find_by_id($user_id);
        if($innovator){
            return $innovator['name'];
        }else{
        	return "";
        }
	}

	function get_one_join($user_id){
		$this->db->select("mara_innovator.*, user.email, mara_center.name as mara_center");
		$this->db->from("mara_innovator");
		$this->db->join('user','user.user_id = mara_innovator.id');
		$this->db->join('mara_center', 'mara_center.id = mara_innovator.mara_center_id');
		$this->db->where('mara_innovator.id', $user_id);
		$q = $this->db->get();

		return $q->row_array();
	}

	function get_join($where = NULL, $limit = NULL, $order_by = NULL){
		$this->db->select("mara_innovator.*, user.email, user.created_date, mara_center.name as mara_center");
		$this->db->from("mara_innovator");
		$this->db->join('user','user.user_id = mara_innovator.id');
		$this->db->join('mara_center', 'mara_center.id = mara_innovator.mara_center_id');
		if($where != NULL){
			$this->db->where($where);
		}
		if($limit != NULL){
			$this->db->limit($limit);
		}
		if($order_by != NULL){
			$this->db->order_by($order_by);
		}
		return $this->db->get();
	}
	
}

?>