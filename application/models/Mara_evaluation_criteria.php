<?php

require_once(APPPATH . 'models/General_model.php');
class Mara_evaluation_criteria extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "mara_evaluation_criteria";
		$this->primary_field = "id";
	}

	function delete_by_evaluation_focus($focus_id){
		$this->db->where('mara_evaluation_focus_id', $focus_id);
		return $this->db->delete($this->table_name);
	}

	function get_max_point($where = NULL){
		$arg = "SELECT SUM(mara_evaluation_criteria.max_point) as max_point FROM `mara_evaluation_criteria` 
					".
					($where != NULL ? " WHERE {$where}" : "")
					." GROUP BY mara_evaluation_criteria.mara_evaluation_focus_id";

		$query = $this->db->query($arg);
            
        return $query;
	}
}

?>