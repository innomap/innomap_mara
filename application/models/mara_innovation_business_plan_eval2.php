<?php

require_once(APPPATH . 'models/General_model.php');
class Mara_innovation_business_plan_eval2 extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "mara_innovation_business_plan_eval2";
		$this->primary_field = "id";
	}

	function get_total_score($where = NULL){
		$arg = "SELECT ROUND(SUM(mara_innovation_business_plan_eval2.score),1) as score FROM `mara_innovation_business_plan_eval2` 
				WHERE mara_innovation_business_plan_eval2.score != 0
					".
					($where != NULL ? " AND {$where}" : "")
					." GROUP BY mara_innovation_business_plan_eval2.mara_innovation_business_plan_id";

		$query = $this->db->query($arg);
            
        return $query;
	}
}

?>