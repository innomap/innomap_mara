<?php

require_once(APPPATH . 'models/General_model.php');
class Innovation extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "innovation";
		$this->primary_field = "innovation_id";
	}
	

	function delete_innovation_category($innovation_id){
		$this->db->where('innovation_id', $innovation_id);
		return $this->db->delete('innovation_category');
	}

	function get_innovation_category($id){
		$this->db->where('innovation_id',$id);
		$q = $this->db->get('innovation_category');
		return $q->result_array();
	}

	function add_innovation_category($data){
		return $this->db->insert('innovation_category', $data);
	}

	function get_innovation_picture($id){
		$this->db->where('innovation_id',$id);
		$q = $this->db->get('innovation_picture');
		return $q->result_array();
	}

	function add_innovation_picture($data){
		return $this->db->insert('innovation_picture', $data);
	}

	function delete_innovation_picture($picture_id){
		$this->db->where('innovation_picture_id', $picture_id);
		return $this->db->delete('innovation_picture');
	}

	function get_number($where = NULL){
		$this->db->select("*");
        $this->db->from($this->table_name);
        if($where != NULL){
        	$this->db->where($where);
        }
        $q = $this->db->get();

        return $q->num_rows();
	}

	function get_join($where = NULL, $group_by = NULL){
		$this->db->select('*, mara_innovator.name as innovator_name, mara_center.name as mara_center');
		$this->db->from($this->table_name);
		$this->db->join('mara_innovation_category', 'mara_innovation_category.innovation_id = innovation.innovation_id');
		$this->db->join('mara_innovator', 'mara_innovator.id = innovation.analyst_creator_id');
		$this->db->join('user', 'mara_innovator.id = user.user_id');
		$this->db->join('mara_center', 'mara_center.id = mara_innovator.mara_center_id');
		if($where != NULL){
			$this->db->where($where);
		}

		if($group_by != NULL){
			$this->db->group_by($group_by);
		}

		return $this->db->get();
	}

}

?>