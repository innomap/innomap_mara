<?php

require_once(APPPATH . 'models/General_model.php');
class Mara_innovation_evaluator extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "mara_innovation_evaluator";
	}

	function get_list_innovations($where = NULL){
		$this->db->select('*');
		$this->db->from($this->table_name);
		$this->db->join('innovation', 'innovation.innovation_id = mara_innovation_evaluator.innovation_id');
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get()->result_array();
	}

	function get_list_evaluation($innovation_id, $where = NULL){
		$this->db->select('mara_evaluator.name as evaluator, mara_innovation_evaluator.*');
		$this->db->from($this->table_name);
		$this->db->join('mara_evaluator', 'mara_innovation_evaluator.mara_evaluator_id = mara_evaluator.evaluator_id');
		$this->db->where("mara_innovation_evaluator.innovation_id = ".$innovation_id);
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get()->result_array();
	}

	function is_evaluation_done($innovation_id){
		$this->db->select($this->table_name.'.*');
		$this->db->from($this->table_name);
		$this->db->join('mara_evaluation', $this->table_name.'.mara_evaluator_id = mara_evaluation.mara_evaluator_id AND '.$this->table_name.'.innovation_id = mara_evaluation.innovation_id','LEFT');
		$this->db->where($this->table_name.".innovation_id", $innovation_id);
		$this->db->where("id IS NULL");

		$row = $this->db->get()->num_rows();
		
		if($row > 0){
			$result = false;
		}else{
			$result = true;
		}

		return $result;
	}
}

?>