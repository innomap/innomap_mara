<?php

require_once(APPPATH . 'models/General_model.php');
class Mara_evaluation_detail extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "mara_evaluation_detail";
		$this->primary_field = "id";
	}

	function delete_by_evaluation($evaluation_id){
		$this->db->where("mara_evaluation_id", $evaluation_id);
		return $this->db->delete($this->table_name);
	}

	function get_by_innovation($innovation_id){
		$this->db->select('mara_evaluation_detail.*');
		$this->db->from($this->table_name);
		$this->db->join('mara_evaluation','mara_evaluation.id = mara_evaluation_detail.mara_evaluation_id');
		$this->db->where('innovation_id', $innovation_id);

		return $this->db->get()->result_array();
	}

	function get_join_criteria($where = NULL, $limit = NULL, $offset = NULL, $group_by = NULL){
		$this->db->select('*');
		$this->db->from($this->table_name);
		$this->db->join('mara_evaluation_criteria',' mara_evaluation_criteria.id = mara_evaluation_detail.mara_eval_criteria_id');
		$this->db->join('mara_evaluation_focus',' mara_evaluation_criteria.mara_evaluation_focus_id = mara_evaluation_focus.id');
		if($where != NULL){
			$this->db->where($where);
		}

		if($group_by != NULL){
			$this->db->group_by($group_by);
		}

		if($limit != NULL){
			$this->db->limit($limit);
		}

		if($offset != NULL){
			$this->db->offset($offset);
		}

		return $this->db->get()->result_array();
	}

	function get_score($where = NULL){
		$arg = "SELECT SUM(mara_evaluation_detail.point) as total FROM `mara_evaluation_detail` 
				JOIN mara_evaluation_criteria ON mara_evaluation_criteria.id = mara_evaluation_detail.mara_eval_criteria_id
					".
					($where != NULL ? " WHERE {$where}" : "")
					." GROUP BY mara_evaluation_criteria.mara_evaluation_focus_id";

		$query = $this->db->query($arg);
            
        return $query;
	}
}

?>