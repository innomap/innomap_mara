<?php

require_once(APPPATH . 'models/General_model.php');
class Mara_innovation_category extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "mara_innovation_category";
		$this->primary_field = "id";
	}

	function get_number($where = NULL){
		$this->db->select("*");
        $this->db->from($this->table_name);
        if($where != NULL){
        	$this->db->where($where);
        }
        $q = $this->db->get();

        return $q->num_rows();
	}
}

?>