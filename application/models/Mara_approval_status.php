<?php

require_once(APPPATH . 'models/General_model.php');
class Mara_approval_status extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "mara_approval_status";
		$this->primary_field = "id";
	}

	function get_innovation_status($innovation_id){
		$get_status = $this->find("innovation_id = ".$innovation_id, 1, 0, "id DESC");
        return $get_status[0]['status'];
	}

	function get_innovation_last_status($innovation_id, $where = NULL){
		$this->db->select('*,'.$this->table_name.'.innovation_id as innovation_id,'.$this->table_name.'.mara_round_id as mara_round_id');
		$this->db->from($this->table_name);
		$this->db->join('mara_round', 'mara_round.id = mara_approval_status.mara_round_id');
		$this->db->join('mara_evaluation', 'mara_evaluation.id = mara_approval_status.mara_evaluation_id','LEFT');
		$this->db->where("mara_approval_status.innovation_id = ".$innovation_id);
		if($where != NULL){
			$this->db->where($where);
		}
		$this->db->order_by('mara_approval_status.id', "DESC");
		$q = $this->db->get();
		return $q->row_array();
	}

	function get_number($where = NULL, $group_by = NULL){
		$this->db->select("*");
        $this->db->from($this->table_name);
        if($where != NULL){
        	$this->db->where($where);
        }

        if($group_by != NULL){
        	$this->db->group_by($group_by);
        }
        $q = $this->db->get();

        return $q->num_rows();
	}

	function get_innovation_list($where = NULL, $group_by = NULL){
		$this->db->select('*, mara_innovator.name as innovator_name, mara_center.name as mara_center');
		$this->db->from($this->table_name);
		$this->db->join('innovation', 'innovation.innovation_id = '.$this->table_name.'.innovation_id');
		$this->db->join('mara_innovation_category', 'mara_innovation_category.innovation_id = innovation.innovation_id');
		$this->db->join('mara_innovator', 'mara_innovator.id = innovation.analyst_creator_id');
		$this->db->join('user', 'mara_innovator.id = user.user_id');
		$this->db->join('mara_center', 'mara_center.id = mara_innovator.mara_center_id');
		if($where != NULL){
			$this->db->where($where);
		}

		if($group_by != NULL){
			$this->db->group_by($group_by);
		}

		return $this->db->get();
	}

	function get_business_plan_list($where = NULL, $group_by = NULL){
		$this->db->select('mara_innovation_business_plan.*,innovation.innovation_id, innovation.name_in_melayu, mara_innovator.name as innovator_name, mara_center.name as mara_center, mara_innovation_business_plan.id as business_plan_id');
		$this->db->from($this->table_name);
		$this->db->join('innovation', 'innovation.innovation_id = '.$this->table_name.'.innovation_id');
		$this->db->join('mara_innovation_business_plan', 'mara_innovation_business_plan.innovation_id = innovation.innovation_id');
		$this->db->join('mara_innovator', 'mara_innovator.id = innovation.analyst_creator_id');
		$this->db->join('user', 'mara_innovator.id = user.user_id');
		$this->db->join('mara_center', 'mara_center.id = mara_innovator.mara_center_id');
		if($where != NULL){
			$this->db->where($where);
		}

		if($group_by != NULL){
			$this->db->group_by($group_by);
		}

		return $this->db->get();
	}
}

?>