<?php

require_once(APPPATH . 'models/General_model.php');
class Mara_innovation_team extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "mara_innovation_team";
		$this->primary_field = "id";
	}

	function delete_by_innovation($innovation_id){
		$this->db->where('innovation_id', $innovation_id);
		return $this->db->delete($this->table_name);
	}
}

?>