<?php

require_once(APPPATH . 'models/General_model.php');
class Mara_innovation_business_plan_eval1 extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "mara_innovation_business_plan_eval1";
		$this->primary_field = "id";
	}

	function get_total_score($where = NULL){
		$arg = "SELECT ROUND(SUM(mara_innovation_business_plan_eval1.score),1) as score FROM `mara_innovation_business_plan_eval1` 
				WHERE mara_innovation_business_plan_eval1.score != 0
					".
					($where != NULL ? " AND {$where}" : "")
					." GROUP BY mara_innovation_business_plan_eval1.mara_innovation_business_plan_id";

		$query = $this->db->query($arg);
            
        return $query;
	}
}

?>