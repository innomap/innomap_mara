<?php

require_once(APPPATH . 'models/General_model.php');
class Mara_group_evaluator extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "mara_group_evaluator";
		$this->primary_field = "id";
	}

	function delete_by_group($group_id){
		$this->db->where('mara_group_expert_id', $group_id);
		return $this->db->delete($this->table_name);
	}

	function delete_by_evaluator($evaluator_id){
		$this->db->where('mara_evaluator_id', $evaluator_id);
		return $this->db->delete($this->table_name);
	}
}

?>