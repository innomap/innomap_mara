<?php

require_once(APPPATH . 'models/General_model.php');
class User extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "user";
		$this->primary_field = "user_id";
	}

	function is_email_exist($email, $id = 0){
		$user = $this->find_one('email = "'.$email.'" AND is_mara_user = 1'.($id != 0 ? ' AND user_id != '.$id : ''));
		if($user != NULL){
			return true;
		}else{
			return false;
		}
	}

	function insert_user($data, $role = ROLE_MARA_INNOVATOR){
		$data["password"] = $this->get_hash($data["username"], $data["password"]);
		if($id = $this->insert($data)){
			$this->insert_role(array('user_id' => $id, 'role_id' => $role));
		}

		return $id;
	}

	function update_user($id, $data){
		if(isset($data['password'])){
			$data["password"] = $this->get_hash($data["username"], $data["password"]);	
		}
		
		return $this->update($id, $data);
	}

	function get_hash($username, $password) {
		return md5($username.':'.$password);
	}

	function insert_role($data){
		$this->db->insert('user_role', $data);
		return $this->db->insert_id();
	}

	function auth($where){
		$this->db->select('user.*, user_role.role_id');
		$this->db->from('user');
		$this->db->join('user_role', 'user_role.user_id = user.user_id');
		$this->db->where($where);
		$q = $this->db->get();
		return $q->row_array();
	}

	function update_password($id, $data){
		$data["password"] = $this->get_hash($data["username"], $data["password"]);
		return $this->update($id, $data);
	}

	function get_number($where = NULL){
		$this->db->select("*");
        $this->db->from($this->table_name);
        if($where != NULL){
        	$this->db->where($where);
        }
        $q = $this->db->get();

        return $q->num_rows();
	}
}

?>