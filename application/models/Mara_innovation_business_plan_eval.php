<?php

require_once(APPPATH . 'models/General_model.php');
class Mara_innovation_business_plan_eval extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "mara_innovation_business_plan_eval";
		$this->primary_field = "id";
	}

	function get_total_score($where = NULL){
		$arg = "SELECT ROUND(SUM(mara_innovation_business_plan_eval.score),1) as score FROM `mara_innovation_business_plan_eval` 
				WHERE mara_innovation_business_plan_eval.score != 0
					".
					($where != NULL ? " AND {$where}" : "")
					." GROUP BY mara_innovation_business_plan_eval.mara_innovation_business_plan_id";

		$query = $this->db->query($arg);
            
        return $query;
	}
}

?>