<?php

require_once(APPPATH . 'models/General_model.php');
class Mara_evaluation extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "mara_evaluation";
		$this->primary_field = "id";
	}

	function check_evaluation_id($innovation_id){
		if($this->find_one("innovation_id = ".$innovation_id)){
			return true;
		}else{
			return false;
		}
	}

	function get_number($where = NULL){
		$this->db->select('*');
		$this->db->from($this->table_name);
		if($where != NULL){
			$this->db->where($where);
		}

		return $this->db->get()->num_rows();
	}

	function get_top_score_list($where = NULL, $limit = 60){
		$arg = "SELECT mara_evaluation.innovation_id,mara_evaluation.mara_evaluator_id,innovation.name as innovation_name, mara_innovator.name as innovator_name,user.email, mara_innovator.telp_no,mara_innovator.kp_no,mara_innovator.staff_id, mara_center.name as mara_center_name, ROUND(AVG(mara_evaluation.total),1) as score, mara_innovation_team.id as team_id, mara_innovation_info.validation_attachment FROM `mara_evaluation` 
					JOIN innovation ON innovation.innovation_id = mara_evaluation.innovation_id
					JOIN mara_innovation_category ON innovation.innovation_id = mara_innovation_category.innovation_id
					JOIN mara_innovator ON mara_innovator.id = innovation.analyst_creator_id
					JOIN user ON mara_innovator.id = user.user_id
					JOIN mara_center ON mara_center.id = mara_innovator.mara_center_id
					JOIN mara_innovation_team ON mara_innovation_team.innovation_id = innovation.innovation_id
					JOIN mara_innovation_info ON mara_innovation_info.innovation_id = innovation.innovation_id
					WHERE mara_evaluation.total != 0 AND mara_innovation_team.id IS NOT NULL AND validation_attachment != '' ".
					($where != NULL ? " AND {$where}" : "")
					." GROUP BY mara_evaluation.innovation_id ORDER BY score DESC LIMIT 0,{$limit}";

		$query = $this->db->query($arg);
            
        return $query;
	}

	function get_top_score_by_group_list($where = NULL, $limit = 100){
		$arg = "SELECT mara_evaluation.innovation_id,mara_evaluation.mara_evaluator_id,innovation.name as innovation_name, mara_innovator.name as innovator_name,user.email, mara_innovator.telp_no,mara_innovator.kp_no,mara_innovator.staff_id, mara_center.name as mara_center_name, ROUND(AVG(mara_evaluation.total),1) as score, mara_innovation_team.id as team_id, mara_innovation_info.validation_attachment FROM `mara_evaluation` 
					JOIN innovation ON innovation.innovation_id = mara_evaluation.innovation_id
					JOIN mara_innovation_category ON innovation.innovation_id = mara_innovation_category.innovation_id
					JOIN mara_innovator ON mara_innovator.id = innovation.analyst_creator_id
					JOIN user ON mara_innovator.id = user.user_id
					JOIN mara_center ON mara_center.id = mara_innovator.mara_center_id
					JOIN mara_group_evaluator ON mara_group_evaluator.mara_evaluator_id = mara_evaluation.mara_evaluator_id
					JOIN mara_innovation_team ON mara_innovation_team.innovation_id = innovation.innovation_id
					JOIN mara_innovation_info ON mara_innovation_info.innovation_id = innovation.innovation_id
					WHERE mara_evaluation.total != 0 AND mara_innovation_team.id IS NOT NULL AND validation_attachment != ''
					".
					($where != NULL ? " AND {$where}" : "")
					." GROUP BY mara_evaluation.innovation_id ORDER BY score DESC LIMIT 0,{$limit}";

		$query = $this->db->query($arg);
            
        return $query;
	}

	function get_average_score($where = NULL){
		$arg = "SELECT ROUND(AVG(mara_evaluation.total),1) as score FROM `mara_evaluation` 
				WHERE mara_evaluation.total != 0
					".
					($where != NULL ? " AND {$where}" : "")
					." GROUP BY mara_evaluation.innovation_id";

		$query = $this->db->query($arg);
            
        return $query;
	}
}

?>