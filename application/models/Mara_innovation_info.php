<?php

require_once(APPPATH . 'models/General_model.php');
class Mara_innovation_info extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "mara_innovation_info";
		$this->primary_field = "id";
	}

	function update_by_innovation($innovation_id, $data){
		$this->db->where('innovation_id',$innovation_id);
		return $this->db->update('mara_innovation_info',$data);
	}
}

?>