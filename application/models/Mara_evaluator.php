<?php

require_once(APPPATH . 'models/General_model.php');
class Mara_evaluator extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "mara_evaluator";
		$this->primary_field = "evaluator_id";
	}
	
	function get_with_user($where = NULL){
		$this->db->select('user.*, mara_evaluator.*, mara_group_expert.name as group_name');
		$this->db->from('mara_evaluator');
		$this->db->join('user', 'user.user_id = mara_evaluator.evaluator_id');
		$this->db->join('mara_group_evaluator', 'mara_group_evaluator.mara_evaluator_id = mara_evaluator.evaluator_id');
		$this->db->join('mara_group_expert', 'mara_group_evaluator.mara_group_expert_id = mara_group_expert.id');
		if($where != NULL){
			$this->db->where($where);
		}

		$q = $this->db->get();
		return $q->result_array();
	}
}

?>