<?php

require_once(APPPATH . 'models/General_model.php');
class Mara_forgot_password_token extends General_model {
	function __construct() {
		parent::__construct();		
		$this->table_name = "mara_forgot_password_token";
		$this->primary_field = "";
	}

	public function insert_token($data){
        return $this->db->insert($this->table_name, $data);
    }

    public function delete_where($where){
        $this->db->where($where);
        return $this->db->delete($this->table_name);
    }
}

?>