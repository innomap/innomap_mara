<div class="col-xs-10 col-sm-10 col-md-10 col-md-offset-1 margin-top-55">
	<div class="col-md-12">
		<div class="col-md-6 dashboard-box dashboard-box-logo">
			<img src="<?= base_url().ASSETS_IMG."logo_maratex.png" ?>" width="100%">
		</div>
		<div class="col-md-2 dashboard-box bg-blue-1"></div>
		<div class="col-md-2 dashboard-box bg-grey-1 dashboard-box-year">2016</div>
		<div class="col-md-2 dashboard-box bg-transparent"></div>

		<div class="col-md-4 dashboard-box no-padding bg-blue-2 opacity-05">
			<img src="<?= base_url().ASSETS_IMG."dashboard-pic-1.png" ?>" width="100%" height="100%">
		</div>
		<div class="col-md-4 dashboard-box bg-grey-2 dashboard-box-motto text-center">"Exploring Talent, Reaping Opportunity"</div>
		<div class="col-md-2 dashboard-box bg-blue-2"></div>
		<div class="col-md-2 dashboard-box no-padding">
			<img src="<?= base_url().ASSETS_IMG."dashboard-pic-2.png" ?>" width="100%" height="100%">
		</div>

		<div class="col-md-2 dashboard-box bg-transparent"></div>
		<div class="col-md-2 dashboard-box bg-grey-1"></div>
		<div class="col-md-2 dashboard-box no-padding">
			<img src="<?= base_url().ASSETS_IMG."dashboard-pic-3.png" ?>" width="100%" height="100%">
		</div>
		<div class="col-md-4 dashboard-box bg-transparent"></div>
		<div class="col-md-2 dashboard-box bg-grey-3 opacity-05"></div>

		<div class="col-md-12 dashboard-contact-us no-padding">
			<div class="col-md-1">
				<img src="<?= base_url().ASSETS_IMG."ic-contact-us.png" ?>" width="100%">
			</div>
			<div class="col-md-11">
				<p><span>Tn. Hj Rosidi bin Ishak</span> | <a href="mailto:rosidi@mara.gov.my">rosidi@mara.gov.my</a></p>
				<p><span>Mazinah Joary Binti Md Tajuddin</span> | <a href="mailto:mazinah@mara.gov.my">mazinah@mara.gov.my</a></p>
				<p><span>Khairiah binti Abdullah</span> | <a href="mailto:khairiah.abdullah@mara.gov.my">khairiah.abdullah@mara.gov.my</a></p>
			</div>
		</div>
	</div>
</div>

<div class="col-md-2 no-padding dashboard-logo-wrap text-right margin-top-20">
	<img src="<?= base_url().ASSETS_IMG."logo-mara.png" ?>" width="15%">
	<img src="<?= base_url().ASSETS_IMG."logo-50-th.png" ?>" width="25%">
	<img src="<?= base_url().ASSETS_IMG."logo-mini.png" ?>" width="20%">
</div>