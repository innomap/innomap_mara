<script type="text/javascript">
	var alert = "<?= $alert ?>";
</script>
<div class="col-md-12 container">
	<div class="col-md-8 col-md-offset-2 text-center">
		<div class="col-xs-6 col-md-6 link-logo">
			<a href="<?= base_url() ?>">
				<img src="<?= base_url().ASSETS_IMG."logo_maratex.png" ?>" width="70%" class="padding-top-10">
			</a>
		</div>
		<div class="col-xs-6 col-md-6 link-logo">
			<a href="<?= base_url() ?>">
				<img src="<?= base_url().ASSETS_IMG."logo-aim-2016.png" ?>" width="40%">
			</a>
		</div>
	</div>
	<div class="col-md-4 col-md-offset-4 margin-top-55">		
		<div class="col-md-12 login-box bg-grey-2">
			<div class="alert alert-info alert-dismissable hide">
			    <i class="fa fa-info"></i>
			    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			    <?= $alert ?>
			</div>
			<form id="form-login" action="<?= base_url().'accounts/login_auth' ?>" method="POST">
				<div class="form-group">
					<input type="text" class="form-control" name="email" placeholder="<?= lang('email') ?>">
				</div>

				<div class="form-group">
					<input type="password" class="form-control" name="password" placeholder="<?= lang('password') ?>">
				</div>

				<div class="form-group text-center">
					<input type="submit" class="btn btn-success rounded" name="btn_login" value="Log In">
				</div>
			</form>
			<div class="col-md-12 no-padding">
				<div class="col-md-6 col-md-offset-6 text-right no-padding">
					<a href="<?= base_url().'forgot_password' ?>"><?= lang('forgot_password') ?></a>
				</div>
			</div>
		</div>
	</div>
</div>

