<script type="text/javascript">
	var alert = "<?= $alert ?>";
</script>
<div class="col-md-12 container">
	<div class="col-md-8 col-md-offset-2 text-center">
		<div class="col-xs-6 col-md-6 link-logo">
			<a href="<?= base_url() ?>">
				<img src="<?= base_url().ASSETS_IMG."logo_maratex.png" ?>" width="70%" class="padding-top-10">
			</a>
		</div>
		<div class="col-xs-6 col-md-6 link-logo">
			<a href="<?= base_url() ?>">
				<img src="<?= base_url().ASSETS_IMG."logo-aim-2016.png" ?>" width="40%">
			</a>
		</div>
	</div>
	<div class="col-md-6 col-md-offset-3">		
		<div class="col-md-12 text-center">
			<h2><?= lang('reset_password') ?></h2>
		</div>
		<div class="col-md-12 login-box bg-grey-2">
			<div class="alert alert-info alert-dismissable hide">
			    <i class="fa fa-info"></i>
			    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			    <?= $alert ?>
			</div>
			<form id="form-forgot-password" action="<?= base_url().'accounts/forgot_password_handler' ?>" method="POST">
				<p><?= lang('forgot_password_msg') ?></p>
				<div class="form-group">
					<input type="text" class="form-control" name="email" placeholder="<?= lang('email') ?>">
				</div>

				<div class="form-group text-center">
					<input type="submit" class="btn btn-success rounded" name="btn_login" value="<?= lang('send_email') ?>">
				</div>
			</form>
			<div class="col-md-12 text-center">
				<a href="<?= base_url().'login' ?>">Log In</a>
			</div>
		</div>
	</div>
</div>

