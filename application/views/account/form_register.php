<script type="text/javascript">
	var alert = "<?= $alert ?>";
</script>

<div class="col-md-12 container">
	<div class="col-md-8 col-md-offset-2 text-center">
		<div class="col-xs-6 col-md-6 link-logo">
			<a href="<?= base_url() ?>">
				<img src="<?= base_url().ASSETS_IMG."logo_maratex.png" ?>" width="70%" class="padding-top-10">
			</a>
		</div>
		<div class="col-xs-6 col-md-6 link-logo">
			<a href="<?= base_url() ?>">
				<img src="<?= base_url().ASSETS_IMG."logo-aim-2016.png" ?>" width="40%">
			</a>
		</div>
	</div>
	<div class="col-md-6 col-md-offset-3">
		<div class="col-md-12 text-center">
			<h2><?= lang('registration_form') ?></h2>
		</div>

		<div class="col-md-12 login-box bg-grey-2">
			<div class="alert alert-info alert-dismissable hide">
			    <i class="fa fa-info"></i>
			    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			    <?= $alert ?>
			</div>

			<form id="form-register" action="<?= base_url().'accounts/register_handler' ?>" method="POST" enctype="multipart/form-data">
				<div class="form-group">
					<input type="text" class="form-control" name="email" placeholder="<?= lang('email') ?>">
				</div>

				<div class="form-group">
					<input type="password" class="form-control" name="password" placeholder="<?= lang('password') ?>">
				</div>

				<div class="form-group">
					<input type="password" class="form-control" name="retype_password" placeholder="<?= lang('retype_password') ?>">	
				</div>

				<div class="form-group">
					<input type="text" class="form-control" name="name" placeholder="<?= lang('name') ?>">
				</div>

				<div class="form-group">
					<input type="text" class="form-control" name="kp_no" placeholder="<?= lang('no_kp') ?>">
				</div>

				<div class="form-group">
					<input type="text" class="form-control" name="telp_no" placeholder="<?= lang('telp_no') ?>">
				</div>

				<div class="form-group">
					<input type="text" class="form-control" name="fax_no" placeholder="<?= lang('fax_no') ?>">
				</div>

				<div class="form-group">
					<input type="text" class="form-control" id="mc-keyword" name="mara_center_keyword" data-provide="typeahead" autocomplete="off" placeholder="<?= lang('mara_center') ?>">	
				</div>
				<input type="hidden" name="mara_center" id="mc-id"/>

				<div class="form-group">
					<input type="text" class="form-control" name="mara_address" placeholder="<?= lang('mara_address') ?>">
				</div>

				<div class="form-group">
					<input type="text" class="form-control" name="mara_telp_no" placeholder="<?= lang('mara_telp_no') ?>">
				</div>

				<div class="form-group">
					<input type="text" class="form-control" name="mara_fax_no" placeholder="<?= lang('mara_fax_no') ?>">
				</div>

				<div class="form-group">
					<input type="text" class="form-control" name="staff_id" placeholder="<?= lang('staff_id') ?>">
				</div>

				<div class="form-group file-field">
					<label><?= lang('photo') ?></label>
					<input type="file" class="form-control" name="innovator_photo">
				</div>

				<div class="form-group text-center">
					<input type="submit" name="btn_register" class="btn btn-success rounded" value="<?= lang('register') ?>">
				</div>
			</form>

			<div class="col-md-12 text-center">
				<span><?= lang('already_have_account') ?> <a href="<?= base_url().'login' ?>">Log In</a></span>
			</div>
		</div>
	</div>
</div>