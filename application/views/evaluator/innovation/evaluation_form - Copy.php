<script type="text/javascript">
	var alert = "<?= $alert ?>",
		view_mode = "<?= $view_mode; ?>";
</script>
<div class="col-md-10 col-md-offset-1 content">
	<div class="col-md-12 text-center">
		<h2><?= lang('evaluation_form') ?></h2>
	</div>

	<div class="col-md-12">
		<div class="alert alert-info alert-dismissable hide">
		    <i class="fa fa-info"></i>
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		    <?= $alert ?>
		</div>

		<form action="<?= base_url().PATH_TO_EVALUATOR.'innovations/evaluation_handler' ?>" id="form-evaluation" method="post">
			<input type="hidden" name="evaluation_id" value="<?= (isset($evaluation) ? $evaluation['id'] : 0) ?>">
			
			<table border="1" cellspacing="0" cellpadding="0" id="table-evaluation-form">
				<tr>
					<td><?= lang('evaluator') ?></td>
					<td colspan="2">
						<?= $evaluator['name'] ?>
						<input type="hidden" name="evaluator" value="<?= $evaluator['evaluator_id'] ?>">
					</td>
				</tr>
				<tr>
					<td><?= lang('innovation') ?></td>
					<td colspan="2">
						<?= $innovation['name_in_melayu'] ?>
						<input type="hidden" name="innovation_id" value="<?= $innovation['innovation_id'] ?>">
					</td>
				</tr>
				<tr class="head">
					<th style="width:20%"><b><?= lang('evaluation_focus') ?></b></th>
					<th style="width:55%"><b><?= lang('criteria') ?></b></th>
					<th><b><?= lang('marks') ?></b></th>
				</tr>
				<tr>
					<td><b><?= lang('hip6_label_fa_1') ?></b></td>
					<td colspan="2">
						<table>
							<tr><td><b><?= lang('hip6_label_cr_1_1') ?></b></td></tr>
							<tr>
								<td width="80%"><?= lang('hip6_label_desc_1_1') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
								<td width="20%">
									<div class="form-group">
										<input type="text" class="form-control" id="he-originality-innovation" name="originality_innovation" value="<?= (isset($evaluation) ? $evaluation['originality_innovation'] : 0) ?>">
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><b><?= lang('hip6_label_fa_2') ?></b></td>
					<td colspan="2">
						<table>
							<tr><td><b><?= lang('hip6_label_cr_2_1') ?></b></td></tr>
							<tr>
								<td width="80%"><?= lang('hip6_label_desc_2_1') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
								<td width="20%">
									<div class="form-group">
										<input type="text" class="form-control" id="he-comparable" name="comparable" value="<?= (isset($evaluation) ? $evaluation['comparable'] : 0) ?>">
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td></td>
					<td colspan="2">
						<table>
							<tr><td><b><?= lang('hip6_label_cr_2_2') ?></b></td></tr>
							<tr>
								<td width="80%"><?= lang('hip6_label_desc_2_2') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
								<td width="20%">
									<div class="form-group">
										<input type="text" class="form-control" id="he-innovation-impact" name="innovation_impact" value="<?= (isset($evaluation) ? $evaluation['innovation_impact'] : 0) ?>">
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><b><?= lang('hip6_label_fa_3') ?></b></td>
					<td colspan="2">
						<table>
							<tr><td><b><?= lang('hip6_label_cr_3_1') ?></b></td></tr>
							<tr>
								<td width="80%"><?= lang('hip6_label_desc_3_1') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
								<td width="20%">
									<div class="form-group">
										<input type="text" class="form-control" id="he-unit-cost" name="unit_cost" value="<?= (isset($evaluation) ? $evaluation['unit_cost'] : 0) ?>">
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td></td>
					<td colspan="2">
						<table>
							<tr><td><b><?= lang('hip6_label_cr_3_2') ?></b></td></tr>
							<tr>
								<td width="80%"><?= lang('hip6_label_desc_3_2') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
								<td width="20%">
									<div class="form-group">
										<input type="text" class="form-control" id="he-cost-of-assistance" name="cost_of_assistance" value="<?= (isset($evaluation) ? $evaluation['cost_of_assistance'] : 0) ?>">
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><b><?= lang('hip6_label_fa_4') ?></b></td>
					<td colspan="2">
						<table>
							<tr><td><b><?= lang('hip6_label_cr_4_1') ?></b></td></tr>
							<tr>
								<td width="80%"><?= lang('hip6_label_desc_4_1') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
								<td width="20%">
									<div class="form-group">
										<input type="text" class="form-control" id="he-excluded-group" name="excluded_group" value="<?= (isset($evaluation) ? $evaluation['excluded_group'] : 0) ?>">
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><b><?= lang('hip6_label_fa_5') ?></b></td>
					<td colspan="2">
						<table>
							<tr><td><b><?= lang('hip6_label_cr_5_1') ?></b></td></tr>
							<tr>
								<td width="80%"><?= lang('hip6_label_desc_5_1') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
								<td width="20%">
									<div class="form-group">
										<input type="text" class="form-control" id="he-market-size" name="market_size" value="<?= (isset($evaluation) ? $evaluation['market_size'] : 0) ?>">
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><b><?= lang('hip6_label_fa_6') ?></b></td>
					<td colspan="2">
						<table>
							<tr><td><b><?= lang('hip6_label_cr_6_1') ?></b></td></tr>
							<tr>
								<td width="80%"><?= lang('hip6_label_desc_6_1') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
								<td width="20%">
									<div class="form-group">
										<input type="text" class="form-control" id="he-attitude-of-entrepreneur" name="attitude_of_entrepreneur" value="<?= (isset($evaluation) ? $evaluation['attitude_of_entrepreneur'] : 0) ?>">
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td></td>
					<td colspan="2">
						<table>
							<tr><td><b><?= lang('hip6_label_cr_6_2') ?></b></td></tr>
							<tr>
								<td width="80%"><?= lang('hip6_label_desc_6_2') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
								<td width="20%">
									<div class="form-group">
										<input type="text" class="form-control" id="he-diffusion-platform" name="diffusion_platform" value="<?= (isset($evaluation) ? $evaluation['diffusion_platform'] : 0) ?>">
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td></td>
					<td colspan="2">
						<table>
							<tr><td><b><?= lang('hip6_label_cr_6_3') ?></b></td></tr>
							<tr>
								<td width="80%"><?= lang('hip6_label_desc_6_3') ?> <div class="label_max"><i><?=lang('label_max_value'); ?> 10</i></div></td>
								<td width="20%">
									<div class="form-group">
										<input type="text" class="form-control" id="he-strategic-linkages" name="strategic_linkages" value="<?= (isset($evaluation) ? $evaluation['strategic_linkages'] : 0) ?>">
									</div>
								</td>
							</tr>
							<tr>
								<td class="clm-total" width="80%"><b><?= lang('label_total') ?></b></td>
								<td width="20%">
									<div class="form-group">
										<input type="text" class="form-control" id="he-total" name="total" value="<?= (isset($evaluation) ? $evaluation['total'] : 0) ?>" readonly><div class="action_wrap"><span id="score-balance"><?= (isset($evaluation) ? 100-$evaluation['total'] : 100); ?></span> points available</div>
									</div>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>

			<div class="form-group">
				<label><?= lang('label_comment') ?></label>
				<textarea class="form-control" placeholder="<?= lang('label_comment') ?>" rows="5" name="comment"><?=(isset($evaluation) ? $evaluation['comment'] : '')?></textarea>
			</div>

			<div class="col-md-12 text-center">
				<button type="button" class="btn btn-default flat btn-cancel"><?= lang('cancel') ?></button>
				<input type="submit" class="btn btn-success flat" value="<?= lang('save') ?>">
			</div>
		</form>
	</div>
</div>