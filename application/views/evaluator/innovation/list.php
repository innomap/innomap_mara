<script type="text/javascript">
	var alert = "<?=$alert?>";
</script>
<div class="col-md-12">
    <div class="col-md-12 table-responsive">
		<div class="alert alert-info alert-dismissable hide">
		    <i class="fa fa-info"></i>
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		    <?= $alert ?>
		</div>
		
		<form id="form-list-evaluation" action="<?= base_url().PATH_TO_EVALUATOR.'innovations/approve' ?>" method="post">
			<table id="table-innovation" class="table table-bordered table-striped table-dark">
			    <thead>
				    <tr>
						<th>No</th>
						<th><?= lang('innovator') ?></th>
						<th><?= lang('innovation_product') ?></th>
						<th><?= lang('description') ?></th>
						<th><?= lang('status') ?></th>
						<th><?= lang('action') ?></th>
				    </tr>
			    </thead>
			    <tbody>
			    <?php $no = 1; foreach ($innovations as $innovation) { ?>
				<tr>
				    <td><?= $no ?></td>
				    <td><?= $innovation['innovator'] ?></td>
				    <td><?= $innovation['name_in_melayu'] ?></td>
				    <td><?= ellipsis($innovation['description_in_melayu'], 100) ?></td>
				    <td class="<?= $status[$innovation['status']]['class'] ?>"><?= $status[$innovation['status']]['name'] ?></td>
				    <td>
						<button type="button" class="btn btn-default btn-action btn-view-innovation" title="<?= lang('view_detail') ?>" data-id="<?= $innovation['innovation_id'] ?>">
						    <span class="fa fa-search fa-lg"></span>
						</button>

						<?php if(!$innovation['has_evaluation_id'] || !$innovation['is_complete']){ ?>
							<button type="button" class="btn btn-default btn-action btn-evaluate-innovation" title="<?= lang('evaluate') ?>" data-id="<?= $innovation['innovation_id'] ?>">
							    <span class="fa fa-edit fa-lg"></span>
							</button>
						<?php }else{ ?>
							<button type="button" class="btn btn-default btn-action btn-view-evaluation" title="<?= lang('view_evaluation_result') ?>" data-id="<?= $innovation['innovation_id'] ?>">
							    <span class="fa fa-file-text-o fa-lg"></span>
							</button>
						<?php } ?>
						
				    </td>
				</tr>
			    <?php $no++;} ?>
			    </tbody>
			</table>
		</form>
    </div>
</div>