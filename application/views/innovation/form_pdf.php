<style>
table tr td table th{
	background: #888d97;
	color:#FFF;
}

table tr td table.bordered, table tr td table.bordered tr td{
	border:1px solid #ddd !important;
}
</style>

<table border="0" width="100%">
	<tr>
		<td><img src="<?= base_url().ASSETS_IMG."logo_maratex.png" ?>" width="200%"></td>
		<td><img src="<?= base_url().ASSETS_IMG."logo-aim-2016.png" ?>" width="100%"></td>
	</tr>
</table>

<table border="0" width="100%">
	<tr>
		<td colspan="2" style="text-align:center"><h2><?= lang('innovation_form') ?></h2></td>
	</tr>
	<tr>
		<td width="30%"><?= lang('mara_center') ?> : </td>
		<td width="70%"><?= $innovator['mara_center'] ?></td>
	</tr>
	<tr>
		<td><?= lang('mara_address') ?> : </td>
		<td><?= $innovator['mara_address'] ?></td>
	</tr>
	<tr>
		<td><?= lang('mara_telp_no') ?> : </td>
		<td><?= $innovator['mara_telp_no'] ?></td>
	</tr>
	<tr>
		<td><?= lang('mara_fax_no') ?> : </td>
		<td><?= $innovator['mara_fax_no'] ?></td>
	</tr>
	<tr>
		<td><?= lang('application_category') ?> : </td>
		<td>
			<?php foreach ($categories_produk_level as $key => $value) {
				echo $i_categories['level'] == $key ? $value : '';
			} ?>
		</td>
	</tr>
	<tr>
		<td><?= lang('category') ?> : </td>
		<td>
			<?php foreach ($categories as $key => $value) {
				echo $i_categories['category'] == $key ? $value : "";
			} ?>
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<?php if($i_categories['category'] == 0){
				$areas = $categories_services_area;
			}else{
				$areas = $categories_produk_area;
			} 

			foreach ($areas as $key => $value) {
				echo ($i_categories['area'] == $key ? $value : "");
			}

			?>
		</td>
	</tr>

	<tr>
		<td><?= lang('innovator_photo') ?> : </td>
		<td>
			<?php if(file_exists(realpath(APPPATH . '../'.PATH_TO_INNOVATOR_PHOTO) . DIRECTORY_SEPARATOR . $innovator['photo']) && $innovator['photo'] != "") { ?>
					<img src="<?= base_url().PATH_TO_INNOVATOR_PHOTO_THUMB.$innovator['photo'] ?>" width="80%"/>
			<?php } ?>
		</td>
	</tr>

	<tr>
		<td><?= lang('innovator') ?> : </td>
		<td><?= $innovator['name'] ?></td>
	</tr>

	<tr>
		<td><?= lang('no_kp') ?> : </td>
		<td><?= $innovator['kp_no'] ?></td>
	</tr>

	<tr>
		<td><?= lang('email') ?> : </td>
		<td><?= $innovator['email'] ?></td>
	</tr>

	<tr>
		<td><?= lang('telp_no') ?> : </td>
		<td><?= $innovator['telp_no'] ?></td>
	</tr>

	<tr>
		<td><?= lang('fax_no') ?> : </td>
		<td><?= $innovator['fax_no'] ?></td>
	</tr>

	<?php if($i_categories['category'] != 0){ ?>
		<tr>
			<td><?= lang('instructor_name') ?> : </td>
			<td><?= $i_categories['instructor_name'] ?></td>
		</tr>

		<tr>
			<td><?= lang('instructor_staff_id') ?> : </td>
			<td><?= $i_categories['instructor_staff_id'] ?></td>
		</tr>

		<tr>
			<td><?= lang('instructor_kp_no') ?> : </td>
			<td><?= $i_categories['instructor_kp_no'] ?></td>
		</tr>
	<?php } ?>

	<tr>
		<td><?= lang('innovation_product') ?> : </td>
		<td><?= $innovation['name_in_melayu'] ?></td>
	</tr>

	<?php 
		$length = 1000;
		$desc_length = ceil(strlen($innovation['inspiration'])/$length);
		$start = 0;
	for($i=0;$i < $desc_length;$i++) { ?>
	<tr>
		<td><?= ($i == 0 ? lang('inspiration')." : " : "") ?> </td>
		<td><?= substr($innovation['inspiration'],$start,$length); ?></td>
	</tr>
	<?php $start = $length*($i+1);} ?>

	<?php 
		$length = 1000;
		$desc_length = ceil(strlen($innovation['description'])/$length);
		$start = 0;
	for($i=0;$i < $desc_length;$i++) { ?>
	<tr>
		<td><?= ($i == 0 ? lang('description')." : " : "") ?> </td>
		<td><?= substr($innovation['description'],$start,$length); ?></td>
	</tr>
	<?php $start = $length*($i+1);} ?>

	<?php 
		$length = 1000;
		$desc_length = ceil(strlen($innovation['materials'])/$length);
		$start = 0;
	for($i=0;$i < $desc_length;$i++) { ?>
	<tr>
		<td><?= ($i == 0 ? lang('materials')." : " : "") ?> </td>
		<td><?= substr($innovation['materials'],$start,$length); ?></td>
	</tr>
	<?php $start = $length*($i+1);} ?>

	<?php 
		$length = 1000;
		$desc_length = ceil(strlen($innovation['how_to_use'])/$length);
		$start = 0;
	for($i=0;$i < $desc_length;$i++) { ?>
	<tr>
		<td><?= ($i == 0 ? lang('how_to_use')." : " : "") ?> </td>
		<td><?= substr($innovation['how_to_use'],$start,$length); ?></td>
	</tr>
	<?php $start = $length*($i+1);} ?>

	<tr>
		<td><?= lang('special_achievement') ?> : </td>
		<td><?= $innovation['special_achievement'] ?></td>
	</tr>

	<tr>
		<td><?= lang('created_date') ?> : </td>
		<td><?= $innovation['created_date'] ?></td>
	</tr>

	<tr>
		<td><?= lang('discovered_date') ?> : </td>
		<td><?= $innovation['discovered_date'] ?></td>
	</tr>

	<tr>
		<td><?= lang('manufacturing_cost') ?> : </td>
		<td><?= "RM ".$innovation['manufacturing_costs'] ?></td>
	</tr>

	<tr>
		<td><?= lang('selling_price') ?> : </td>
		<td><?= "RM ".$innovation['selling_price'] ?></td>
	</tr>

	<tr>
		<td><?= lang('target') ?> : </td>
		<td>
			<?php foreach ($targets as $key => $value) {
					foreach ($i_target as $val) {
						echo ($val == $key ? "- ".$value."<br/>" : '');
					}
				}
			?>
		</td>
	</tr>

	<tr>
		<td><?= lang('myipo_protection') ?> : </td>
		<td><?= $innovation['myipo_protection'] == 1 ? "Ya" : "Tiada" ?></td>
	</tr>

	<?php foreach ($i_picture as $key => $value) { 
			if(file_exists(realpath(APPPATH . '../'.PATH_TO_INNOVATION_PICTURE_THUMB) . DIRECTORY_SEPARATOR . $value['picture'])) {
				$url = site_url() . 'assets/attachment/innovation_picture/thumbnail/' . $value['picture'];
			}else{
				$url = site_url() . 'assets/attachment/innovation_picture/thumbnail/default.png';
			} ?>
	<tr>
		<td><?= ($key == 0 ? lang('picture')." : " : '') ?></td>
		<td>
			<img src="<?= $url ?>" width="80%">
		</td>
	</tr>
	<?php } ?>

	<tr>
		<td><?= lang('keyword') ?> : </td>
		<td><?= $i_keyword ?></td>
	</tr>

	<tr>
		<td colspan="2"><?= lang('team_member') ?> : </td>
	</tr>

	<tr>
		<td colspan="2">
			<table border="1" class="bordered" width="100%">
				<tr>
					<th>No</th>
					<th><?= lang('photo') ?></th>
					<th><?= lang('member_name') ?></th>
					<th><?= lang('no_kp') ?></th>
					<th><?= lang('telp_no') ?></th>
				</tr>
			<?php $no=1;
				foreach ($i_team as $key => $value) {
					if($value['photo']){
						if(file_exists(realpath(APPPATH . '../'.PATH_TO_TEAM_MEMBER_PHOTO_THUMB) . DIRECTORY_SEPARATOR . $value['photo'])) {
							$url = site_url() . PATH_TO_TEAM_MEMBER_PHOTO_THUMB . $value['photo'];
						}else{
							$url = "";
						}
					}else{ $url = ""; } ?>

					<tr>
						<td><?= $no; ?></td>
						<td><?= ($url != "" ? "<img src='".$url."' width='50%'>" : "-") ?></td>
						<td><?= $value['name']; ?></td>
						<td><?= $value['kp_no']; ?></td>
						<td><?= $value['telp_no']; ?></td>
					</tr>

			<?php $no++;} ?> 
			</table>
		</td>
	</tr>

	<tr>
		<td colspan="2"><?= lang('heir') ?> : </td>
	</tr>

	<tr>
		<td colspan="2">
			<table border="1" class="bordered" width="100%">
				<tr>
					<th><?= lang('name') ?></th>
					<th><?= lang('no_kp') ?></th>
					<th><?= lang('telp_no') ?></th>
				</tr>
				<tr>
					<td><?= $i_info['heir_name'] ?></td>
					<td><?= $i_info['heir_kp_no'] ?></td>
					<td><?= $i_info['heir_telp_no'] ?></td>
				</tr>
			</table>
		</td>
	</tr>

	<tr>
		<td><?= lang('validation_status') ?> : </td>
		<td><?= ($i_info['validation_attachment'] != "" ? lang('has_validation_msg') : lang('has_not_validation_msg')) ?>
		</td>
	</tr>
</table>