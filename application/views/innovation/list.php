<script type="text/javascript">
	var alert = "<?=$alert?>",
		alert_dialog = "<?= $alert_dialog ?>";
	var lang_delete = "<?= lang('delete') ?>",
		lang_delete_innovation = "<?= lang('delete_innovation') ?>",
		lang_cancel = "<?= lang('cancel') ?>",
		lang_delete_confirm_message = "<?= lang('delete_confirm_message') ?>",
		lang_application_submitted_msg = "<?= lang('application_submitted_msg') ?>";
</script>
<div class="col-md-12">
	<div class="col-md-8 col-md-offset-2">
		<?php $this->load->view('partial/logo_after_login',array('title' => '')); ?>
	</div>	
    <div class="col-md-12">
		<a href="<?= base_url().'innovations/add' ?>">
			<button type="button" class="btn btn-success">
				<span class="fa fa-plus"></span> <?= lang('new_innovation') ?>
			</button>
		</a>
    </div>

    <div class="col-md-12 table-responsive content-box">
		<div class="alert alert-info alert-dismissable hide">
		    <i class="fa fa-info"></i>
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		    <?= $alert ?>
		</div>
		<table id="table-innovation" class="table table-bordered table-striped table-dark">
		    <thead>
		    <tr>
				<th>No</th>
				<th><?= lang('innovation_product') ?></th>
				<th><?= lang('description') ?></th>
				<th><?= lang('status') ?></th>
				<th><?= lang('action') ?></th>
		    </tr>
		    </thead>
		    <tbody>
		    <?php $no = 1; foreach ($innovations as $innovation) { ?>
			<tr>
			    <td><?= $no ?></td>
			    <td><?= $innovation['name_in_melayu'] ?></td>
			    <td><?= ellipsis($innovation['description_in_melayu'], 100) ?></td>
			    <td class="<?= $status[$innovation['status']]['class'] ?>"><?= $innovation['round']." : ".$status[$innovation['status']]['name'] ?></td>
			    <td>
			    <?php 	if($innovation['status'] == INNO_STATUS_DRAFT && $innovation['round_id'] == 1){ ?>
							<button type="button" class="btn btn-default btn-action btn-edit-innovation" title="<?= lang('edit') ?>" data-id="<?= $innovation['innovation_id'] ?>">
							    <span class="fa fa-pencil fa-lg"></span>
							</button>

							<button type="button" class="btn btn-default btn-action btn-delete-innovation text-red" data-id="<?= $innovation['innovation_id'] ?>"
							data-name="<?= $innovation['name_in_melayu'] ?>" title="<?= lang('delete') ?>">
							    <span class="fa fa-trash-o fa-lg"></span>
							</button>
				<?php 	}
						if(($innovation['status'] != INNO_STATUS_DRAFT && $innovation['round_id'] == 1) || $innovation['round_id'] > 1){ ?>
							<button type="button" class="btn btn-default btn-action btn-view-innovation" title="<?= lang('view_detail') ?>" data-id="<?= $innovation['innovation_id'] ?>">
							    <span class="fa fa-search fa-lg"></span> 
							</button>
				<?php 	}
						if(($innovation['status'] == INNO_STATUS_APPROVED && $innovation['round_id'] == 1) || ($innovation['round_id'] > 1 && $innovation['status'] == INNO_STATUS_DRAFT)){ ?>
							<button type="button" class="btn btn-default btn-action btn-business-plan" title="<?= lang('fill_form_business_plan') ?>" data-id="<?= $innovation['innovation_id'] ?>">
							    <span class="fa fa-edit fa-lg"></span>
							</button>
				<?php 	}
						if($innovation['status'] >= INNO_STATUS_SENT_APPROVAL && $innovation['round_id'] > 1){ ?>
							<button type="button" class="btn btn-default btn-action btn-view-business-plan" title="<?= lang('form_business_plan') ?>" data-id="<?= $innovation['innovation_id'] ?>">
							    <span class="fa fa-file-text-o fa-lg"></span> 
							</button>
				<?php 	} ?>
			    </td>
			</tr>
		    <?php $no++;} ?>
		    </tbody>
		</table>
    </div>

    <div class="col-md-12">
		<div class="panel panel-danger">
			<div class="panel-heading text-center"><b><?= lang('application_announcement_msg') ?></b></div>
		</div>
	</div>
</div>