<style>
.text-center{
	text-align: center;
	font-family: "Helvetica";
}
.text-right{
	text-align: right;
}
.text-justify{
	text-align: justify;
}
.padding-top-30{
	padding-top: 30px;
}

.padding-top-15{
	padding-top: 15px;
}

.padding-top-60{
	padding-top: 60px;
}

.padding-right-30{
	padding-right: 30px;
}

.approval-field tr td{
	padding-bottom: 15px;
}
</style>
<table border="0" width="100%">
	<tr>
		<td class="text-center padding-top-60"><img src="<?= base_url().ASSETS_IMG."logo_maratex.png" ?>" width="270%"></td>
	</tr>
	<tr>
		<td class="text-center"><b>EXPLORING TALENT. REAPING OPPORTUNITY</b></td>
	</tr>
	<tr>
		<td class="text-center padding-top-15"><img src="<?= base_url().ASSETS_IMG."logo-aim-2016.png" ?>" width="270%"></td>
	</tr>
	<tr>
		<td class="text-center padding-top-30"><b>PENGESAHAN PEGAWAI MENGAWAL PUSAT (PMP)</b></td>
	</tr>
	<tr>
		<td class="text-justify padding-top-15" style="line-height: 200%;">Saya mengesahkan <b><?= $innovator['name'] ?></b> adalah ketua kumpulan bagi
projek <?= ($innovation != "" ? "<b>".$innovation."</b>" : '................................................................................................................') ?> dari
 <b><?= $innovator['mara_center'] ?></b> (nama pusat).</td>
	</tr>
</table>

<table border="0" width="100%">
	<tr>
		<td colspan="2">
			<table class="approval-field">
				<tr>
					<td class="padding-top-30">Tandatangan <br/> (Pegawai Pengawal Pusat)</td>
					<td>: ......................................................</td>
				</tr>
				<tr>
					<td>Nama</td>
					<td>: ......................................................</td>
				</tr>
				<tr>
					<td>Jawatan</td>
					<td>: ......................................................</td>
				</tr>
				<tr>
					<td>Tarikh</td>
					<td>: ......................................................</td>
				</tr>
			</table>
		</td>
		<td><img src="<?= base_url().ASSETS_IMG."PMP-cap-logo.png" ?>" width="100%"></td>
	</tr>
	<tr>
		<td colspan="3" class="text-right padding-top-50 padding-right-30"><img src="<?= base_url().ASSETS_IMG."logo-50-th.png" ?>" width="100%"></td>
	</tr>
</table>