<script type="text/javascript">
	var alert = "<?= $alert ?>",
		alert_dialog = "",
		lang_send_for_approval_confirm_message = "<?= lang('send_business_plan_for_approval_confirm_message') ?>";
	var	lang_send_innovation_for_approval = "<?= lang('send_business_plan_for_approval') ?>",
		lang_cancel = "<?= lang('cancel') ?>",
		lang_send_for_approval = "<?= lang('send_for_approval') ?>",
		lang_yes = "<?= lang('yes') ?>",
		lang_application_submitted_msg = "<?= lang('business_plan_submitted_msg') ?>";

	var view_mode = "<?= ($view_mode == 1 ? true : false) ?>";
</script>
<div class="col-md-8 col-md-offset-2">
	<?php $this->load->view('partial/logo_after_login',array('title' => lang('form_business_plan'))); ?>

	<div class="alert alert-info alert-dismissable hide">
	    <i class="fa fa-info"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <?= $alert ?>
	</div>

	<form id="form-business-plan" class="form-horizontal" action="<?= base_url().$action_url ?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="innovation_id" value="<?= (isset($innovation) ? $innovation['innovation_id'] : 0) ?>">
		<input type="hidden" name="business_plan_id" value="<?= (isset($business_plan) ? $business_plan['id'] : 0) ?>">
		<input type="hidden" name="round_id" value="<?= (isset($round) ? $round['id'] : 0) ?>">

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('innovation_product') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" name="innovation_name" placeholder="<?= lang('innovation_product') ?>" value="<?= (isset($innovation) ? $innovation['name_in_melayu'] : "") ?>" readonly>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('description') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_bp_description') ?>"><i class="fa fa-question-circle"></i></span></label>
			<div class="col-md-9">
				<textarea class="form-control" name="description" rows="5" placeholder="<?= lang('description') ?>"><?= (isset($business_plan) ? $business_plan['description'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('the_main_issue_addressed_by_innovation') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_bp_problem') ?>"><i class="fa fa-question-circle"></i></span></label>
			<div class="col-md-9">
				<textarea class="form-control" name="problem" rows="5" placeholder="<?= lang('the_main_issue_addressed_by_innovation') ?>"><?= (isset($business_plan) ? $business_plan['problem'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('target_user') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_bp_target_user') ?>"><i class="fa fa-question-circle"></i></span></label>
			<div class="col-md-9">
				<textarea class="form-control" name="target_user" rows="5" placeholder="<?= lang('target_user') ?>"><?= (isset($business_plan) ? $business_plan['target_user'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('target_buyer') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_bp_target_buyer') ?>"><i class="fa fa-question-circle"></i></span></label>
			<div class="col-md-9">
				<textarea class="form-control" name="target_buyer" rows="5" placeholder="<?= lang('target_buyer') ?>"><?= (isset($business_plan) ? $business_plan['target_buyer'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('cost_per_unit') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_bp_cost_per_unit') ?>"><i class="fa fa-question-circle"></i></span></label>
			<div class="col-md-9">
				<textarea class="form-control" name="manufacturing_cost" rows="5" placeholder="<?= lang('cost_per_unit') ?>"><?= (isset($business_plan) ? $business_plan['manufacturing_cost'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('competitor') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_bp_competitor') ?>"><i class="fa fa-question-circle"></i></span></label>
			<div class="col-md-9">
				<textarea class="form-control" name="competitor" rows="5" placeholder="<?= lang('competitor') ?>"><?= (isset($business_plan) ? $business_plan['competitor'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('competitive_advantage') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_bp_competitive_advantage') ?>"><i class="fa fa-question-circle"></i></span></label>
			<div class="col-md-9">
				<textarea class="form-control" name="competitive_advantage" rows="5" placeholder="<?= lang('competitive_advantage') ?>"><?= (isset($business_plan) ? $business_plan['competitive_advantage'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('innovation_impact') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_bp_innovation_impact') ?>"><i class="fa fa-question-circle"></i></span></label>
			<div class="col-md-9">
				<textarea class="form-control" name="impact" rows="5" placeholder="<?= lang('innovation_impact') ?>"><?= (isset($business_plan) ? $business_plan['impact'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('business_capital') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_bp_business_capital') ?>"><i class="fa fa-question-circle"></i></span></label>
			<div class="col-md-9">
				<textarea class="form-control" name="business_capital" rows="5" placeholder="<?= lang('business_capital') ?>"><?= (isset($business_plan) ? $business_plan['business_capital'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('source_of_income') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_bp_source_of_income') ?>"><i class="fa fa-question-circle"></i></span></label>
			<div class="col-md-9">
				<textarea class="form-control" name="source_of_income" rows="5" placeholder="<?= lang('source_of_income') ?>"><?= (isset($business_plan) ? $business_plan['source_of_income'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group picture-wrapper">
			<label class="col-md-3 control-label"><?= lang('picture') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_bp_project_pict') ?>"><i class="fa fa-question-circle"></i></span></label>

			<div class="col-md-9 item">
				<?php if(isset($business_plan)){
						foreach ($business_plan_picture as $key => $value) { 
							if(file_exists(realpath(APPPATH . '../'.PATH_TO_INNOVATION_BUSINESS_PLAN_PICTURE_THUMB) . DIRECTORY_SEPARATOR . $value['picture'])) {
								$url = site_url() . 'assets/attachment/business_plan_picture/thumbnail/' . $value['picture'];
							}else{
								$url = site_url() . 'assets/attachment/business_plan_picture/thumbnail/default.jpg';
							} ?>
							<div class="col-md-12">
								<a href="<?= $url ?>" class="fancyboxs">
									<img src="<?= $url ?>" width="300px">
								</a>
								<button type="button" class="btn btn-danger delete-picture" data-id="<?= $value['id'] ?>" data-name="<?= $value['picture']?>"><span class="fa fa-times"></span></button>
							</div>
				<?php 	}
					} ?>

				<div class="col-md-11">
					<input type="file" class="form-control picture-item" name="picture_0">
				</div>
				<div class="col-md-1">
					<button type="button" class="btn btn-grey add-picture"><span class="fa fa-plus"></span></button>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('url') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_bp_url') ?>"><i class="fa fa-question-circle"></i></span></label>
			<div class="col-md-9">
				<div class="input-group">
			    	<div class="input-group-addon">http://</div>
			    	<input type="text" class="form-control" name="url" placeholder="<?= lang('url') ?>" value="<?= (isset($business_plan) ? $business_plan['url'] : "") ?>">
				</div>			
			</div>
		</div>

		<div class="form-group doc-wrapper">
			<label class="col-md-3 control-label"><?= lang('supporting_document') ?></label>

			<div class="col-md-9 item">
				<?php if(isset($business_plan)){
						foreach ($business_plan_doc as $key => $value) { 
							if(file_exists(realpath(APPPATH . '../'.PATH_TO_INNOVATION_BUSINESS_PLAN_DOCUMENT) . DIRECTORY_SEPARATOR . $value['filename'])) {
								$url = site_url() . 'assets/attachment/business_plan_document/' . $value['filename']; ?>
								<div class="col-md-12">
									<a href="<?= $url ?>" target="_blank"><?= lang('view_supporting_document') ?></a>
									<button type="button" class="btn btn-danger delete-doc" data-id="<?= $value['id'] ?>" data-name="<?= $value['filename']?>"><span class="fa fa-times"></span></button>
								</div>
				<?php		} 
					 	}
					} ?>

				<div class="col-md-11">
					<input type="file" class="form-control doc-item" name="doc_0">
				</div>
				<div class="col-md-1">
					<button type="button" class="btn btn-grey add-doc"><span class="fa fa-plus"></span></button>
				</div>
			</div>
		</div>

		<div class="col-md-12 text-center">
			<button type="button" class="btn btn-default btn-back flat" onclick="window.history.back()"><?= ($this->userdata['role_id'] == ROLE_MARA_INNOVATOR ? lang('cancel') : lang('back')) ?></button>
			<input type="submit" name="btn_save" id="btn-save" data-id="btn_save" class="btn btn-success flat" value="<?= lang('save') ?>">
			<?php 
			date_default_timezone_set("Asia/Kuala_Lumpur");
			if(strtotime(date('Y-m-d H:i:s')) >= strtotime('2016-04-07 12:00:00')){
				if(strtotime($round['deadline']) > strtotime(date("Y-m-d H:i:s"))){ ?>
				<input type="submit" name="btn_send_for_approval" data-id="btn_send_for_approval" class="btn btn-success flat btn-send-for-approval" data-id="<?= isset($innovation) ? $innovation['innovation_id'] : 0 ?>" value="<?= lang('send_for_approval') ?>">
			<?php }
			} ?>
		</div>
	</form>
</div>