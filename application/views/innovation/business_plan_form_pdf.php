<style>
table tr td table th{
	background: #888d97;
	color:#FFF;
}

table tr td table.bordered, table tr td table.bordered tr td{
	border:1px solid #ddd !important;
}
</style>

<table border="0" width="100%">
	<tr>
		<td><img src="<?= base_url().ASSETS_IMG."logo_maratex.png" ?>" width="200%"></td>
		<td><img src="<?= base_url().ASSETS_IMG."logo-aim-2016.png" ?>" width="100%"></td>
	</tr>
</table>

<table border="0" width="100%">
	<tr>
		<td colspan="2" style="text-align:center"><h2><?= lang('form_business_plan') ?></h2></td>
	</tr>
	<tr>
		<td width="30%"><?= lang('innovation_product') ?> : </td>
		<td width="70%"><?= $innovation['name_in_melayu'] ?></td>
	</tr>

	<tr>
		<td><?= lang('description') ?> : </td>
		<td><?= $business_plan['description'] ?></td>
	</tr>

	<tr>
		<td><?= lang('the_main_issue_addressed_by_innovation') ?> : </td>
		<td><?= $business_plan['problem'] ?></td>
	</tr>

	<tr>
		<td><?= lang('the_main_issue_addressed_by_innovation') ?> : </td>
		<td><?= $business_plan['problem'] ?></td>
	</tr>

	<tr>
		<td><?= lang('target_user') ?> : </td>
		<td><?= $business_plan['target_user'] ?></td>
	</tr>

	<tr>
		<td><?= lang('target_buyer') ?> : </td>
		<td><?= $business_plan['target_buyer'] ?></td>
	</tr>

	<tr>
		<td><?= lang('cost_per_unit') ?> : </td>
		<td><?= $business_plan['manufacturing_cost'] ?></td>
	</tr>

	<tr>
		<td><?= lang('competitor') ?> : </td>
		<td><?= $business_plan['competitor'] ?></td>
	</tr>

	<tr>
		<td><?= lang('competitive_advantage') ?> : </td>
		<td><?= $business_plan['competitive_advantage'] ?></td>
	</tr>

	<tr>
		<td><?= lang('innovation_impact') ?> : </td>
		<td><?= $business_plan['impact'] ?></td>
	</tr>

	<tr>
		<td><?= lang('business_capital') ?> : </td>
		<td><?= $business_plan['business_capital'] ?></td>
	</tr>

	<tr>
		<td><?= lang('source_of_income') ?> : </td>
		<td><?= $business_plan['source_of_income'] ?></td>
	</tr>

	<tr>
		<td><?= lang('picture') ?> : </td>
		<td>
			<table border="0" width="100%">
			<?php foreach ($business_plan_picture as $key => $value) { 
					if(file_exists(realpath(APPPATH . '../'.PATH_TO_INNOVATION_BUSINESS_PLAN_PICTURE_THUMB) . DIRECTORY_SEPARATOR . $value['picture'])) {
						$url = site_url() . 'assets/attachment/business_plan_picture/thumbnail/' . $value['picture'];
					}else{
						$url = site_url() . 'assets/attachment/business_plan_picture/thumbnail/default.png';
					} ?>
					<tr>
						<td><img src="<?= $url ?>" width="50%"></td>
					</tr>
			<?php } ?>
			</table>
		</td>
	</tr>

	<tr>
		<td><?= lang('url') ?> : </td>
		<td><?= "http://".$business_plan['url'] ?></td>
	</tr>
</table>