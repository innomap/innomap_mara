<script type="text/javascript">
	var alert = "<?= $alert ?>",
		alert_dialog = "",
		days_left = "<?= (isset($days_left) ? $days_left : 0) ?>";
		if(days_left != 0){
			var lang_send_for_approval_confirm_message = "<?= str_replace('{days_left}','"+days_left+"',lang('send_for_approval_confirm_message_reminder')) ?>";
		}else{
			var lang_send_for_approval_confirm_message = "<?= lang('send_for_approval_confirm_message') ?>";
		}
	var	lang_send_innovation_for_approval = "<?= lang('send_innovation_for_approval') ?>",
		lang_cancel = "<?= lang('cancel') ?>",
		lang_send_for_approval = "<?= lang('send_for_approval') ?>",
		lang_yes = "<?= lang('yes') ?>",
		lang_application_submitted_msg = "<?= lang('application_submitted_msg') ?>";

	var view_mode = "<?= ($view_mode == 1 ? true : false) ?>";
</script>
<div class="col-md-8 col-md-offset-2">
	<?php $this->load->view('partial/logo_after_login',array('title' => lang('innovation_form'))); ?>

	<div class="alert alert-info alert-dismissable hide">
	    <i class="fa fa-info"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <?= $alert ?>
	</div>

	<form id="form-innovation" class="form-horizontal" action="<?= base_url().$action_url ?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="id" value="<?= (isset($innovation) ? $innovation['innovation_id'] : 0) ?>">

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('mara_center') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" value="<?= $innovator['mara_center'] ?>" readonly>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('mara_address') ?></label>
			<div class="col-md-9">
				<textarea class="form-control" disabled><?= $innovator['mara_address'] ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('mara_telp_no') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" value="<?= $innovator['mara_telp_no'] ?>" readonly>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('mara_fax_no') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" value="<?= $innovator['mara_fax_no'] ?>" readonly>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('application_category') ?></label>
			<div class="col-md-9">
				<select name="category_produk_level" class="form-control category-produk-level-opt">
				<?php foreach ($categories_produk_level as $key => $value) { ?>
					<option value="<?= $key ?>" <?= isset($innovation) ? ($i_categories['level'] == $key ? "selected" : "") : "" ?>><?= $value ?></option>
				<?php } ?>
				</select>
			</div>
		</div>

		<div class="form-group category-wrapper">
			<label class="col-md-3 control-label"><?= lang('category') ?></label>

			<div class="col-md-9">
				<select class="form-control category-opt">
				<?php foreach ($categories as $key => $value) { ?>
					<option value="<?= $key ?>" <?= isset($innovation) ? ($i_categories['category'] == $key ? "selected" : "") : "" ?>><?= $value ?></option>
				<?php } ?>
				</select>
				<input type="hidden" name="category" value="<?= isset($innovation) ? $i_categories['category'] : '' ?>">

				<select name="category_service_area" class="form-control category-service-area-opt">
				<?php foreach ($categories_services_area as $key => $value) { ?>
					<option value="<?= $key ?>" <?= isset($innovation) ? ($i_categories['area'] == $key ? "selected" : "") : "" ?>><?= $value ?></option>
				<?php } ?>
				</select>

				<select name="category_produk_area" class="form-control category-produk-area-opt">
				<?php foreach ($categories_produk_area as $key => $value) { ?>
					<option value="<?= $key ?>" <?= isset($innovation) ? ($i_categories['area'] == $key ? "selected" : "") : "" ?>><?= $value ?></option>
				<?php } ?>
				</select>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('innovator_photo') ?></label>
			<div class="col-md-9">
				<?php if(file_exists(realpath(APPPATH . '../'.PATH_TO_INNOVATOR_PHOTO) . DIRECTORY_SEPARATOR . $innovator['photo']) && $innovator['photo'] != "") { ?>
						<a href="<?= base_url().PATH_TO_INNOVATOR_PHOTO.$innovator['photo'] ?>" class="fancyboxs"><img src="<?= base_url().PATH_TO_INNOVATOR_PHOTO_THUMB.$innovator['photo'] ?>" width="30%"/></a>
				<?php } ?>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('innovator') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" placeholder="<?= lang('innovator') ?>" value="<?= $innovator['name'] ?>" readonly>
				<input type="hidden" name="innovator_id" value="<?= $innovator['id'] ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('no_kp') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" value="<?= $innovator['kp_no'] ?>" readonly>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('email') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" value="<?= $innovator['email'] ?>" readonly>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('telp_no') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" value="<?= $innovator['telp_no'] ?>" readonly>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('fax_no') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" value="<?= $innovator['fax_no'] ?>" readonly>
			</div>
		</div>

		<div class="form-group field-instructor">
			<label class="col-md-3 control-label"><?= lang('instructor_name') ?></label>
			<div class="col-md-9">
				<input type="text" name="instructor_name" class="form-control" placeholder="<?= lang('instructor_name') ?>" value="<?= isset($innovation) ? $i_categories['instructor_name'] : '' ?>">
			</div>
		</div>

		<div class="form-group field-instructor">
			<label class="col-md-3 control-label"><?= lang('instructor_staff_id') ?></label>
			<div class="col-md-9">
				<input type="text" name="instructor_staff_id" class="form-control" placeholder="<?= lang('instructor_staff_id') ?>" value="<?= isset($innovation) ? $i_categories['instructor_staff_id'] : '' ?>">
			</div>
		</div>

		<div class="form-group field-instructor">
			<label class="col-md-3 control-label"><?= lang('instructor_kp_no') ?></label>
			<div class="col-md-9">
				<input type="text" name="instructor_kp_no" class="form-control" placeholder="<?= lang('instructor_kp_no') ?>" value="<?= isset($innovation) ? $i_categories['instructor_kp_no'] : '' ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('innovation_product') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" name="innovation_name" placeholder="<?= lang('innovation_product') ?>" value="<?= (isset($innovation) ? $innovation['name_in_melayu'] : "") ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('inspiration') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_idea') ?>"><i class="fa fa-question-circle"></i></span></label>
			<div class="col-md-9">
				<textarea class="form-control" name="inspiration" rows="5" placeholder="<?= lang('inspiration') ?>"><?= (isset($innovation) ? $innovation['inspiration'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('description') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_description') ?>"><i class="fa fa-question-circle"></i></span></label>
			<div class="col-md-9">
				<textarea class="form-control" name="description" rows="5" placeholder="<?= lang('description_placeholder') ?>"><?= (isset($innovation) ? $innovation['description'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('material') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_material') ?>"><i class="fa fa-question-circle"></i></span></label>
			<div class="col-md-9">
				<textarea class="form-control" name="materials" rows="5" placeholder="<?= lang('material') ?>"><?= (isset($innovation) ? $innovation['materials'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('how_to_use') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_how_to_use') ?>"><i class="fa fa-question-circle"></i></span></label>
			<div class="col-md-9">
				<textarea class="form-control" name="how_to_use" rows="5" placeholder="<?= lang('how_to_use') ?>"><?= (isset($innovation) ? $innovation['how_to_use'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('special_achievement') ?></label>
			<div class="col-md-9">
				<textarea class="form-control" name="special_achievement" rows="5" placeholder="<?= lang('special_achievement') ?>"><?= (isset($innovation) ? $innovation['special_achievement'] : "") ?></textarea>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('created_date') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control datepicker" name="created_date" value="<?= (isset($innovation) ? $innovation['created_date'] : "") ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('discovered_date') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control datepicker" name="discovered_date" value="<?= (isset($innovation) ? $innovation['discovered_date'] : "") ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('manufacturing_cost') ?></label>
			<div class="col-md-9">
				<div class="input-group">
			    	<div class="input-group-addon">RM</div>
			    	<input type="text" class="form-control" name="manufacturing_costs" value="<?= (isset($innovation) ? $innovation['manufacturing_costs'] : "") ?>">
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('selling_price') ?></label>
			<div class="col-md-9">
				<div class="input-group">
			    	<div class="input-group-addon">RM</div>
			    	<input type="text" class="form-control" name="selling_price" value="<?= (isset($innovation) ? $innovation['selling_price'] : "") ?>">
				</div>
			</div>
		</div>		

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('target') ?></label>
			<div class="col-md-9">
				<?php foreach ($targets as $key => $value) { ?>
					<div class="checkbox"> 
						<label> <input type="checkbox" name="target[]" value="<?= $key ?>" 
							<?php if(isset($innovation)){ 
									foreach ($i_target as $val) {
										echo ($val == $key ? 'checked' : '');
									}
								}?>> <?= $value ?> </label> 
					</div>
				<?php } ?>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('myipo_protection') ?></label>
			<div class="col-md-9">
				<div class="radio"> 
					<label> <input type="radio" name="myipo_protection" value="1" <?= (isset($innovation) ? ($innovation['myipo_protection'] == 1 ? "checked" : "") : "checked") ?>> <?= lang('yes') ?> </label> 
				</div>
				<div class="radio"> 
					<label> <input type="radio" name="myipo_protection" value="2" <?= (isset($innovation) ? ($innovation['myipo_protection'] == 2 ? "checked" : "") : "") ?>> <?= lang('no') ?> </label> 
				</div>
			</div>
		</div>

		<div class="form-group picture-wrapper">
			<label class="col-md-3 control-label"><?= lang('picture') ?>&nbsp;<span href="#" class="popover-item" data-toggle="popover" data-content="<?= lang('tips_images') ?>"><i class="fa fa-question-circle"></i></span></label>

			<div class="col-md-9 item">
				<?php if(isset($innovation)){
						foreach ($i_picture as $key => $value) { 
							if(file_exists(realpath(APPPATH . '../'.PATH_TO_INNOVATION_PICTURE_THUMB) . DIRECTORY_SEPARATOR . $value['picture'])) {
								$url = site_url() . 'assets/attachment/innovation_picture/thumbnail/' . $value['picture'];
							}else{
								$url = site_url() . 'assets/attachment/innovation_picture/thumbnail/default.jpg';
							} ?>
							<div class="col-md-12">
								<a href="<?= $url ?>" class="fancyboxs">
									<img src="<?= $url ?>" width="300px">
								</a>
								<button type="button" class="btn btn-danger delete-picture" data-id="<?= $value['innovation_picture_id'] ?>" data-name="<?= $value['picture']?>"><span class="fa fa-times"></span></button>
							</div>
				<?php 	}
					} ?>

				<div class="col-md-11">
					<input type="file" class="form-control picture-item" name="picture_0">
				</div>
				<div class="col-md-1">
					<button type="button" class="btn btn-grey add-picture"><span class="fa fa-plus"></span></button>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('keyword') ?></label>
			<div class="col-md-9">
				<input type="text" class="form-control" name="keyword" placeholder="<?= lang('keyword_separated_by_comma') ?>" value="<?= (isset($innovation) ? $i_keyword : "") ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('team_member') ?></label>
			<div class="col-md-9 team-member-wrap">
				<?php if(isset($innovation)){
						foreach ($i_team as $key => $value) {
							if($value['photo']){
								if(file_exists(realpath(APPPATH . '../'.PATH_TO_TEAM_MEMBER_PHOTO_THUMB) . DIRECTORY_SEPARATOR . $value['photo'])) {
									$url = site_url() . PATH_TO_TEAM_MEMBER_PHOTO_THUMB . $value['photo'];
								}else{
									$url = "";
								}
							}else{ $url = ""; } ?> 
							<input type="hidden" class="team-member-id" name="team_member_id[]" value="<?= $value['id'] ?>">
							<div class="col-md-11 team-member-item">
								<input type="hidden" name="h_team_member_pic_<?= $key ?>" value="<?= $value['photo'] ?>">
								<input type="hidden" name="i_deleted_team_member_<?= $key ?>">
								<div class="col-md-6">
									<input type="text"class="form-control" name="team_member_<?= $key ?>" placeholder="<?= lang('member_name') ?>" value="<?= $value['name'] ?>">
								</div>
								<div class="col-md-3">
									<input type="text"class="form-control" name="team_member_kp_<?= $key ?>" placeholder="<?= lang('no_kp') ?>" value="<?= $value['kp_no'] ?>">
								</div>
								<div class="col-md-3">
									<input type="text"class="form-control" name="team_member_telp_<?= $key ?>" placeholder="<?= lang('telp_no') ?>" value="<?= $value['telp_no'] ?>">
								</div>
								<div class="col-md-12 margin-tb-10">
									<div class="col-md-1">
										<label><?= lang('photo') ?></label>
									</div>
									<?php if($url != "" ){ ?>
										<div class="col-md-4">
											<a href="<?= $url ?>" class="fancyboxs">
												<img src="<?= $url ?>" width="100%">
											</a>
										</div>
									<?php } ?>
									<div class="col-md-<?= $url != "" ? '7' : '11' ?> no-padding">
										<input type="file" class="form-control" name="team_member_pic_<?= $key ?>">
									</div>
								</div>
							</div>
							<?php if($key == 0){ ?>
								<div class="col-md-1">
									<button type="button" class="btn btn-grey add-team-member"><span class="fa fa-plus"></span></button>
								</div>
				<?php 		}else{ ?>
								<div class="col-md-1">
									<button type="button" class="btn btn-danger delete-team-member"><span class="fa fa-times"></span></button>
								</div>
				<?php		}
						}
					  }else{ ?>
					  	<input type="hidden" class="team-member-id" name="team_member_id[]" value="0">
						<div class="col-md-11 team-member-item">	
							<input type="hidden" name="h_team_member_pic_0" value="">
							<input type="hidden" name="i_deleted_team_member_0">
							<div class="col-md-6">
								<input type="text"class="form-control" name="team_member_0" placeholder="<?= lang('member_name') ?>">
							</div>
							<div class="col-md-3">
								<input type="text"class="form-control" name="team_member_kp_0" placeholder="<?= lang('no_kp') ?>">
							</div>
							<div class="col-md-3">
								<input type="text"class="form-control" name="team_member_telp_0" placeholder="<?= lang('telp_no') ?>">
							</div>
							<div class="col-md-12 margin-tb-10">
								<div class="col-md-1">
									<label><?= lang('photo') ?></label>
								</div>
								<div class="col-md-11 no-padding">
									<input type="file" class="form-control" name="team_member_pic_0">
								</div>
							</div>
						</div>
						<div class="col-md-1">
							<button type="button" class="btn btn-grey add-team-member"><span class="fa fa-plus"></span></button>
						</div>
				<?php } ?>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('heir') ?></label>
			<div class="col-md-9">
				<div class="col-md-11 no-padding-left">
					<div class="col-md-6 no-padding-left">
						<input type="text"class="form-control" name="heir_name" placeholder="<?= lang('name') ?>" value="<?= isset($innovation) ? $i_info['heir_name'] : '' ?>">
					</div>
					<div class="col-md-3 no-padding-left">
						<input type="text"class="form-control" name="heir_kp" placeholder="<?= lang('no_kp') ?>" value="<?= isset($innovation) ? $i_info['heir_kp_no'] : '' ?>">
					</div>
					<div class="col-md-3 no-padding-left">
						<input type="text"class="form-control" name="heir_telp" placeholder="<?= lang('telp_no') ?>" value="<?= isset($innovation) ? $i_info['heir_telp_no'] : '' ?>">
					</div>
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('validation_status') ?></label>
			<div class="col-md-9">
				<a href="<?= base_url().'innovations/generate_approval_form_pdf/'.$this->userdata['id'].'/'.(isset($innovation) ? $innovation['innovation_id'] : 0) ?>" class="btn-download-wrap" target="_blank">
					<?= lang('download_file_validation') ?>
				</a>
				<div class="checkbox"> 
					<label> <input type="checkbox" name="has_validation" class="has-validation" value="1" <?= isset($innovation) ? ($i_info['validation_attachment'] != "" ? 'checked' : '') : '' ?>> <?= lang('has_validation_msg') ?> </label> 
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-3"></div>
			<div class="col-md-9">
				<?php if(isset($innovation)){
							if(file_exists(realpath(APPPATH . '../'.PATH_TO_INNOVATION_ATTACHMENT) . DIRECTORY_SEPARATOR . $i_info['validation_attachment']) && $i_info['validation_attachment'] != "") { ?>
							<a href="<?= base_url().PATH_TO_INNOVATION_ATTACHMENT.$i_info['validation_attachment'] ?>" target="_blank"><?= lang('view_attachment') ?></a>
				<?php 		} ?>
						<input type="hidden" name="h_validation_attachment" value="<?= isset($innovation) ? $i_info['validation_attachment'] : '' ?>">
				<?php	} ?>
				<input type="file" name="validation_attachment" class="form-control validation-attachment">
			</div>
		</div>

		<div class="col-md-12 text-center">
			<button type="button" class="btn btn-default btn-back flat" onclick="window.history.back()"><?= ($this->userdata['role_id'] == ROLE_MARA_INNOVATOR ? lang('cancel') : lang('back')) ?></button>
			<input type="submit" name="btn_save" id="btn-save" data-id="btn_save" class="btn btn-success flat" value="<?= lang('save') ?>">
			<?php 
			date_default_timezone_set("Asia/Kuala_Lumpur");
			if($this->userdata['id'] != 1691){
				if(strtotime(date('Y-m-d')) > strtotime('2016-02-18')){
					if(isset($innovation)){
						if($innovation['status'] == INNO_STATUS_DRAFT && (strtotime($i_status['deadline']) > strtotime(date("Y-m-d H:i:s")))){ ?>
							<input type="submit" name="btn_send_for_approval" data-id="btn_send_for_approval" class="btn btn-success flat btn-send-for-approval" data-id="<?= isset($innovation) ? $innovation['innovation_id'] : 0 ?>" value="<?= lang('send_for_approval') ?>">
			<?php 		}
					}
				 } }else{ ?>
				 <input type="submit" name="btn_send_for_approval" data-id="btn_send_for_approval" class="btn btn-success flat btn-send-for-approval" data-id="<?= isset($innovation) ? $innovation['innovation_id'] : 0 ?>" value="<?= lang('send_for_approval') ?>">
			<?php } ?>
		</div>
	</form>
</div>