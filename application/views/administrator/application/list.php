<script type="text/javascript">
	var alert = "<?=$alert?>";
</script>
<div class="col-md-12">
    <div class="col-md-12 table-responsive">
		<div class="alert alert-info alert-dismissable hide">
		    <i class="fa fa-info"></i>
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		    <?= $alert ?>
		</div>

		<div class="row">
			<div class="form-group col-md-6">
				<label class="col-md-3"><?= lang('filter_by_status') ?></label>
				<div class="col-md-3">
					<select id="dt-filter-status" class="form-control">
						<option>All</option>
						<?php foreach ($status as $key => $value) { ?>
								<option><?= $value['name'] ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
		</div>
		
		<form id="form-application" action="<?= base_url().PATH_TO_ADMIN.'applications/list_submit_handler' ?>" method="post">
			<table id="table-application" class="table table-bordered table-striped table-dark">
			    <thead>
				    <tr>
				    	<th></th>
						<th>No</th>
						<th><?= lang('innovator') ?></th>
						<th><?= lang('innovation_product') ?></th>
						<th><?= lang('description') ?></th>
						<th><?= lang('status') ?></th>
						<th><?= lang('average_score') ?></th>
						<th><?= lang('application_category') ?></th>
						<th><?= lang('innovation_category') ?></th>
						<th><?= lang('subcategory') ?></th>
						<th><?= lang('action') ?></th>
				    </tr>
			    </thead>
			    <tbody>
			    <?php $no = 1; foreach ($innovations as $innovation) { ?>
				<tr>
					<td><input type="checkbox" name="ids[]" data-is-evaluated="<?= ($innovation['is_evaluation_done'] ? 1 : 0) ?>" data-has-evaluation="<?= $innovation['has_evaluation'] ?>" value="<?= $innovation['innovation_id'] ?>" <?= ($innovation['status'] == INNO_STATUS_APPROVED ? 'disabled' : '') ?>></td>
				    <td><?= $no ?></td>
				    <td><?= $innovation['innovator'] ?></td>
				    <td><?= $innovation['name_in_melayu'] ?></td>
				    <td><?= ellipsis($innovation['description_in_melayu'], 100) ?></td>
				    <td class="<?= $status[$innovation['status']]['class'] ?>"><?= $status[$innovation['status']]['name'] ?></td>
				    <td><?= $innovation['score']; ?></td>
				    <td><?= $innovation['application_category']; ?></td>
				    <td><?= $innovation['category']; ?></td>
				    <td><?= $innovation['subcategory']; ?></td>
				    <td>
				    	<input type="hidden" name="action">
						<button type="button" class="btn btn-default btn-action btn-view-innovation" title="<?= lang('view_innovation_detail') ?>" data-id="<?= $innovation['innovation_id'] ?>">
						    <span class="fa fa-search fa-lg"></span>
						</button>

						<?php if($innovation['status'] > INNO_STATUS_SENT_APPROVAL){ ?>
							<button type="button" class="btn btn-default btn-action btn-view-evaluation-list" title="<?= lang('view_evaluation_result') ?>" data-id="<?= $innovation['innovation_id'] ?>">
							    <span class="fa fa-files-o fa-lg"></span>
							</button>
						<?php } ?>

						<?php if($innovation['status'] <= INNO_STATUS_SENT_APPROVAL){ ?>
							<button type="button" class="btn btn-default btn-action btn-assign-innovation" title="<?= lang('assign_to_evaluator') ?>" data-id="<?= $innovation['innovation_id'] ?>">
								    <span class="fa fa-user fa-lg"></span>
							</button>
						<?php } ?>
						<button type="button" class="btn btn-default btn-action btn-download-pdf" title="<?= lang('download_pdf') ?>" data-id="<?= $innovation['innovation_id'] ?>">
						    <span class="fa fa-download fa-lg"></span> 
						</button>
						<?php if(isset($innovation['business_plan']) && $innovation['last_status'] > INNO_STATUS_DRAFT){ ?>
						<button type="button" class="btn btn-default btn-action btn-business-plan-eval" title="<?= lang('business_plan_evaluation') ?>" data-id="<?= $innovation['innovation_id'] ?>" data-title="<?= $innovation['name_in_melayu'] ?>">
						    <span class="fa fa-edit fa-lg"></span> 
						</button>
						<?php }else if(!isset($innovation['business_plan'])){ ?>
						<button type="button" class="btn btn-default btn-action btn-business-plan-eval" title="<?= lang('business_plan_evaluation') ?>" data-id="<?= $innovation['innovation_id'] ?>" data-title="<?= $innovation['name_in_melayu'] ?>">
						    <span class="fa fa-plus-square fa-lg"></span> 
						</button>
						<?php } ?>
				    </td>
				</tr>
			    <?php $no++;} ?>
			    </tbody>
			</table>

			<?php if(count($innovations) > 0){ ?>
				<div class="form-group">
					<label><?= lang('with_selected')." : " ?></label>
					<input type="submit" value="<?= lang('proceed_to_next_round') ?>" data-id="btn-proceed" class="btn btn-success">
				</div>
			<?php } ?>
		</form>
    </div>
</div>
<?= $form_assign ?>
<?= $form_bp_eval ?>