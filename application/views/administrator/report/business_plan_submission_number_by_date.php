<div class="col-md-12 report_type" data-content="bp-submission-num-by-date">
	<div class="row">
		<form id="form-bp-submission-num" action="<?= base_url().PATH_TO_ADMIN.'reports/export_business_plan_submission_number_by_date' ?>" method="post">
			<div class="form-group col-md-12">
				<div class="col-md-1">
					<label><?= lang('filter') ?>:</label>
				</div>
				<div class="col-md-2">
					<label><?= lang('from') ?></label>
					<input type="text" class="form-control from-datepicker" name="date_from" value="<?= $from_date ?>">
				</div>
				<div class="col-md-2">
					<label><?= lang('to') ?></label>
					<input type="text" class="form-control to-datepicker" name="date_to" value="<?= $to_date ?>">
				</div>
				<div class="col-md-3 report-filter-act">
					<button type="button" class="btn btn-primary flat" id="submit-bp-submission-num">Submit</button>
					<button type="submit" class="btn btn-success flat" id="export-bp-submission-num">Export to Excel</button>
				</div>
			</div>
		</form>
	</div>

	<div class="col-md-12" id="container-bp-submission-num-by-date">
	</div>
</div>

<?= $modal_detail; ?>