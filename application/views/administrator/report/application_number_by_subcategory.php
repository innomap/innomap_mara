<div class="col-md-12 report_type" data-content="application-num-by-sub-category">
	<div class="col-md-12">
		<form id="form-application-num-by-subcat" action="<?= base_url().PATH_TO_ADMIN.'reports/export_application_number_by_subcategory' ?>" method="post">
			<div class="form-group col-md-12">
				<div class="col-md-1">
					<label><?= lang('filter') ?>:</label>
				</div>
				<div class="col-md-12">
					<?php foreach ($categories as $key => $category) {
			            if($key == 0){
			                $subcategories = $service_subcategories;
			            }else{
			                $subcategories = $product_subcategories;
			            }

			            foreach ($subcategories as $i => $subcategory) { ?>
			                <div class="checkbox"> 
								<label> <input type="checkbox" class="subcategory" name="subcategories[]" value="<?= $key.",".$i ?>" data-id="<?= $i ?>" data-category="<?= $key ?>" checked/> <?= $subcategory ?></label>
							</div>
			            <?php }
			        } ?>
					
				</div>
				<div class="col-md-3">
					<button type="button" class="btn btn-primary flat" id="submit-application-num-by-sub-cat">Submit</button>
					<button type="submit" class="btn btn-success flat" id="export-application-num-by-sub-cat">Export to Excel</button>
				</div>
			</div>
		</form>
	</div>

	<div class="col-md-12" id="container-application-num-by-sub-cat">
	</div>
</div>

<?= $modal_detail; ?>