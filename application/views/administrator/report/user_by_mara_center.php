<div class="col-md-12 report_type" data-content="user-by-mara-center">
	<div class="col-md-12 text-center margin-btm-50">
		<h2><?= lang('report_user_by_mara_center') ?></h2>
	</div>

	<div class="row">
		<form id="form-submission-num" action="<?= base_url().PATH_TO_ADMIN.'reports/submission_by_mara_center' ?>" method="post">
			<div class="form-group col-md-12">
				<div class="col-md-1 no-padding-right">
					<label><?= lang('mara_center') ?>:</label>
				</div>
				<div class="col-md-4">
					<select class="form-control" name="mara_center_id">
							<option value="0"><?= "-- ".lang('all_mara_center')." --" ?></option>
						<?php foreach ($mara_center as $key => $value) { ?>
							<option value="<?= $value['id'] ?>" <?= $selected_mara_center == $value['id'] ? 'selected' : '' ?>><?= $value['name'] ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-3">
					<button type="submit" name="btn_report" class="btn btn-primary flat">Submit</button>
					<button type="submit" name="btn_export" class="btn btn-success flat" <?= count($users) <= 0 ? 'disabled' : '' ?>>Export to Excel</button>
				</div>
			</div>
		</form>
	</div>

	<div class="col-md-12">
		<table id="table-list" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>No</th>
					<th><?= lang('mara_center') ?></th>
					<th><?= lang('submission_number') ?></th>
				</tr>
			</thead>
			<tbody>
				<?php $no=1; foreach ($users as $key => $value) { ?>
					<tr>
						<td><?= $no; ?></td>
						<td><?= $value['name'] ?></td>
						<td><?= count($value['submissions']) ?></td>
					</tr>
				<?php $no++;} ?>
			</tbody>
		</table>
	</div>
</div>