<div class="col-md-12 report_type" data-content="user-by-mara-center">
	<div class="col-md-12 text-center margin-btm-50">
		<h2><?= lang('report_top_60_scores_by_each_award_category') ?></h2>
	</div>

	<div class="row">
		<form id="form-submission-num" action="<?= base_url().PATH_TO_ADMIN.'reports/top_score_by_award_category' ?>" method="post">
			<div class="form-group col-md-12">
				<div class="col-md-2 no-padding-right">
					<label><?= lang('award_category') ?>:</label>
				</div>
				<div class="col-md-4">
					<select class="form-control" name="award_category">
						<?php foreach ($award_categories as $key => $value) { ?>
							<option value="<?= $value['id'] ?>" <?= $selected_award_category == $value['id'] ? 'selected' : '' ?>><?= $value['category'] ?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-3">
					<button type="submit" name="btn_report" class="btn btn-primary flat">Submit</button>
					<button type="submit" name="btn_export" class="btn btn-success flat" <?= count($submissions) <= 0 ? 'disabled' : '' ?>>Export to Excel</button>
				</div>
			</div>
		</form>
	</div>

	<div class="col-md-12">
		<table id="table-list" class="table table-bordered table-striped">
			<thead>
				<tr>
					<th><?= lang('ranking') ?></th>
					<th><?= lang('project_name') ?></th>
					<th><?= lang('team_leader') ?></th>
					<th><?= lang('mara_center') ?></th>
					<th><?= lang('average_score') ?></th>
					<th><?= lang('group_name') ?></th>
				</tr>
			</thead>
			<tbody>
				<?php $no=1; foreach ($submissions as $key => $value) { ?>
					<tr>
						<td><?= $no ?></td>
						<td><?= $value['innovation_name'] ?></td>
						<td><?= $value['innovator_name'] ?></td>
						<td><?= $value['mara_center_name'] ?></td>
						<td><?= $value['score'] ?></td>
						<td><?= $value['group_name'] ?></td>
					</tr>
				<?php $no++;} ?>
			</tbody>
		</table>
	</div>
</div>