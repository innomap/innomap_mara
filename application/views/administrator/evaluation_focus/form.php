<div class="modal fade" id="modal-evaluation-focus" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= lang('cancel') ?></span></button>
				<h4 class="modal-title"><?= lang('evaluation_focus') ?></h4>
			</div>
			<form role="form" id="form-evaluation-focus" class="form-horizontal" action="<?= site_url(PATH_TO_ADMIN.'evaluation_focus/store/') ?>" method="POST">
			<input type="hidden" name="id" />
			<input type="hidden" name="form_id" value="<?= $form_id ?>" />
				<div class="modal-body">
					<!-- text input -->
					<div class="form-group">
						<div class="col-md-12">
							<label><?= lang('evaluation_focus') ?></label>
						</div>
						<div class="col-md-12">
							<input type="text" name="label" class="form-control" placeholder="<?= lang('evaluation_focus') ?>" />
						</div>
					</div>
					<!-- text input -->
					<div class="form-group">
						<div class="col-md-12">
							<label><?= lang('percentage') ?></label>
						</div>
						<div class="col-md-12">
							<input type="text" name="percentage" class="form-control" placeholder="<?= lang('percentage') ?>" />
						</div>
					</div>
					<!-- text input -->
					<div class="form-group">
						<div class="col-md-12">
							<label><?= lang('criteria') ?></label>
						</div>
						<div class="col-md-12 criteria-wrap">
							<div class="col-md-12 no-padding">
								<div class="col-md-3 no-padding-left q-item">
									<input type="text" name="question[]" class="form-control" placeholder="<?= lang('criteria') ?>" />
								</div>
								<div class="col-md-3 no-padding-left desc-item">
									<textarea name="description[]" class="form-control" rows="3" placeholder="<?= lang('question') ?>" ></textarea>
								</div>
								<div class="col-md-3 no-padding-left desc-item">
									<textarea name="guideline[]" class="form-control" rows="3" placeholder="<?= lang('guidelines') ?>" ></textarea>
								</div>
								<div class="col-md-2 no-padding-left point-item">
									<input type="text" name="max_point[]" class="form-control no-padding-right" placeholder="<?= lang('max_point') ?>" />
								</div>
								<div class="col-md-1 no-padding-left">
									<button type="button" class="btn btn-grey add-criteria"><span class="fa fa-plus"></span></button>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default flat" data-dismiss="modal"><?= lang('cancel') ?></button>
					<button type="submit" class="btn btn-success flat"><?= lang('save') ?></button>
				</div>
			</form>
		</div>
	</div>
</div>