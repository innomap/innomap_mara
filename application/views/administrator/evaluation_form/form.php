<div class="modal fade" id="modal-evaluation-form" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= lang('cancel') ?></span></button>
				<h4 class="modal-title"><?= lang('evaluation_form') ?></h4>
			</div>
			<form role="form" id="form-evaluation-form" class="form-horizontal" action="<?= site_url(PATH_TO_ADMIN.'evaluation_form/store/') ?>" method="POST">
			<input type="hidden" name="id" />
				<div class="modal-body">
					<!-- text input -->
					<div class="form-group">
						<div class="col-md-12">
							<label><?= lang('category') ?></label>
						</div>
						<div class="col-md-12">
							<select id="category" name="category" class="form-control">
								<?php foreach ($categories as $key => $value) { ?>
									<option value="<?= $key ?>"><?= $value ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<!-- text input -->
					<div class="form-group">
						<div class="col-md-12">
							<label><?= lang('subcategory') ?></label>
						</div>
						<div class="col-md-12">
							<select id="subcategory-service" name="subcategory_service" class="form-control">
								<?php foreach ($categories_services_area as $key => $value) { ?>
									<option value="<?= $key ?>"><?= $value ?></option>
								<?php } ?>
							</select>

							<select id="subcategory-produk" name="subcategory_product" class="form-control" style="display:none">
								<?php foreach ($categories_produk_level as $key => $value) { ?>
									<option value="<?= $key ?>"><?= $value ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default flat" data-dismiss="modal"><?= lang('cancel') ?></button>
					<button type="submit" class="btn btn-success flat"><?= lang('save') ?></button>
				</div>
			</form>
		</div>
	</div>
</div>