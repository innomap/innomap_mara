<div class="modal fade" id="modal-bp-evaluation" data-backdrop="static" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"><?= lang('cancel') ?></span></button>
				<h4 class="modal-title"><?= lang('business_plan_evaluation') ?></h4>
			</div>
			<form role="form" id="form-bp-evaluation" action="<?= site_url(PATH_TO_ADMIN.'business_plans/evaluation_handler/') ?>" method="POST" enctype="multipart/form-data">
			<input type="hidden" name="id" />
			<input type="hidden" name="innovation_id" />
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<!-- text input -->
							<div class="form-group">
								<label><?= lang('innovation_product') ?></label>
								<input type="text" name="innovation_product" class="form-control" placeholder="<?= lang('innovation_product') ?>" readonly/>
							</div>
							<div class="form-group">
								<label>Markah 1</label>
							</div>
							<div class="form-group">
								<div class="col-md-4 no-padding">
									<label><?= lang('average_score') ?></label>
								</div>
								<div class="col-md-8">
									<input type="text" name="first_evaluation_score" class="form-control" placeholder="<?= lang('first_eval_score') ?>" readonly/>
								</div>
							</div>
							<div class="eval-detail-wrap">
								
							</div>
							<div class="form-group">
								<label>Markah 2</label>
							</div>
							<!-- text input -->
							<div class="col-md-12 no-padding score-wrap">
								<input type="hidden" class="score-id" name="score_id[]" value="0">
								<div class="form-group col-md-12 no-padding">
									<div class="col-md-4 no-padding">
										<input type="text" name="score_0" class="form-control" placeholder="Markah" />
									</div>
									<div class="col-md-4">
										<input type="file" name="score_attachment_0" class="form-control"/>
									</div>
									<div class="col-md-1">
										<button type="button" class="btn btn-grey add-score"><span class="fa fa-plus"></span></button>
									</div>
									<div class="col-md-3">
									</div>
								</div>
							</div>

							<div class="form-group">
								<label>Markah 3</label>
							</div>
							<!-- text input -->
							<div class="col-md-12 no-padding score1-wrap">
								<input type="hidden" class="score1-id" name="score1_id[]" value="0">
								<div class="form-group col-md-12 no-padding">
									<div class="col-md-4 no-padding">
										<input type="text" name="score1_0" class="form-control" placeholder="Markah" />
									</div>
									<div class="col-md-4">
										<input type="file" name="score1_attachment_0" class="form-control"/>
									</div>
									<div class="col-md-1">
										<button type="button" class="btn btn-grey add-score1"><span class="fa fa-plus"></span></button>
									</div>
									<div class="col-md-3">
									</div>
								</div>
							</div>

							<div class="form-group">
								<label>Markah 4</label>
							</div>
							<!-- text input -->
							<div class="col-md-12 no-padding score2-wrap">
								<input type="hidden" class="score2-id" name="score2_id[]" value="0">
								<div class="form-group col-md-12 no-padding">
									<div class="col-md-4 no-padding">
										<input type="text" name="score2_0" class="form-control" placeholder="Markah" />
									</div>
									<div class="col-md-4">
										<input type="file" name="score2_attachment_0" class="form-control"/>
									</div>
									<div class="col-md-1">
										<button type="button" class="btn btn-grey add-score2"><span class="fa fa-plus"></span></button>
									</div>
									<div class="col-md-3">
									</div>
								</div>
							</div>

							<!-- <div class="col-md-12">
								<div class="form-group">
									<label>Markah 3</label>
									<input type="text" name="score_3" class="form-control" placeholder="Markah 3"/>
								</div>
							</div>

							<div class="col-md-12">
								<div class="form-group">
									<label>Markah 4</label>
									<input type="text" name="score_4" class="form-control" placeholder="Markah 4"/>
								</div>
							</div> -->
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default flat" data-dismiss="modal"><?= lang('cancel') ?></button>
					<button type="submit" class="btn btn-success flat"><?= lang('save') ?></button>
				</div>
			</form>
		</div>
	</div>
</div>