<script type="text/javascript">
	var alert = "<?=$alert?>";
</script>
<div class="col-md-12">
    <div class="col-md-12 table-responsive">
		<div class="alert alert-info alert-dismissable hide">
		    <i class="fa fa-info"></i>
		    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		    <?= $alert ?>
		</div>

		<div class="row">
			<div class="form-group col-md-6">
				<label class="col-md-3"><?= lang('filter_by_status') ?></label>
				<div class="col-md-3">
					<select id="dt-filter-status" class="form-control">
						<option>All</option>
						<?php foreach ($status as $key => $value) { ?>
								<option><?= $value['name'] ?></option>
						<?php } ?>
					</select>
				</div>
			</div>
		</div>
		
		<form id="form-business-plan-list" action="<?= base_url().PATH_TO_ADMIN.'applications/list_submit_handler' ?>" method="post">
			<table id="table-business-plan" class="table table-bordered table-striped table-dark">
			    <thead>
				    <tr>
						<th>No</th>
						<th><?= lang('innovation_product') ?></th>
						<th><?= lang('description') ?></th>
						<th><?= lang('score') ?></th>
						<th><?= lang('status') ?></th>
						<th><?= lang('action') ?></th>
				    </tr>
			    </thead>
			    <tbody>
			    <?php $no = 1; foreach ($business_plans as $key=>$value) { ?>
				<tr>
				    <td><?= $no ?></td>
				    <td><?= $value['title'] ?></td>
				    <td><?= ellipsis($value['description'], 100) ?></td>
				    <td><?= $value['score'] != NULL ? $value['score'] : "-" ?></td>
				    <td class="<?= $status[$value['status']]['class'] ?>"><?= $status[$value['status']]['name'] ?></td>
				    <td>
				    	<input type="hidden" name="action">
						<button type="button" class="btn btn-default btn-action btn-view" title="<?= lang('view_innovation_detail') ?>" data-id="<?= $value['id'] ?>">
						    <span class="fa fa-search fa-lg"></span>
						</button>

						<button type="button" class="btn btn-default btn-action btn-bp-evaluation" title="<?= lang('view_innovation_detail') ?>" data-id="<?= $value['id'] ?>" data-title="<?= $value['title'] ?>" data-first-score="<?= $value['first_score'] ?>">
						    <span class="fa fa-edit fa-lg"></span>
						</button>

						<button type="button" class="btn btn-default btn-action btn-download-pdf" title="<?= lang('download_pdf') ?>" data-id="<?= $value['id'] ?>">
						    <span class="fa fa-download fa-lg"></span> 
						</button>
				    </td>
				</tr>
			    <?php $no++;} ?>
			    </tbody>
			</table>

			<?php /*if(count($business_plans) > 0){ ?>
				<div class="form-group">
					<label><?= lang('with_selected')." : " ?></label>
					<input type="submit" value="<?= lang('proceed_to_next_round') ?>" data-id="btn-proceed" class="btn btn-success">
				</div>
			<?php }*/ ?>
		</form>
    </div>
    <?= $form_view; ?>
</div>