<script type="text/javascript">
	var lang_delete = "<?= lang('delete') ?>",
		lang_delete_group = "<?= lang('delete_group') ?>",
		lang_cancel = "<?= lang('cancel') ?>",
		lang_delete_confirm_message = "<?= lang('delete_confirm_message') ?>";
</script>
<div class="col-md-12">
    <div class="col-md-12 text-center">
		<h2><?= lang('form_group') ?></h2>
    </div>
    <div class="col-md-8 col-md-offset-2 content-box">
		<form role="form" class="form-horizontal" id="form-group" action="<?= site_url(PATH_TO_ADMIN.'group_experts/'.$form_action.'/') ?>" method="POST">
			<input type="hidden" name="id" value="<?= isset($group) ? $group['id'] : '' ?>" />
				<div class="form-group">
					<label class="col-md-3 control-label"><?= lang('group_name') ?></label>
					<div class="col-md-9">
						<input type="text" name="name" class="form-control" value="<?= isset($group) ? $group['name'] : '' ?>">
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label"><?= lang('group_description') ?></label>
					<div class="col-md-9">
						<textarea class="form-control" name="description"><?= isset($group) ? $group['description'] : '' ?></textarea>
					</div>
				</div>						
				
				<div class="form-group text-center">
					<button type="button" class="btn btn-default flat btn-cancel"><?= lang('cancel') ?></button>
					<button type="submit" class="btn btn-success flat"><?= lang('save') ?></button>
				</div>
			</form>
    </div>
</div>