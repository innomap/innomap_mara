<div class="col-xs-12 col-md-12 container">
	<div class="col-xs-12 col-md-8 col-md-offset-2">
		<div class="col-xs-12 col-md-12 text-center">
			<div class="col-xs-6 col-md-6 text-left link-logo">
				<a href="<?= base_url() ?>">
					<img src="<?= base_url().ASSETS_IMG."logo_maratex.png" ?>" width="80%" class="padding-top-10">
				</a>
			</div>
			<div class="col-xs-6 col-md-6 text-right link-logo">
				<a href="<?= base_url() ?>">
					<img src="<?= base_url().ASSETS_IMG."logo-aim-2016.png" ?>" width="40%">
				</a>
			</div>
			<div class="col-xs-12 col-md-12">
				<h2><b>Pertandingan</b></h2>
			</div>
		</div>
		
		<div class="col-xs-12 col-md-12 login-box bg-grey-2">
			<p>2 KATEGORI </p>
			<p>
				<ol type="1">
					<li><b>KATEGORI PERKHIDMATAN:</b>
						<ol type="a">
							<li>Tadbir Urus Korporat </li>
							<li>Idea Perniagaan</li>
							<li>Pendidikan dan Latihan</li>
						</ol>
						<p>Penyertaan terbuka kepada <b>kumpulan warga kerja MARA dan subsidiari</b> sahaja</p>
					</li>
					<li><b>KATEGORI PRODUK:</b>
						<ol type="a">
							<li>Sains dan Teknologi Hijau</li>
							<li>Tenaga, Elektronik & Telekomunikasi</li>
							<li>ICT & Multimedia</li>
							<li>Rekacipta & Rekabentuk</li>
						</ol>
						<p>Penyertaan dibuka kepada * Peringkat Warga Kerja *Peringkat Komuniti * Peringkat Sekolah</p>
					</li>
				</ol>

				<p>Terdapat dua kategori anugerah yang dipertandingkan iaitu Kategori Perkhidmatan dan Kategori Produk.</p>
				<ol type="1">
					<li><b>KATEGORI PERKHIDMATAN</b> : Penyertaan terbuka kepada kumpulan warga kerja MARA dan subsidiari MARA sahaja.  Kategori ini melibatkan tiga bidang berikut:
						<ol type="a">
							<li><b>Tadbir Urus Korporat</b>: Tadbir Urus Korporat Pusat Pentadbiran MARA  meliputi aspek-aspek seperti kemudahan dan khidmat pengurusan, sistem e-perkhidmatan, pengkomersialan dan lain-lain program-program untuk menjana pendapatan. Secara keseluruhan, projek inovasi ini diharap dapat membantu individu, kumpulan atau organisasi untuk meningkatkan mutu kerja dan kecekapan di samping memperbaiki program sedia ada dan juga sebagai asas untuk memperbaiki kelemahan yang ada. </li>
							<li><b>Idea Perniagaan</b>: Kategori ini memberi fokus kepada Idea Perniagaan yang boleh diketengahkan supaya keusahawanan adalah satu bidang kerjaya pilihan. Peserta perlu menyediakan cadangan Rancangan Perniagaan yang boleh dilaksanakan terutama  di kawasan luar bandar.</li>
							<li><b>Pendidikan dan Latihan</b>(Fokus: Idea Pendidikan Abad ke 21): Pendidikan dan latihan berfokuskan kepada idea pembelajaran abad ke 21 merupakan satu aktiviti berhubung dengan usaha bagi penyelesaian isu/masalah pendidikan abad ke 21 berkaitan kaedah pembelajaran berteraskan ICT, pentaksiran, pengurusan bilik darjah, ciri-ciri guru dan pelajar abad ke 21 dan lain-lain.</li>
						</ol>
						
					</li>
					<li><b>KATEGORI PRODUK</b> : Kategori anugerah adalah seperti di bawah:
						<ol type="a">
							<li>Kategori Produk (Peringkat WARGA KERJA): Penyertaan terbuka kepada semua warga kerja MARA dan subsidiari</li>
							<li>Kategori Produk (Peringkat KOMUNITI): Penyertaan dikhaskan kepada kumpulan pelajar IPMa iaitu GIAT, IKM, KKTM, KPM, KM, MKIC, KPTM, KUPTM, GMI dan UniKL.</li>
							<li>Kategori Produk (Peringkat SEKOLAH): Penyertaan dikhaskan kepada kumpulan pelajar MRSM sahaja.</li>
						</ol>
						<br/>
						<p>Skop Kategori Produk adalah seperti berikut:</p>
						<ol type="a">
							<li><b>Sains dan Teknologi Hijau: </b>Bidang ini meliputi pengenalan, pewujudan atau penambahbaikan sistem, produk atau kaedah teknikal / proses fizikal serta penyelesaian masalah yang menggunakan bidang pengetahuan yang tertentu seperti teknologi hijau. </li>
							<li><b>Tenaga, Elektronik & Telekomunikasi: </b>Bidang ini meliputi pewujudan atau penambahbaikan sistem, produk atau kaedah teknikal/proses fizikal serta penyelesaian masalah yang menggunakan bidang pengetahuan yang tertentu seperti tenaga, elektronik dan telekomunikasi.</li>
							<li><b>ICT & Multimedia: </b>Inovasi dalam bidang ini merangkumi aplikasi, perkakasan, kandungan tempatan dan multimedia yang kreatif. Penggunaan teknologi maklumat dalam organisasi sebagai alat untuk meningkatkan prestasi, keberkesanan dan produktiviti organisasi secara inovatif dan kreatif. Inovasi dari aspek teknikal seperti penggunaan alat yang baru, unik dengan pihak yang berkaitan. Inovasi dari segi pendekatan seperti memperkenalkan perkara-perkara baru, menjalankan benchmarking serta mendapatkan kerjasama daripada pihak lain. Teknologi yang digunakan adalah seperti <i>web based, client server, VOIP, wireless</i> dan sebagainya.</li>
							<li><b>Rekacipta & Rekabentuk: </b>Bidang ini meliputi pengenalan, pewujudan atau penambahbaikan sistem, produk atau kaedah teknikal / proses fizikal serta penyelesaian masalah yang menggunakan bidang pengetahuan yang tertentu seperti rekabentuk dan rekacipta.</li>
						</ol>
					</li>
				</ol>
			</p>

			<p><b>PELAKSANAAN</b> (Syarat-syarat penyertaan) 
				<ol type="1">
					<li>Penyertaan mestilah secara berkumpulan bagi warga kerja dan tambahan kepada kumpulan pelajar mesti disertai seorang guru / pengajar sebagai penasihat.</li>
					<li>Setiap penyertaan hendaklah disahkan oleh Pegawai Mengawal Pusat (PMP) berkenaan.</li>
					<li>Inovasi mestilah mewakili kepentingan mana-mana sektor dalam organisasi MARA atau projek secara hybrid yang menjurus kepada program kesejahteraan Rakyat Luar Bandar.</li>
					<li>Inovasi hendaklah memberi faedah yang signifikan dan bernilai tinggi kepada organisasi lain serta bermanafaat kepada masyarakat.</li>
					<li>Inovasi yang dipertandingkan <b><u>mestilah idea asal</u></b> kumpulan atau <b><u>penambahbaikan</u></b> daripada projek sedia ada. </li>
					<li>Inovasi mestilah bernilai komersial dan boleh dipasarkan.</li>
					<li>Setiap Institusi Pendidikan MARA (IPMa) adalah diwajibkan untuk menghantar sekurang-kurangnya SATU (1) penyertaan daripada kumpulan warga kerja, dan satu (1) penyertaan daripada kumpulan pelajar berkaitan.</li>
					<li>Pusat-pusat pentadbiran MARA, Bahagian-bahagian di ibu pejabat dan Subsidiari diwajibkan menghantar sekurang-kurangnya  satu (1) penyertaan dari mana-mana kategori kumpulan warga kerja .</li>
					<li>Cadangan Projek akan disaring dalam 3 peringkat.</li>
					<li>Projek yang melepasi <b>saringan kedua</b> sahaja layak untuk <b>dipertimbangkan</b> oleh Jawatankuasa bagi menerima Anugerah Hari Inovasi MARA 2016 dan Geran Pengkomersialan. </li>
					<li>Hanya penyertaan dalam program MARATeX sahaja yang layak untuk mewakili MARA dalam pertandingan dan mana-mana penganugerahan anjuran badan-badan kerajaan atau pertubuhan swasta.</li>
					<li>Semua penyertaan dan penilaian adalah secara <i>online</i> melalui laman sesawang <b>MARATeX2016.innomap.my</b>  </li>
					<li>Keputusan Panel Penilai adalah MUKTAMAD dan sebarang rayuan tidak dilayan.</li>
				</ol>
			</p>

			<p><b>PROSES PENILAIAN DAN PEMILIHAN PENERIMA ANUGERAH INOVASI</b>
				<ol type="1">
					<li>Maklumat Penyertaan: Peserta mestilah mematuhi perkara-perkara berikut:
						<ol type="a">
							<li>Penyertaan MARATeX 2016 dilaksanakan secara <i>online</i> melalui laman web MARATeX2016.innomap.my yang dibuka mulai 11 Februari 2016 selepas jam 3 petang.</li>
							<li>Penyertaan ditutup pada 11 Mac 2016 jam 12 tengah malam.  Sebarang kelewatan dari tarikh dan masa ini mengakibatkan penyertaan DITOLAK dan tidak disenaraikan dalam pangkalan data untuk penilaian seterusnya.</li>
							<li>Semua urusan penyertaan dan implikasi kos berkaitan penyertaan program MARATeX adalah tanggungjawab kumpulan dan pusat masing-masing. Ini termasuk makan minum dan penginapan semasa kehadiran di bengkel/kolokium/simposium/konferen anjuran MARATeX.</li>
							<li>Penyertaan adalah PERCUMA tanpa dikenakan tanpa yuran pendaftaran serta had bilangan maksima.</li>
						</ol>
					</li>
				</ol>
			</p>

			<p><b>MAKLUMAT PENYERTAAN</b><br>Peserta mestilah mematuhi perkara-perkara berikut:
				<ol type="1">
					<li>Penyertaan MARATeX 2016 adalah melalui laman web MARATeX2016.innomap.my sahaja yang akan dibuka mulai 11 Februari 2016 selepas jam 3 petang dan penyertaan akan ditutup pada 11 Mac 2016 jam 12 tengah malam. Sebarang kelewatan dari tarikh dan masa ini mengakibatkan penyertaan DITOLAK dan tidak disenaraikan dalam pangkalan data untuk penilaian seterusnya.</li>
					<li>Semua urusan penyertaan dan implikasi kos berkaitan penyertaan program MARATeX adalah tanggung jawab kumpulan dan pusat masing-masing. Ini termasuk makan minum dan penginapan SEMASA PERJALANAN PERGI BALIK dari bengkel/ kolokium/ simposium/  konferen anjuran MARATeX. UNI hanya menanggung makan-minum <b><u>semasa kehadiran</u></b> di bengkel/kolokium/simposium/konferen anjuran MARATeX sahaja.</li>
					<li>Setiap penyertaan adalah PERCUMA tanpa dikenakan apa-apa yuran pendaftaran serta had bilangan maksima.</li>
					<li>Tarikh TENTATIF aktiviti-aktiviti MARATeX 2016 :
						<ol type="a">
							<li>Kolokium MARATeX 2016: (Bengkel Penulisan) (29 - 31 Mac 2016) </li>
							<li>Simposium MARATeX 2016 (Pembentangan <i>Project Defence/Pitching</i>) (19 – 21 April 2016)</li>
							<li>Bengkel Pra Pengkomersialan 19 – 20 Mei 2016</li>
							<li>Konferen MARATeX 2016 (Penilaian Dapatan) 26 -28 Julai 2016 </li>
							<li>Bengkel <i>Capturing The Market & Pitching To The Industry</i>  (23 - 24 Ogos 2016) </li>
							<li>Bengkel <i>Patent Drafting</i> & Penulisan IP (27 – 28 September 2016) </li>
							<li>Hari Inovasi MARA 2016  (26 & 27 Oktober 2016)</li>
						</ol>
					</li>
				</ol>
			</p>

			<p><b>ANUGERAH INOVASI MARA 2016</b>
				<ol type="1">
					<li>Enam (6) kategori anugerah yang dipertandingkan iaitu : 
						<ol type="a">
							<li>Anugerah Kategori Perkhidmatan (Tadbir Urus Korporat)</li>
							<li>Anugerah Kategori Perkhidmatan (Idea Perniagaan)</li>
							<li>Anugerah Kategori Perkhidmatan (Pendidikan & Latihan)</li>
							<li>Anugerah Kategori Produk (Warga Kerja)</li>
							<li>Anugerah Kategori Produk (Komuniti)</li>
							<li>Anugerah Kategori Produk (Sekolah)</li>
						</ol>
					</li>
					<li>Tiga (3) pemenang terbaik bagi setiap kategori akan  menerima hadiah berupa wang tunai, plak dan sijil penghargaan.</li>
					<li>Satu hadiah keseluruhan terbaik iaitu <b>Anugerah Inovasi Ketua Pengarah MARA</b> akan disediakan berupa wang tunai, plak dan sijil penghargaan.</li>
				</ol>
			</p>

			<p><b>AGIHAN IMBUHAN & HADIAH:</b><br/>Pembahagian hadiah wang tunai antara pusat dan pereka  adalah seperti berikut:
				<ol type="i">
					<li>Pusat – 30% daripada nilai hadiah wang tunai yang dimenangi.</li>
					<li>Pereka – 70% daripada nilai hadiah wang tunai yang dimenangi.</li>
				</ol>
			<p>

			<p><b>PROSES PENILAIAN DAN PEMILIHAN PENYERTAAN: <br/>ANUGERAH INOVASI MARA DALAM MARATeX 2016</b>
				<ol type="1">
					<li><b>PERINGKAT PERTAMA : Penilaian Berasaskan Laporan/Cadangan Projek: </b><p>Semua laporan cadangan projek yang diterima akan dinilai oleh Konsultan yang dilantik. Penilaian akan dilaksanakan berdasar kriteria penilaian yang telah ditetapkan.  Pada peringkat ini, projek inovasi akan disenarai pendek untuk penilaian peringkat kedua.</p></li>
					<li><b>PERINGKAT KEDUA: Penilaian Inovasi Yang Disenarai Pendek Berasaskan Taklimat Dan Demonstrasi Serta <i>Project Defence/Pitching</i></b><p>Inovasi yang disenarai pendek oleh Konsultan di peringkat kedua akan dijemput untuk memberi taklimat dan demonstrasi serta <i>project defence/pitching</i> mengenai projek inovasi masing-masing dalam Simposium MARATeX 2016. Taklimat ini bertujuan untuk membolehkan Konsultan mendapat maklumat terperinci, penjelasan lanjut dan membuat pengesahan ke atas pematuhan kriteria penilaian yang ditetapkan. </p></b></li>
					<li><b>PERINGKAT KETIGA : Pemilihan Projek Inovasi Terbaik</b><br/><p>Pembentangan dapatan dan prototaip projek untuk dinilai oleh Panel dalam Konferen MARATeX bagi memilih projek inovasi terbaik sebagai Finalis Anugerah Inovasi MARA dan Anugerah KIK MARA 2016.</p></li>
					<li>Sebarang pertanyaan boleh ditujukan kepada :<br/>
						<p>
							<b>URUS SETIA</b><br/>
							<b>MARATEX 2016</b><br/>
							<b>UNIT INOVASI DAN PENYELIDIKAN MARA</b><br/>
							<b>TINGKAT 9, IBU PEJABAT MARA</b><br/>
							<b>JALAN RAJA LAUT, 50609,</b><br/>
							<b>Kuala Lumpur</b><br/>
							<b>Faks: 03 2691 0486</b><br/>
						</p>
					</li>
				</ol>
			</p>

			<p>Atau, berhubung terus dengan senarai pegawai dibawah:
				<ol type="1">
					<li>Encik Rosidi bin Ishak<br/>No Tel 03 2613 4360<br/>E-mel: rosidi@mara.gov.my</li>
					<li>Puan Mazinah Joary binti Mohammed Tajudin<br/>No Tel 03 2613 4375<br/>E-mel: mazinah@mara.gov.my</li>
					<li>Puan Khairiah binti Abdullah<br/>No Tel 03 2613 4457<br/>E-mel: khairiah.abdullah@mara.gov.my</li>
					<li>Puan Hanirus binti Osman<br/>No Tel 03 2613 4463<br/>E-mel: hanirus@mara.gov.my</li>
					<li>Puan Sundarambal a/p Gengatharan<br/>No Tel 03 2613 5052<br/>E-mel: sundarambal@mara.gov.my</li>
				</ol>
			</p>

			<div class="col-md-4 text-center margin-top-20">
				<a href="<?= base_url().'assets/attachment/'.USER_GUIDE_FILENAME ?>" class="btn btn-primary" target="_blank">Panduan Penggunaan Sistem</a>
			</div>
		</div>
	</div>
</div>

