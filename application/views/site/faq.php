<div class="col-xs-12 col-md-12 container">
	<div class="col-xs-12 col-md-8 col-md-offset-2">
		<div class="col-xs-12 col-md-12 text-center">
			<div class="col-xs-6 col-md-6 text-left link-logo">
				<a href="<?= base_url() ?>">
					<img src="<?= base_url().ASSETS_IMG."logo_maratex.png" ?>" width="80%" class="padding-top-10">
				</a>
			</div>
			<div class="col-xs-6 col-md-6 text-right link-logo">
				<a href="<?= base_url() ?>">
					<img src="<?= base_url().ASSETS_IMG."logo-aim-2016.png" ?>" width="40%">
				</a>
			</div>
			<div class="col-xs-12 col-md-12">
				<h2><b>Soalan Lazim</b></h2>
			</div>
		</div>
		
		<div class="col-xs-12 col-md-12 login-box bg-grey-2">
			<ol class="faq-list" type="1">
				<li><b>Apakah MARATeX 2016?</b><br/>MARATeX 2016 adalah platform untuk penyertaan warga MARA bagi Anugerah Inovasi MARA (AIM) 2016 dan pertandingan-pertandingan inovasi peringkat dalam dan luar negara. MARATeX 2016 ini  adalah program Unit Inovasi Dan Penyelidikan MARA dengan kerjasama Yayasan Inovasi Malaysia (YIM).</li>
				<li><b>Bilakah pendaftaran MARATeX bermula?</b><br/>Penyertaan MARATeX 2016 dibuka secara <i>online</i> pada 11 Februari 2016 selepas 3 petang.</li>
				<li><b>Bilakah pendaftaran MARATeX berakhir?</b><br/>Penyertaan MARATeX 2016 <u>ditutup</u> pada 11 Mac 2016 jam 12 tengah malam.</li>
				<li><b>Apakah aktiviti yang ditawarkan dalam MARATEx 2016 ini?</b><br/>
					<ol type="a">
						<li>Bengkel Pengurusan MARATeX (17 Februari 2016)</li>
						<li>Kolokium MARATeX 2016: (Bengkel Penulisan) (22 - 24 Mac 2016)</li>
						<li>Simposium MARATeX 2016 (Pembentangan Project Defence/Pitching) (19 – 21 April 2016)</li>
						<li>Bengkel Pra Pengkomersialan 19 – 20 Mei 2016 </li>
						<li>Konferen MARATeX 2016 (Penilaian Dapatan) 26 -28 Julai 2016</li>
						<li>Bengkel <i>Capturing The Market & Pitching To The Industry</i>  (23 - 24 Ogos 2016) </li>
						<li>Bengkel <i>Patent Drafting</i> & Penulisan IP (27 – 28 September 2016)</li>
						<li>Hari Inovasi MARA 2016  (26 & 27 Oktober 2016)</li>
					</ol>
				</li>
				<li><b>Apakah kategori penyertaan dalam MARATeX? Siapakah yang boleh menyertainya?</b><br/>
					<ol type="1">
						<li><b>KATEGORI PERKHIDMATAN:</b>
							<ol type="a">
								<li>Tadbir Urus Korporat</li>
								<li>Idea Perniagaan</li>
								<li>Pendidikan dan Latihan</li>
							</ol>
							<p>*** Penyertaan terbuka kepada <b>kumpulan warga kerja MARA dan subsidiari</b> MARA sahaja</p>
						</li>
						<li><b>KATEGORI PRODUK:</b>
							<ol type="a">
								<li>Sains dan Teknologi Hijau</li>
								<li>Tenaga, Elektronik & Telekomunikasi</li>
								<li>ICT & Multimedia</li>
								<li>Rekacipta & Rekabentuk</li>
							</ol>
							<p>*** Penyertaan dibuka kepada Peringkat Warga Kerja, Peringkat Komuniti  dan Peringkat Sekolah</p>
						</li>
					</ol>
				</li>
				<li><b>Siapakah yang boleh menyertai MARATeX 2016 ini?</b><br/>
					<ol type="1">
						<li><b>KATEGORI PERKHIDMATAN</b>
							<p>Penyertaan terbuka kepada <b>kumpulan warga kerja MARA dan subsidiari MARA</b> sahaja</p>
						</li>
						<li><b>KATEGORI PRODUK: 
							<p>Peringkat WARGA KERJA: Penyertaan terbuka kepada semua warga kerja MARA dan subsidiari</p>
							<p>Peringkat KOMUNITI: Penyertaan dikhaskan kepada kumpulan pelajar IPMa iaitu GIAT, IKM, KKTM, KPM, KM, MKIC, KPTM, KUPTM, GMI dan UniKL.</p>
							<p>Peringkat SEKOLAH: Penyertaan dikhaskan kepada kumpulan pelajar MRSM sahaja </p>
						</b></li>
					</ol>
				</li>
				<li><b>Apakah yang dimaksudkan dengan bidang-bidang pada kategori perkhidmatan?</b><br/>
					<ol type="a">
						<li><b>Tadbir Urus Korporat :</b> Tadbir Urus Korporat Pusat Pentadbiran MARA  meliputi aspek-aspek seperti kemudahan dan khidmat pengurusan, sistem e-perkhidmatan, pengkomersialan dan lain-lain program-program untuk menjana pendapatan. Secara keseluruhan, projek inovasi ini diharap dapat membantu individu, kumpulan atau organisasi untuk meningkatkan mutu kerja dan kecekapan di samping memperbaiki program sedia ada dan juga sebagai asas untuk memperbaiki kelemahan yang ada.</li>
						<li><b>Idea Perniagaan:</b> Kategori ini memberi fokus kepada Idea Perniagaan yang boleh diketengahkan supaya keusahawanan adalah satu bidang kerjaya pilihan. Peserta perlu menyediakan cadangan Rancangan Perniagaan yang boleh dilaksanakan terutama di kawasan luar bandar.</li>
						<li><b>Pendidikan dan Latihan</b> (Fokus: Idea Pendidikan Abad ke 21): Pendidikan dan latihan berfokuskan kepada idea pembelajaran abad ke 21 merupakan satu aktiviti berhubung dengan usaha bagi penyelesaian isu/masalah pendidikan abad ke 21 berkaitan kaedah pembelajaran berteraskan ICT, pentaksiran, pengurusan bilik darjah, ciri-ciri guru dan pelajar abad ke 21 dan lain-lain.</li>
					</ol>
				</li>
				<li><b>Apakah yang dimaksudkan dengan skop pada kategori produk?</b><br/>
					<ol type="a">
						<li><b>Sains & Teknologi Hijau</b>: Bidang ini meliputi pengenalan, pewujudan atau penambahbaikan sistem, produk atau kaedah teknikal / proses fizikal serta penyelesaian masalah yang menggunakan bidang pengetahuan yang tertentu seperti teknologi hijau. </li>
						<li><b>Tenaga, Elektronik & Telekomunikasi</b>: Bidang ini meliputi pewujudan atau penambahbaikan sistem, produk atau kaedah teknikal/proses fizikal serta penyelesaian masalah yang menggunakan bidang pengetahuan yang tertentu seperti tenaga, elektronik dan telekomunikasi.</li>
						<li><b>ICT & Multimedia</b>: Inovasi dalam bidang ini merangkumi aplikasi, perkakasan, kandungan tempatan dan multimedia yang kreatif. Penggunaan teknologi maklumat dalam organisasi sebagai alat untuk meningkatkan prestasi, keberkesanan dan produktiviti organisasi secara inovatif dan kreatif. Inovasi dari aspek teknikal seperti penggunaan alat yang baru, unik dengan pihak yang berkaitan. Inovasi dari segi pendekatan seperti memperkenalkan perkara-perkara baru, menjalankan benchmarking serta mendapatkan kerjasama daripada pihak lain. Teknologi yang digunakan adalah seperti <i>web based, client server, VOIP, wireless</i> dan sebagainya.</li>
						<li><b>Rekacipta & Rekabentuk</b>: Bidang ini meliputi pengenalan, pewujudan atau penambahbaikan sistem, produk atau kaedah teknikal / proses fizikal serta penyelesaian masalah yang menggunakan bidang pengetahuan yang tertentu seperti rekabentuk dan rekacipta. </li>
					</ol>
				</li>
				<li><b>Apakah syarat-syarat penyertaan MARATeX 2016 ini?</b><br/>
					Syarat-syarat penyertaan MARATeX 2016 adalah seperti berikut:
					<ol type="1">
						<li>Penyertaan mestilah secara berkumpulan bagi warga kerja dan tambahan kepada kumpulan pelajar mesti disertai seorang guru / pengajar sebagai penasihat.</li>
						<li>Setiap penyertaan hendaklah disahkan oleh Pegawai Mengawal Pusat (PMP) berkenaan.</li>
						<li>Hasilan inovasi mestilah mewakili kepentingan mana-mana sektor dalam organisasi MARA atau projek secara hybrid yang menjurus kepada program kesejahteraan Rakyat Luar Bandar.</li>
						<li>Hasilan inovasi hendaklah memberi faedah yang signifikan dan bernilai tinggi kepada organisasi lain serta bermanafaat kepada masyarakat.</li>
						<li>Projek yang dipertandingkan mestilah idea asal kumpulan atau penambahbaikan daripada projek sedia ada.</li>
						<li>Projek mestilah bernilai komersial dan boleh dipasarkan.</li>
						<li>Setiap Institusi Pendidikan MARA (IPMa) adalah diwajibkan untuk menghantar sekurang-kurangnya SATU (1) penyertaan daripada kumpulan warga kerja, dan satu (1) penyertaan daripada kumpulan pelajar berkaitan.</li>
						<li>Pusat-pusat pentadbiran MARA, Bahagian-bahagian di ibu pejabat dan Subsidiari diwajibkan menghantar sekurang-kurangnya  satu (1) penyertaan dari mana-mana kategori kumpulan warga kerja .</li>
						<li>Setiap penyertaan akan disaring dalam 3 peringkat oleh barisan panel yang dilantik. Hanya penyertaan yang berjaya melepasi saringan bagi setiap peringkat sahaja yang akan dihubungi untuk ke peringkat seterusnya.</li>
						<li>Hanya senarai finalis sahaja layak untuk <b>dipertimbangkan</b> bagi menerima  Anugerah Inovasi MARA (AIM) 2016,  Geran MARATeX dan penyertaan lain yang diuruskan oleh MARATeX bersama agensi atau pertubuhan luar. </li>
						<li>Hanya penyertaan dalam program MARATeX sahaja yang layak untuk mewakili MARA dalam pertandingan dan mana-mana penganugerahan anjuran badan-badan kerajaan atau pertubuhan swasta.</li>
						<li>Semua penyertaan dan penilaian adalah secara online melalui laman sesawang MARATeX2016.innomap.my  </li>
						<li>Keputusan Panel Penilai adalah MUKTAMAD dan sebarang rayuan tidak dilayan.</li>
					</ol>
				</li>
				<li><b>Bagaimanakah penyertaan dan penilaian dilakukan untuk MARATeX 2016 ini?</b><br/>
					<p>Semua penyertaan dan penilaian adalah secara <i>online</i> melalui laman sesawang <b>MARATeX2016.innomap.my</b></p></li>
				<li><b>Perlukan Pengawai Mengawal Pusat(PMP) membuat pengesahan penyertaan MARATeX?</b><br/>
					<p>Ya. Sila muat-turun BORANG PENGESAHAN PRGAWAI MENGAWAL PUSAT (PMP) yang disediakan pada borang penyertaan MARATeX; dan sila muatnaik BORANG kembali yang sudah lengkap pada ruang yang disediakan pada Borang Pendaftaran MARATeX.</p>
				</li>
				<li><b>Bolehkah saya save dan simpan penyertaan online saya buat beberapa ketika sebelum menghantar (submit)?</b><br/>
					<p>Ya boleh. Sebarang pindaan boleh dibuat sehingga tarikh tutup penyertaan pada 11 Mac 2016.</p>
				</li>
				<li><b>Apakah antara perkara lain yang perlu diberi perhatian oleh peserta MARATex 2016 ini?</b><br/>
					<ol type="a">
						<li>Semua urusan penyertaan dan implikasi kos berkaitan penyertaan program MARATeX adalah tanggung jawab kumpulan dan pusat masing-masing. Ini termasuk makan minum dan penginapan SEMASA PERJALANAN PERGI BALIK dari bengkel/ kolokium/ simposium/  konferen anjuran MARATeX. UNI hanya menanggung makan-minum <b><u>semasa kehadiran</u></b> di bengkel/kolokium/simposium/konferen anjuran MARATeX sahaja.</li>
						<li>Penyertaan adalah PERCUMA tanpa dikenakan tanpa yuran pendaftaran serta had bilangan maksima. </li>
					</ol>
				</li>
				<li><b>Jika saya terpilih, apakah proses penilaian yang seterusnya perlu saya ambil tahu?</b><br/>
					<ol type="1">
						<li><b>PERINGKAT PERTAMA : Penilaian Berasaskan Laporan/Cadangan Projek: </b>
							<p>Semua laporan cadangan projek yang diterima akan dinilai oleh Konsultan yang dilantik. Penilaian akan dilaksanakan berdasar kriteria penilaian yang telah ditetapkan.  Pada peringkat ini, projek inovasi akan disenarai pendek untuk penilaian peringkat kedua.</p>
						</li>
						<li><b>PERINGKAT KEDUA: Penilaian Inovasi Yang Disenarai Pendek Berasaskan Taklimat Dan Demonstrasi Serta Project Defence/Pitching</b>
							<p>Inovasi yang disenarai pendek oleh Konsultan di peringkat kedua akan dijemput untuk memberi taklimat dan demonstrasi serta project defence/pitching mengenai projek inovasi masing-masing dalam Simposium MARATeX 2016. Taklimat ini bertujuan untuk membolehkan Konsultan mendapat maklumat terperinci, penjelasan lanjut dan membuat pengesahan ke atas pematuhan kriteria penilaian yang ditetapkan.</p>
						</li>
						<li><b>PERINGKAT KETIGA : Pemilihan Projek Inovasi Terbaik</b>
							<p>Pembentangan dapatan dan prototaip projek untuk dinilai oleh Panel dalam Konferen MARATeX bagi memilih projek inovasi terbaik sebagai Finalis Anugerah Inovasi MARA dan Anugerah KIK MARA 2016</p>
						</li>
					</ol>
				</li>
				<li><b>Bolehkah produk yang sama masuk kategori yang berbeza?</b><br/>
					<p>TIDAK BOLEH.</p><p>Produk yang sama TIDAK DIBENARKAN untuk menyertai kategori yang berbeza.</p>
				</li>
				<li><b>Apakah Anugerah Inovasi MARA (AIM) yang ditawarkan dalam MARATeX 2016 ini?</b><br/>
					<ol type="1">
						<li>Enam (6) kategori anugerah yang dipertandingkan iaitu : 
							<ol type="a">
								<li>Anugerah Kategori Perkhidmatan (Tadbir Urus Korporat)</li>
								<li>Anugerah Kategori Perkhidmatan (Idea Perniagaan)</li>
								<li>Anugerah Kategori Perkhidmatan (Pendidikan & Latihan)</li>
								<li>Anugerah Kategori Produk (Warga Kerja)</li>
								<li>Anugerah Kategori Produk (Komuniti)</li>
								<li>Anugerah Kategori Produk (Sekolah)</li>
							</ol>
						</li>
						<li>Tiga pemenang terbaik bagi setiap kategori akan  menerima hadiah berupa wang tunai, plak dan sijil penghargaan.</li>
						<li>Satu hadiah keseluruhan terbaik iaitu <b>Anugerah Inovasi Ketua Pengarah MARA</b> akan disediakan berupa wang tunai, plak dan sijil penghargaan.</li>
						<li>Pembahagian hadiah wang tunai antara pusat dan pereka  adalah seperti berikut:
							<ol type="i">
								<li>Pusat – 30% daripada nilai hadiah wang tunai yang dimenangi.</li>
								<li>Pereka – 70% daripada nilai hadiah wang tunai yang dimenangi.</li>
							</ol>
						</li>
					</ol>
				</li>
				<li><b>Bagaimanakah agihan dibuat pada imbuhan dan hadiah yang dimenangi dalam AIM 2016?  </b><br/>
					<p>Pembahagian hadiah wang tunai antara pusat dan pereka  adalah seperti berikut:
						<ol type="i">
							<li>Pusat – 30% daripada nilai hadiah wang tunai yang dimenangi.</li>
							<li>Pereka – 70% daripada nilai hadiah wang tunai yang dimenangi.</li>
						</ol>
					</p>
				</li>
				<li><b>Siapakah yang boleh dihubungi jika saya masih mempunyai soalan lain yang tidak tersenarai di sini?</b><br/>
					<p>
						<b>URUS SETIA</b><br/>
						MARATEX 2016<br/>
						UNIT INOVASI DAN PENYELIDIKAN MARA<br/>
						TINGKAT 9, IBU PEJABAT MARA<br/>
						JALAN RAJA LAUT, 50609,<br/>
						Kuala Lumpur<br/>
						Faks: 03 2691 0486<br/>
					</p>

					<p>Atau, berhubung terus dengan senarai pegawai dibawah:
						<ol type="1">
							<li>Encik Rosidi bin Ishak<br/>No Tel 03 2613 4360<br/>E-mel: rosidi@mara.gov.my</li>
							<li>Puan Mazinah Joary binti Mohammed Tajudin<br/>No Tel 03 2613 4375<br/>E-mel: mazinah@mara.gov.my</li>
							<li>Puan Khairiah binti Abdullah<br/>No Tel 03 2613 4457<br/>E-mel: khairiah.abdullah@mara.gov.my</li>
							<li>Puan Hanirus binti Osman<br/>No Tel 03 2613 4463<br/>E-mel: hanirus@mara.gov.my</li>
							<li>Puan Sundarambal a/p Gengatharan<br/>No Tel 03 2613 5052<br/>E-mel: sundarambal@mara.gov.my</li>
						</ol>
					</p>
				</li>

			</ol>

			<div class="col-md-4 text-center margin-top-20">
				<a href="<?= base_url().'assets/attachment/'.USER_GUIDE_FILENAME ?>" class="btn btn-primary" target="_blank">Panduan Penggunaan Sistem</a>
			</div>
		</div>
	</div>
</div>

