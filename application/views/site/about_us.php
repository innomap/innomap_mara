<div class="col-xs-12 col-md-12 container">
	<div class="col-xs-12 col-md-8 col-md-offset-2">
		<div class="col-xs-12 col-md-12 text-center">
			<div class="col-xs-6 col-md-6 text-left link-logo">
				<a href="<?= base_url() ?>">
					<img src="<?= base_url().ASSETS_IMG."logo_maratex.png" ?>" width="80%" class="padding-top-10">
				</a>
			</div>
			<div class="col-xs-6 col-md-6 text-right link-logo">
				<a href="<?= base_url() ?>">
					<img src="<?= base_url().ASSETS_IMG."logo-aim-2016.png" ?>" width="40%">
				</a>
			</div>
			<div class="col-xs-12 col-md-12">
				<h2><b>Pengenalan</b></h2>
			</div>
		</div>
		
		<div class="col-xs-12 col-md-12 login-box bg-grey-2">
			<p><big><b>MARATeX 2016</b></big> adalah platform untuk penyertaan warga MARA bagi <big><b>Anugerah Inovasi MARA (AIM) 2016</b></big> dan pertandingan-pertandingan inovasi peringkat dalam dan luar negara. MARATeX 2016 ini adalah program Unit Inovasi Dan Penyelidikan MARA dengan kerjasama Yayasan Inovasi Malaysia (YIM).</p>
			<p><big><b>Objektif</b></big> pelaksanaan MARATeX 2016 adalah untuk mengiktiraf usaha inovasi dan kreativiti warga MARA yang menjana idea, ciptaan dan produk baru dalam sains dan teknologi yang menyumbang kepada pembangunan ekonomi MARA amnya dan untuk negara Malaysia khususnya.</p>
			<p><big><b>Hari Inovasi MARA</b></big> merupakan acara kemuncak tahunan MARA dalam mengiktiraf inovasi hasil kerja warganya disamping penyaluran geran penyelidikan SGPIM dalam membantu penyelidik menghasilkan prototaip dan cadangan dapatan untuk membantu penambahbaikan aliran proses kerja berhubungkait dengan program-program utama MARA.</p>
			<p>Perlaksanaan program ini sebelumnya adalah secara berasingan berdasarkan penglibatan MARA melalui jemputan yang dikeluarkan oleh badan-badan kerajaan dan swasta atau pun pertandingan yang disertai oleh MARA. Ini menyebabkan MARA tidak bersedia sepenuhnya untuk setiap penyertaan tersebut atau tidak dapat menghantar penyertaan kerana kesuntukan masa.</p>
			<p>Dalam usaha untuk memantapkan lagi perlaksanaan AIM 2016, semua program-program yang berkaitan inovasi akan menggunakan jenama MARATeX serta penyertaannya akan dilaksanakan lebih awal dengan <b>kerjasama Yayasan Inovasi Malaysia (YIM) di bawah anak syarikatnya, YIM Technology Resources Sdn Bhd (YTR)</b> melalui pendaftaran atau permohonan penyertaan serta penilaian secara <i>on-line</i>.</p>
		</div>
	</div>
</div>

