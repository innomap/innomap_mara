<div class="col-xs-12 col-md-12 container">
	<div class="col-xs-12 col-md-8 col-md-offset-2">
		<div class="col-xs-12 col-md-12 text-center">
			<div class="col-xs-6 col-md-6 text-left link-logo">
				<a href="<?= base_url() ?>">
					<img src="<?= base_url().ASSETS_IMG."logo_maratex.png" ?>" width="80%" class="padding-top-10">
				</a>
			</div>
			<div class="col-xs-6 col-md-6 text-right link-logo">
				<a href="<?= base_url() ?>">
					<img src="<?= base_url().ASSETS_IMG."logo-aim-2016.png" ?>" width="40%">
				</a>
			</div>
			<div class="col-xs-12 col-md-12">
				<h2><b>Berita</b></h2>
			</div>
		</div>
		
		<div class="col-xs-12 col-md-12 login-box bg-grey-2">
			<p> <b>BERITA MARATeX 2016</b>
				<ol type="1">
					<li><b>Penyertaan MARATeX 2016 <u>dibuka</u></b> secara online pada <b>11 Februari 2016 selepas 3 petang.</b></li>
					<li><b>BENGKEL PENGURUSAN INOVASI MARATeX</b> pada <b>17 Februari 2016.</b></li>
					<li><b>Penyertaan MARATeX 2016 <u>ditutup</u></b> pada <b>11 Mac 2016 jam 12 tengah malam.</b></li>
				</ol>
			</p>

			<p> <b>JADUAL PELAKSANAAN MARATEX 2016 ADALAH SEPERTI TARIKH TENTATIF BERIKUT :</b>
				<ol type="1">
					<li>Kolokium MARATeX 2016: (Bengkel Penulisan) (29 - 31 Mac 2016) </li>
					<li>Simposium MARATeX 2016 (Pembentangan <i>Project Defence/Pitching</i>) (19 – 21 April 2016)</li>
					<li>Bengkel Pra Pengkomersialan 19 – 20 Mei 2016</li>
					<li>Konferen MARATeX 2016 (Penilaian Dapatan) 26 -28 Julai 2016 </li>
					<li>Bengkel <i>Capturing The Market & Pitching To The Industry</i>  (23 - 24 Ogos 2016) </li>
					<li>Bengkel <i>Patent Drafting</i> & Penulisan IP (27 – 28 September 2016) </li>
					<li>Hari Inovasi MARA 2016  (26 & 27 Oktober 2016)</li>
				</ol>
			</p>
		</div>
	</div>
</div>

