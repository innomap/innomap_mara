<div class="col-xs-12 col-md-12 container">
	<div class="col-xs-12 col-md-8 col-md-offset-2">
		<div class="col-xs-12 col-md-12 text-center">
			<div class="col-xs-6 col-md-6 text-left link-logo">
				<a href="<?= base_url() ?>">
					<img src="<?= base_url().ASSETS_IMG."logo_maratex.png" ?>" width="80%" class="padding-top-10">
				</a>
			</div>
			<div class="col-xs-6 col-md-6 text-right link-logo">
				<a href="<?= base_url() ?>">
					<img src="<?= base_url().ASSETS_IMG."logo-aim-2016.png" ?>" width="40%">
				</a>
			</div>
			<div class="col-xs-12 col-md-12">
				<h2><b>Hubungi Sekretariat</b></h2>
			</div>
		</div>
		
		<div class="col-xs-12 col-md-12 login-box bg-grey-2">
			<p>
				<b>URUS SETIA</b><br/>
				<b>MARATEX 2016</b><br/>
				<b>UNIT INOVASI DAN PENYELIDIKAN MARA</b><br/>
				<b>TINGKAT 9, IBU PEJABAT MARA</b><br/>
				<b>JALAN RAJA LAUT, 50609,</b><br/>
				<b>Kuala Lumpur</b><br/>
				<b>Faks: 03 2691 0486</b><br/>
			</p>

			<p>Atau, berhubung terus dengan senarai pegawai dibawah:
				<ol type="1">
					<li>Encik Rosidi bin Ishak<br/>No Tel 03 2613 4360<br/>E-mel: <a href="mailto:rosidi@mara.gov.my">rosidi@mara.gov.my</a></li>
					<li>Puan Mazinah Joary binti Mohammed Tajudin<br/>No Tel 03 2613 4375<br/>E-mel: <a href="mailto:mazinah@mara.gov.my">mazinah@mara.gov.my</a></li>
					<li>Puan Khairiah binti Abdullah<br/>No Tel 03 2613 4457<br/>E-mel: <a href="mailto:khairiah.abdullah@mara.gov.my">khairiah.abdullah@mara.gov.my</a></li>
					<li>Puan Hanirus binti Osman<br/>No Tel 03 2613 4463<br/>E-mel: <a href="mailto:hanirus@mara.gov.my">hanirus@mara.gov.my</a></li>
					<li>Puan Sundarambal a/p Gengatharan<br/>No Tel 03 2613 5052<br/>E-mel: <a href="mailto:sundarambal@mara.gov.my">sundarambal@mara.gov.my</a></li>
				</ol>
			</p>
		</div>
	</div>
</div>

