<script type="text/javascript">
	var alert = "<?= $alert ?>",
		userId = "<?= $this->userdata['id'] ?>";
</script>
<div class="col-md-8 col-md-offset-2">
	<?php $this->load->view('partial/logo_after_login',array('title' => lang('edit_profile'))); ?>

	<div class="col-md-12 alert alert-info hide alert-dismissable">
	    <i class="fa fa-info"></i>
	    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	    <?= $alert ?>
	</div>

	<form id="form-profile" class="form-horizontal" action="<?= base_url().'profile/save' ?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="id" value="<?= (isset($innovation) ? $innovation['innovation_id'] : 0) ?>">
		<input type="hidden" name="username" value="<?= (isset($innovation) ? $innovation['username'] : 0) ?>">

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('email') ?></label>
			<div class="col-md-9">
				<input type="text" name="email" class="form-control" placeholder="<?= lang('email') ?>" value="<?= (isset($innovator) ? $innovator['email'] : '') ?>">
			</div>
		</div>

		<div class="form-group">
			<div class="col-md-9 col-md-offset-3">
				<button type="button" class="btn btn-default btn-change-pass">Change Password</button>
			</div>
		</div>
		<div class="collapse-group collapse">
			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('password') ?></label>
				<div class="col-md-9">
					<input type="password" name="password" placeholder="<?= lang('password') ?>" class="form-control">
				</div>
			</div>

			<div class="form-group">
				<label class="col-md-3 control-label"><?= lang('retype_password') ?></label>
				<div class="col-md-9">
					<input type="password" name="retype_password" placeholder="<?= lang('retype_password') ?>" class="form-control">
				</div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('name') ?></label>
			<div class="col-md-9">
				<input type="text" name="name" class="form-control" placeholder="<?= lang('email') ?>" value="<?= (isset($innovator) ? $innovator['name'] : '') ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('no_kp') ?></label>
			<div class="col-md-9">
				<input type="text" name="kp_no" class="form-control" placeholder="<?= lang('no_kp') ?>" value="<?= (isset($innovator) ? $innovator['kp_no'] : '') ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('telp_no') ?></label>
			<div class="col-md-9">
				<input type="text" name="telp_no" class="form-control" placeholder="<?= lang('telp_no') ?>" value="<?= (isset($innovator) ? $innovator['telp_no'] : '') ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('fax_no') ?></label>
			<div class="col-md-9">
				<input type="text" name="fax_no" class="form-control" placeholder="<?= lang('fax_no') ?>" value="<?= (isset($innovator) ? $innovator['fax_no'] : '') ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('mara_center') ?></label>
			<div class="col-md-9">
				<input type="text"  class="form-control" id="mc-keyword" name="mara_center_keyword" data-provide="typeahead" autocomplete="off" placeholder="<?= lang('mara_center') ?>" value="<?= (isset($innovator) ? $innovator['mara_center'] : '') ?>">
			</div>
			<input type="hidden" name="mara_center" id="mc-id" value="<?= (isset($innovator) ? $innovator['mara_center_id'] : '') ?>"/>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('mara_address') ?></label>
			<div class="col-md-9">
				<input type="text" name="mara_address" class="form-control" placeholder="<?= lang('mara_address') ?>" value="<?= (isset($innovator) ? $innovator['mara_address'] : '') ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('mara_telp_no') ?></label>
			<div class="col-md-9">
				<input type="text" name="mara_telp_no" class="form-control" placeholder="<?= lang('mara_telp_no') ?>" value="<?= (isset($innovator) ? $innovator['mara_telp_no'] : '') ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('mara_fax_no') ?></label>
			<div class="col-md-9">
				<input type="text" name="mara_fax_no" class="form-control" placeholder="<?= lang('mara_fax_no') ?>" value="<?= (isset($innovator) ? $innovator['mara_fax_no'] : '') ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('staff_id') ?></label>
			<div class="col-md-9">
				<input type="text" name="staff_id" class="form-control" placeholder="<?= lang('staff_id') ?>" value="<?= (isset($innovator) ? $innovator['staff_id'] : '') ?>">
			</div>
		</div>

		<div class="form-group">
			<label class="col-md-3 control-label"><?= lang('photo') ?></label>
			<div class="col-md-9">
				<?php if(file_exists(realpath(APPPATH . '../'.PATH_TO_INNOVATOR_PHOTO) . DIRECTORY_SEPARATOR . $innovator['photo']) && $innovator['photo'] != "") { ?>
						<a href="<?= base_url().PATH_TO_INNOVATOR_PHOTO.$innovator['photo'] ?>" class="fancyboxs"><img src="<?= base_url().PATH_TO_INNOVATOR_PHOTO_THUMB.$innovator['photo'] ?>" width="30%"/></a>
						<input type="hidden" name="h_innovator_photo" value="<?= $innovator['photo'] ?>">
				<?php } ?>
				<input type="file" class="form-control" name="innovator_photo">
			</div>
		</div>

		<div class="col-md-12 text-center">
			<input type="submit" name="btn_save" id="btn-save" data-id="btn_save" class="btn btn-success flat" value="<?= lang('save') ?>">
		</div>
	</form>
</div>