<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Layout
{
	function render()
	{
		global $OUT;
		$CI = &get_instance();
		$output = $CI->output->get_output();

		if (!isset($CI->layout)) {
			$CI->layout = false;
		}

		if ($CI->layout != false) {
			if (!preg_match('/(.+).php$/', $CI->layout)) {
				$CI->layout .= '.php';
			}

			$requested = BASEPATH . '../application/layouts/' . $CI->layout;
			$default = BASEPATH . '../application/layouts/default.php';

			if (file_exists($requested)) {
				$layout = $CI->load->file($requested, true);
			} else {
				$layout = $CI->load->file($default, true);
			}

			$view = str_replace("{{content}}", $output, $layout);
			$view = str_replace("{{additional_content}}", @$CI->additional_content, $view);
			$view = str_replace("{{title}}", @$CI->title, $view);
			$view = str_replace("{{header}}", @$CI->header, $view);
			$view = str_replace("{{left}}", @$CI->left, $view);
			$view = str_replace("{{error_message}}", @$CI->error_message, $view);
			$view = str_replace("{{success_message}}", @$CI->success_message, $view);
			$view = str_replace("{{left_sidebar}}", @$CI->left_sidebar, $view);
			$view = str_replace("{{right_sidebar}}", @$CI->right_sidebar, $view);
			$view = str_replace("{{footer}}", @$CI->footer, $view);
			$view = str_replace("{{content_style}}", @$CI->content_style, $view);
			$view = str_replace("{{signin_status}}", @$CI->signin_status, $view);
			$view = str_replace("{{page_color}}", @$CI->page_color, $view);

			$scripts = "";
			$styles = "";
			$metas = "";

			if (count(array(@$CI->metas)) > 0) {
				foreach (array(@$CI->metas) as $array_meta) {
					if ($array_meta) {
						foreach ($array_meta as $meta) {
							$metas .= $meta;
						}
					}
				}
				//$metas = implode("\n", $CI->meta);
			}

			if (count(array(@$CI->scripts)) > 0) {
				foreach (array(@$CI->scripts) as $array_script) {
					if ($array_script) {
						foreach ($array_script as $script) {
							$base_path = 'assets/js/';
							$ext = '.js';
							if ($script == 'toolbar/get_javascript') {
								$base_path = '';
								$ext = '';
							}

							$scripts .= "<script type='text/javascript' src='" . base_url() . $base_path . $script . $ext . "'></script>";
						}
					}
				}
				$scripts .= "<script type='text/javascript'>var base_url = '" . base_url() . "';</script>";
			}

			if (count(array(@$CI->styles)) > 0) {
				foreach (array(@$CI->styles) as $array_style) {
					if ($array_style) {
						foreach ($array_style as $style) {
							$styles .= "<link rel='stylesheet' type='text/css' href='" . base_url() . "assets/css/" . $style . ".css' />";
						}
					}
				}
			}

			if (count(array(@$CI->parts)) > 0) {
				foreach (array(@$CI->parts) as $name => $array_part) {
					if ($array_part) {
						foreach ($array_part as $key_part => $part) {
							$view = str_replace("{{" . $key_part . "}}", $part, $view);
						}
					}
				}
			}

			$view = str_replace("{{metas}}", $metas, $view);
			$view = str_replace("{{scripts}}", $scripts, $view);
			$view = str_replace("{{styles}}", $styles, $view);
			$view = preg_replace("/{{.*?}}/ims", "", $view);
		} else {
			$view = $output;
		}

		$OUT->_display($view);
	}
}
