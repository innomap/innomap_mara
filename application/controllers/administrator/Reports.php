<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Reports extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Reports";
		$this->menu = "report";

        $this->lang->load(PATH_TO_ADMIN.'report',$this->language);

        $this->load->model('mara_innovator');
        $this->load->model('mara_approval_status');
        $this->load->model('innovation');

		$this->scripts[] = 'plugins/highcharts/highcharts';
        $this->scripts[] = 'plugins/bootstrap-datepicker/bootstrap-datepicker.min';
		$this->scripts[] = 'administrator/report';

        $this->styles[] = 'bootstrap-datepicker/bootstrap-datepicker.min';
    }

    public function index(){}

    function registration_number_by_date(){
        $user = $this->mara_innovator->get_join(NULL, 1,"user_id DESC")->row_array();
        $data['from_date'] = date('Y-m-d',strtotime('-29 days',strtotime($user['created_date'])));
        $data['to_date'] = date('Y-m-d',strtotime($user['created_date']));
        $data['modal_detail'] = $this->load->view(PATH_TO_ADMIN.'report/modal_detail', array('action_url' => 'export_registration_number_by_date'), TRUE);
        $this->load->view(PATH_TO_ADMIN.'report/registration_number_by_date',$data);
    }

    function registration_number_by_date_handler(){
        $this->layout = FALSE;
        
        $xAxis = array();
        $data = array();
        $date_from = $this->input->post('date_from');
        $date_to = $this->input->post('date_to');

        $days = $this->count_interval($date_from, $date_to);
        
        for ($i=0; $i <= $days; $i++) { 
            $raw_date = strtotime('+'.$i.' days',strtotime($date_from));
            $xAxis[] = date('d-m-Y',$raw_date);
            $date = date('Y-m-d', $raw_date);
            $data[] = $this->mara_innovator->get_join("DATE( created_date ) =  '".$date."'")->num_rows();
        }

        $result = array('xAxis' => $xAxis, 'series' => array(array('name' => 'Registration Number', 'data' => $data)));
        echo json_encode($result);
    }

    function application_number_by_date(){
        $application = $this->mara_approval_status->find("status = ".INNO_STATUS_DRAFT." AND mara_round_id = 1", 1, 0,"id DESC");
        $data['from_date'] = date('Y-m-d',strtotime('-29 days',strtotime($application[0]['created_at'])));
        $data['to_date'] = date('Y-m-d',strtotime($application[0]['created_at']));
        $data['modal_detail'] = $this->load->view(PATH_TO_ADMIN.'report/modal_detail', array('action_url' => 'export_application_number_by_date'), TRUE);
        $this->load->view(PATH_TO_ADMIN.'report/application_number_by_date',$data);
    }

    function application_number_by_date_handler(){
        $this->layout = FALSE;
        
        $xAxis = array();
        $data = array();
        $date_from = $this->input->post('date_from');
        $date_to = $this->input->post('date_to');

        $days = $this->count_interval($date_from, $date_to);
        
        for ($i=0; $i <= $days; $i++) { 
            $raw_date = strtotime('+'.$i.' days',strtotime($date_from));
            $xAxis[] = date('d-m-Y',$raw_date);
            $date = date('Y-m-d', $raw_date);
            $data[] = $this->mara_approval_status->get_number("status = ".INNO_STATUS_DRAFT." AND mara_round_id = 1 AND DATE( created_at ) =  '".$date."'");
        }

        $result = array('xAxis' => $xAxis, 'series' => array(array('name' => 'Draft Number', 'data' => $data)));
        echo json_encode($result);
    }

    function submission_number_by_date(){
        $application = $this->mara_approval_status->find("status = ".INNO_STATUS_SENT_APPROVAL." AND mara_round_id = 1", 1, 0,"id DESC");
        $data['from_date'] = date('Y-m-d',strtotime('-29 days',strtotime($application[0]['created_at'])));
        $data['to_date'] = date('Y-m-d',strtotime($application[0]['created_at']));
        $data['modal_detail'] = $this->load->view(PATH_TO_ADMIN.'report/modal_detail', array('action_url' => 'export_submission_number_by_date'), TRUE);
        $this->load->view(PATH_TO_ADMIN.'report/submission_number_by_date',$data);
    }

    function submission_number_by_date_handler(){
        $this->layout = FALSE;
        
        $xAxis = array();
        $data = array();
        $date_from = $this->input->post('date_from');
        $date_to = $this->input->post('date_to');

        $days = $this->count_interval($date_from, $date_to);
        
        for ($i=0; $i <= $days; $i++) { 
            $raw_date = strtotime('+'.$i.' days',strtotime($date_from));
            $xAxis[] = date('d-m-Y',$raw_date);
            $date = date('Y-m-d', $raw_date);
            $data[] = $this->mara_approval_status->get_number("status = ".INNO_STATUS_SENT_APPROVAL." AND mara_round_id = 1 AND DATE( created_at ) =  '".$date."'");
        }

        $result = array('xAxis' => $xAxis, 'series' => array(array('name' => 'Submission Number', 'data' => $data)));
        echo json_encode($result);
    }

    private function count_interval($date_from, $date_to){
        $datetime1 = date_create($date_from);
        $datetime2 = date_create($date_to);
        $interval = date_diff($datetime1, $datetime2);
        return intval($interval->format('%a'));
    }

    function get_detail(){
        $this->layout = FALSE;

        $date = $this->input->post('date');
        $type = $this->input->post('type');

        if($type == 1){ // registration by number
            $this->lang->load('account',$this->language);

            $columns = array("No",lang('email'),lang('name'),lang('mara_center'));
            $fields = array("no","email","name","mara_center");
            $data = $this->mara_innovator->get_join("DATE( created_date ) =  '".date('Y-m-d',strtotime($date))."'")->result_array();
        }else if($type == 2){ //application by number
            $columns = array("No","Innovator","Innovation","Tindakan");
            $fields = array("no","innovator_name","name_in_melayu","detail_innovation");
            $data = $this->mara_approval_status->get_innovation_list("mara_approval_status.status = ".INNO_STATUS_DRAFT." AND mara_round_id = 1 AND DATE( created_at ) =  '".date('Y-m-d',strtotime($date))."'")->result_array();
        }else if($type == 3){ //submission by number
            $columns = array("No","Innovator","Innovation","Tindakan");
            $fields = array("no","innovator_name","name_in_melayu","detail_innovation");
            $data = $this->mara_approval_status->get_innovation_list("mara_approval_status.status = ".INNO_STATUS_SENT_APPROVAL." AND mara_round_id = 1 AND DATE( created_at ) =  '".date('Y-m-d',strtotime($date))."'")->result_array();
        }else if($type == 5){
            $category = $this->input->post('category');
            $columns = array("No","Innovator","Innovation","Tindakan");
            $fields = array("no","innovator_name","name_in_melayu","detail_innovation");
            $data = $this->innovation->get_join("category = ".$category)->result_array();
        }else if($type == 6){
            $category = $this->input->post('category');
            $subcategory = $this->input->post('subcategory');
            $columns = array("No","Innovator","Innovation","Tindakan");
            $fields = array("no","innovator_name","name_in_melayu","detail_innovation");
            $data = $this->innovation->get_join("category = ".$category." AND area = ".$subcategory)->result_array();
        }else if($type == 7){ //submission by number
            $columns = array("No","Innovator","Innovation","Tindakan");
            $fields = array("no","innovator_name","name_in_melayu","detail_business_plan");
            $data = $this->mara_approval_status->get_business_plan_list("mara_approval_status.status = ".INNO_STATUS_SENT_APPROVAL." AND mara_round_id = 2 AND DATE( mara_approval_status.created_at ) =  '".date('Y-m-d',strtotime($date))."'")->result_array();
        }

        $result = array('columns' => $columns,'data' => $data, 'fields' => $fields);
        echo json_encode($result);
    }

    function export_registration_number_by_date(){
        $this->layout = FALSE;
        $this->lang->load('account',$this->language);

        $date = $this->input->post('date');
        $from = $this->input->post('date_from');
        $to = $this->input->post('date_to');

        if($date != ""){
            $where = "DATE( created_date ) = '".date('Y-m-d',strtotime($date))."'";
        }else{
            $where = "DATE( created_date ) BETWEEN '".date('Y-m-d',strtotime($from))."' AND '".date('Y-m-d',strtotime($to))."'";
        }

        $data = $this->mara_innovator->get_join($where)->result_array();
        $contents = $this->generate_innovator_data($data);

        $file_name = "Registration_Number_by_Date";
        $title = "Registration Number by Date";
        $columns = array("No",lang('email'),lang('name'),lang('mara_center'),lang('mara_address'),lang('mara_telp_no'), lang('mara_fax_no'), lang('staff_id'), lang('telp_no'), lang('fax_no'), lang('no_kp'));
        if(count($contents) > 0){
            $fields = array_keys($contents[0]);

            $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
        }else{
            redirect(base_url().PATH_TO_ADMIN.'reports/registration_number_by_date');
        }
    }

    function export_application_number_by_date(){
        $this->layout = FALSE;

        $date = $this->input->post('date');
        $from = $this->input->post('date_from');
        $to = $this->input->post('date_to');

        if($date != ""){
            $where = "mara_approval_status.status = ".INNO_STATUS_DRAFT." AND mara_round_id = 1 AND DATE( created_at ) =  '".date('Y-m-d',strtotime($date))."'";
        }else{
            $where = "mara_approval_status.status = ".INNO_STATUS_DRAFT." AND mara_round_id = 1 AND DATE( created_at ) BETWEEN '".date('Y-m-d',strtotime($from))."' AND '".date('Y-m-d',strtotime($to))."'";
        }

        $data = $this->mara_approval_status->get_innovation_list($where)->result_array();
        $contents = $this->generate_innovation_data($data);

        $file_name = "Application_Number_by_Date";
        $title = "Application Number by Date";
        $columns = array("No",lang('innovator'),lang('mara_center'),lang('mara_address'),lang('mara_telp_no'),lang('mara_fax_no'), lang('application_category'), lang('category'),lang('no_kp'),lang('staff_id'),lang('email'),lang('telp_no'),lang('fax_no'),lang('instructor_name'),lang('instructor_staff_id'),lang('instructor_kp_no'),lang('innovation_product'),lang('inspiration'),lang('description'),lang('material'),lang('how_to_use'),lang('special_achievement'),lang('created_date'),lang('discovered_date'),lang('manufacturing_cost'),lang('selling_price'),lang('target'),lang('myipo_protection'),lang('keyword'),lang('team_member')."(".lang('name')."/".lang('no_kp')."/".lang('telp_no').")",lang('heir')."(".lang('name')."/".lang('no_kp')."/".lang('telp_no').")");
        if(count($contents) > 0){
            $fields = array_keys($contents[0]);

            $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
        }else{
            redirect(base_url().PATH_TO_ADMIN.'reports/application_number_by_date');
        }
    }

    function export_submission_number_by_date(){
        $this->layout = FALSE;

        $date = $this->input->post('date');
        $from = $this->input->post('date_from');
        $to = $this->input->post('date_to');

        if($date != ""){
            $where = "mara_approval_status.status = ".INNO_STATUS_SENT_APPROVAL." AND mara_round_id = 1 AND DATE( created_at ) =  '".date('Y-m-d',strtotime($date))."'";
        }else{
            $where = "mara_approval_status.status = ".INNO_STATUS_SENT_APPROVAL." AND mara_round_id = 1 AND DATE( created_at ) BETWEEN '".date('Y-m-d',strtotime($from))."' AND '".date('Y-m-d',strtotime($to))."'";
        }

        $data = $this->mara_approval_status->get_innovation_list($where)->result_array();
        $contents = $this->generate_innovation_data($data);

        $file_name = "Submission_Number_by_Date";
        $title = "Submission Number by Date";
        $columns = array("No",lang('innovator'),lang('mara_center'),lang('mara_address'),lang('mara_telp_no'),lang('mara_fax_no'), lang('application_category'), lang('category'),lang('no_kp'),lang('staff_id'),lang('email'),lang('telp_no'),lang('fax_no'),lang('instructor_name'),lang('instructor_staff_id'),lang('instructor_kp_no'),lang('innovation_product'),lang('inspiration'),lang('description'),lang('material'),lang('how_to_use'),lang('special_achievement'),lang('created_date'),lang('discovered_date'),lang('manufacturing_cost'),lang('selling_price'),lang('target'),lang('myipo_protection'),lang('keyword'),lang('team_member')."(".lang('name')."/".lang('no_kp')."/".lang('telp_no').")",lang('heir')."(".lang('name')."/".lang('no_kp')."/".lang('telp_no').")");
        if(count($contents) > 0){
            $fields = array_keys($contents[0]);

            $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
        }else{
            redirect(base_url().PATH_TO_ADMIN.'reports/submission_number_by_date');
        }
    }

    private function generate_innovator_data($data){
        $contents = array();
        $no = 1; 
        foreach ($data as $key => $value) {
            $contents[$key]['no'] = $no;
            $contents[$key]['email'] = $value['email'];
            $contents[$key]['name'] = $value['name'];
            $contents[$key]['mara_center'] = $value['mara_center'];
            $contents[$key]['mara_address'] = $value['mara_address'];
            $contents[$key]['mara_telp_no'] = $value['mara_telp_no'];
            $contents[$key]['mara_fax_no'] = $value['mara_fax_no'];
            $contents[$key]['staff_id'] = $value['staff_id'];
            $contents[$key]['telp_no'] = $value['telp_no'];
            $contents[$key]['fax_no'] = $value['fax_no'];
            $contents[$key]['kp_no'] = $value['kp_no'];
        $no++;}

        return $contents;
    }

    private function generate_innovation_data($data){
        $this->load->model('mara_innovation_team');
        $this->load->model('mara_innovation_info');
        $this->lang->load('innovation',$this->language);
        $categories = unserialize(APPLICATION_CATEGORY);
        $categories_services_area = unserialize(APPLICATION_CATEGORY_SERVICES_AREA);
        $categories_produk_area = unserialize(APPLICATION_CATEGORY_PRODUK_AREA);
        $categories_produk_level = unserialize(APPLICATION_CATEGORY_PRODUK_LEVEL);
        $targets = unserialize(INNOVATION_TARGET);

        $contents = array();
        $no = 1; 
        foreach ($data as $key => $value) {
            $contents[$key]['no'] = $no;
            $contents[$key]['innovator'] = $value['innovator_name'];
            $contents[$key]['mara_center'] = $value['mara_center'];
            $contents[$key]['mara_address'] = $value['mara_address'];
            $contents[$key]['mara_telp_no'] = $value['mara_telp_no'];
            $contents[$key]['mara_fax_no'] = $value['mara_fax_no'];
            $contents[$key]['application_category'] = $categories_produk_level[$value['level']];
            $contents[$key]['category'] = $categories[$value['category']]." - ".($value['category'] == 0 ? $categories_services_area[$value['area']] : $categories_produk_area[$value['area']]);
            $contents[$key]['kp_no'] = $value['kp_no'];
            $contents[$key]['staff_id'] = $value['staff_id'];
            $contents[$key]['email'] = $value['email'];
            $contents[$key]['telp_no'] = $value['telp_no'];
            $contents[$key]['fax_no'] = $value['fax_no'];
            $contents[$key]['instructor_name'] = $value['instructor_name'];
            $contents[$key]['instructor_staff_id'] = $value['instructor_staff_id'];
            $contents[$key]['instructor_kp_no'] = $value['instructor_kp_no'];
            $contents[$key]['innovation'] = $value['name_in_melayu'];
            $contents[$key]['inspiration'] = $value['inspiration'];
            $contents[$key]['description'] = $value['description'];
            $contents[$key]['material'] = $value['materials'];
            $contents[$key]['how_to_use'] = $value['how_to_use'];
            $contents[$key]['special_achievement'] = $value['special_achievement'];
            $contents[$key]['created_date'] = $value['created_date'];
            $contents[$key]['discovered_date'] = $value['discovered_date'];
            $contents[$key]['manufacturing_cost'] = "RM ".$value['manufacturing_costs'];
            $contents[$key]['selling_price'] = "RM ".$value['selling_price'];
            $target_arr = ($value['target'] != NULL ? json_decode($value['target']) : array());
            $target = "";
            foreach ($target_arr as $val) {
                $target .= "- ".$targets[$val]."\n";
            }
            $contents[$key]['target'] = $target;
            $contents[$key]['myipo_protection'] = ($value['myipo_protection'] == 1 ? lang('yes') : lang('no'));
            $keyword_arr = ($value['keyword'] != "" ? json_decode($value['keyword']) : array());
            $keyword = "";
            foreach ($keyword_arr as $val) {
                $keyword .= "- ".$val."\n";
            }
            $contents[$key]['keyword'] = $keyword;
            $team_member = $this->mara_innovation_team->find("innovation_id = ".$value['innovation_id']);
            $team_member_str = "";
            foreach ($team_member as $val) {
                $team_member_str = "- ".$val['name']."(".$val['kp_no']."/".$val['telp_no'].")";
            }

            $contents[$key]['team_member'] = $team_member_str;

            $info = $this->mara_innovation_info->find_one("innovation_id = ".$value['innovation_id']);
            $contents[$key]['heir'] = $info['heir_name']."/".$info['heir_kp_no']."/".$info['heir_telp_no'];
        $no++;}

        return $contents;
    }

    private function export_to_excel($file_name,$title,$columns,$fields,$contents){
        $this->load->library('PHPExcel');

        if(!$fields) {
            echo 'No result. <a href="'.base_url().PATH_TO_ADMIN.'">Back</a>'; die();
        }
        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$file_name.'.xls"');
        header('Cache-Control: max-age=0');

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set properties
        $objPHPExcel->getProperties()->setCreator("Maratex2016");
        $objPHPExcel->getProperties()->setLastModifiedBy("Maratex2016");
        $objPHPExcel->getProperties()->setTitle($title);
        $objPHPExcel->getProperties()->setSubject("Maratex2016");
        
        #Column Width
        $objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(25);
        
        #Column Title Style
        $objPHPExcel->getActiveSheet()->getStyle("A1:AE1")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A1:AE1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        #Wrap Text
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setWrapText(true); 
        
        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);
        $col = 0;
        $row = 1;

        foreach($columns as $column) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $column);
            $col++;
        }
        $col = 0;
        $row++;
        foreach($contents as $content) {
            foreach($fields as $field) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $content[$field]);
                $col++;
            }
            $col = 0;
            $row++;
        }
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Sheet 1');

        // Save it as an excel 2003 file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        //$objWriter->save("nameoffile.xls");
        $objWriter->save('php://output');
        exit;
    }

    function submission_by_mara_center(){
        $this->load->model('mara_center');

        $data['mara_center'] = $this->mara_center->find_all();
        $data['selected_mara_center'] = $this->input->post('mara_center_id');
        if(isset($_POST['btn_report'])){
            $data['users'] = $this->submission_by_mara_center_handler($data['selected_mara_center']);
        }else if(isset($_POST['btn_export'])){
            $this->lang->load('account',$this->language);
            $users = $this->submission_by_mara_center_handler($data['selected_mara_center']);
            $data_content = array();
            foreach ($users as $key => $value) {
                foreach ($value['submissions'] as $submission) {
                    $data_content[] = $submission;
                }
            }
            $contents = $this->generate_innovation_data($data_content);

            $file_name = "Submissions_by_Mara_Center";
            $title = "Submissions by Mara Center";
            $columns = array("No",lang('innovator'),lang('mara_center'),lang('mara_address'),lang('mara_telp_no'),lang('mara_fax_no'), lang('application_category'), lang('category'),lang('no_kp'),lang('staff_id'),lang('email'),lang('telp_no'),lang('fax_no'),lang('instructor_name'),lang('instructor_staff_id'),lang('instructor_kp_no'),lang('innovation_product'),lang('inspiration'),lang('description'),lang('material'),lang('how_to_use'),lang('special_achievement'),lang('created_date'),lang('discovered_date'),lang('manufacturing_cost'),lang('selling_price'),lang('target'),lang('myipo_protection'),lang('keyword'),lang('team_member')."(".lang('name')."/".lang('no_kp')."/".lang('telp_no').")",lang('heir')."(".lang('name')."/".lang('no_kp')."/".lang('telp_no').")");
            $fields = array_keys($contents[0]);

            $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
        }else{
            $data['users'] = $this->submission_by_mara_center_handler();
        }
        $this->load->view(PATH_TO_ADMIN.'report/user_by_mara_center',$data);
    }

    private function submission_by_mara_center_handler($mara_center_id= 0, $export_btn = FALSE){
        $this->load->model('mara_center');

        if($export_btn){
            
        }else{
            if($mara_center_id != 0){
                $where = "id = ".$mara_center_id;
            }else{
                $where = "";
            }
            $mara_center = $this->mara_center->find($where);
            foreach ($mara_center as $key => $value) {
                $where_submission = "mara_approval_status.status = ".INNO_STATUS_SENT_APPROVAL." AND mara_round_id = 1 AND mara_innovator.mara_center_id = ".$value['id'];
                $mara_center[$key]['submissions'] = $this->mara_approval_status->get_innovation_list($where_submission)->result_array();
            }
            return $mara_center;
        }
    }

    function application_number_by_category(){
        $data['modal_detail'] = $this->load->view(PATH_TO_ADMIN.'report/modal_detail', array('action_url' => 'export_application_number_by_category'), TRUE);
        $this->load->view(PATH_TO_ADMIN.'report/application_number_by_category',$data);
    }

    function application_number_by_category_handler(){
        $this->layout = FALSE;
        $this->load->model('innovation');

        $data = array();
        $categories = unserialize(APPLICATION_CATEGORY);
        foreach ($categories as $key => $value) {
            $data[$key]['name'] = $value;
            $data[$key]['y'] = $this->innovation->get_join("category = ".$key)->num_rows();
            $data[$key]['val_id'] = $key;
        }

        $result = array("data" => $data);
        echo json_encode($result);
    }

    function export_application_number_by_category(){
        $this->layout = FALSE;

        $category = $this->input->post('category');

        if($category != ""){
            $where = "category = ".$category;
        }else{
            $where = "";
        }

        $data = $this->innovation->get_join($where)->result_array();
        $contents = $this->generate_innovation_data($data);

        $file_name = "Application_Number_by_Category";
        $title = "Application Number by Category";
        $columns = array("No",lang('innovator'),lang('mara_center'),lang('mara_address'),lang('mara_telp_no'),lang('mara_fax_no'), lang('application_category'), lang('category'),lang('no_kp'),lang('staff_id'),lang('email'),lang('telp_no'),lang('fax_no'),lang('instructor_name'),lang('instructor_staff_id'),lang('instructor_kp_no'),lang('innovation_product'),lang('inspiration'),lang('description'),lang('material'),lang('how_to_use'),lang('special_achievement'),lang('created_date'),lang('discovered_date'),lang('manufacturing_cost'),lang('selling_price'),lang('target'),lang('myipo_protection'),lang('keyword'),lang('team_member')."(".lang('name')."/".lang('no_kp')."/".lang('telp_no').")",lang('heir')."(".lang('name')."/".lang('no_kp')."/".lang('telp_no').")");
        if(count($contents) > 0){
            $fields = array_keys($contents[0]);

            $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
        }else{
            redirect(base_url().PATH_TO_ADMIN.'reports/application_number_by_category');
        }
    }

    function application_number_by_subcategory(){
        $data['categories'] = unserialize(APPLICATION_CATEGORY);
        $data['service_subcategories'] = unserialize(APPLICATION_CATEGORY_SERVICES_AREA);
        $data['product_subcategories'] = unserialize(APPLICATION_CATEGORY_PRODUK_AREA);

        $data['modal_detail'] = $this->load->view(PATH_TO_ADMIN.'report/modal_detail', array('action_url' => 'export_application_number_by_subcategory'), TRUE);
        $this->load->view(PATH_TO_ADMIN.'report/application_number_by_subcategory',$data);
    }

    function application_number_by_subcategory_handler(){
        $this->layout = FALSE;
        $this->load->model('innovation');

        $values = array();
        $list_subcategory = array();
        $categories = unserialize(APPLICATION_CATEGORY);
        $service_subcategories = unserialize(APPLICATION_CATEGORY_SERVICES_AREA);
        $product_subcategories = unserialize(APPLICATION_CATEGORY_PRODUK_AREA);

        $selected_data = $this->input->post('data');
        if($selected_data != ""){
            foreach ($selected_data as $key => $value) {
                if($value['category'] == 0){
                    $subcategories = $service_subcategories;
                }else{
                    $subcategories = $product_subcategories;
                }
                $list_subcategory[] = $subcategories[$value['id']];
                $values[] = array('y' => $this->innovation->get_join("category = ".$value['category']." AND area = ".$value['id'])->num_rows(), 'category_id' => $value['category'], 'subcategory_id' => $value['id']);
            }
        }else{
            foreach ($categories as $key => $category) {
                if($key == 0){
                    $subcategories = $service_subcategories;
                }else{
                    $subcategories = $product_subcategories;
                }

                foreach ($subcategories as $i => $subcategory) {
                    $list_subcategory[] = $subcategory;
                    $values[] = array('y' => $this->innovation->get_join("category = ".$key." AND area = ".$i)->num_rows(), 'category_id' => $key, 'subcategory_id' => $i);
                }
            }    
        }      

        $result = array("categories" => $list_subcategory,"data" => array(array('name' => 'Application Number', 'data' => $values)));
        echo json_encode($result);
    }

    function export_application_number_by_subcategory(){
        $this->layout = FALSE;

        $subcategories = $this->input->post('subcategories');
        $category = $this->input->post('category');
        $subcategory = $this->input->post('subcategory');
        $where = "";

        if($subcategories != ""){
            foreach ($subcategories as $key => $value) {
                $val = explode(",",$value);
                $where .= "(category = ".$val[0]." AND area = ".$val[1].")";
                if($value != end($subcategories)){
                    $where .= " OR ";
                }
            }
        }else if($category != ""){
            $where .= "category = ".$category." AND area = ".$subcategory;
        }

        $data = $this->innovation->get_join($where)->result_array();
        $contents = $this->generate_innovation_data($data);

        $file_name = "Application_Number_by_Subcategory";
        $title = "Application Number by Subcategory";
        $columns = array("No",lang('innovator'),lang('mara_center'),lang('mara_address'),lang('mara_telp_no'),lang('mara_fax_no'), lang('application_category'), lang('category'),lang('no_kp'),lang('staff_id'),lang('email'),lang('telp_no'),lang('fax_no'),lang('instructor_name'),lang('instructor_staff_id'),lang('instructor_kp_no'),lang('innovation_product'),lang('inspiration'),lang('description'),lang('material'),lang('how_to_use'),lang('special_achievement'),lang('created_date'),lang('discovered_date'),lang('manufacturing_cost'),lang('selling_price'),lang('target'),lang('myipo_protection'),lang('keyword'),lang('team_member')."(".lang('name')."/".lang('no_kp')."/".lang('telp_no').")",lang('heir')."(".lang('name')."/".lang('no_kp')."/".lang('telp_no').")");
        if(count($contents) > 0){
            $fields = array_keys($contents[0]);

            $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
        }else{
            redirect(base_url().PATH_TO_ADMIN.'reports/application_number_by_subcategory');
        }
    }

    /*Temporary function*/
    function export_submission_by_subcategory($category, $subcategory){
        $this->layout = FALSE;

        $where = "mara_approval_status.status = ".INNO_STATUS_SENT_APPROVAL." AND mara_round_id = 1 AND category = ".$category." AND area = ".$subcategory;

        $data = $this->mara_approval_status->get_innovation_list($where)->result_array();
        $contents = $this->generate_innovation_data($data);

        $file_name = "Submission_Number_by_Date";
        $title = "Submission Number by Date";
        $columns = array("No",lang('innovator'),lang('mara_center'),lang('mara_address'),lang('mara_telp_no'),lang('mara_fax_no'), lang('application_category'), lang('category'),lang('no_kp'),lang('staff_id'),lang('email'),lang('telp_no'),lang('fax_no'),lang('instructor_name'),lang('instructor_staff_id'),lang('instructor_kp_no'),lang('innovation_product'),lang('inspiration'),lang('description'),lang('material'),lang('how_to_use'),lang('special_achievement'),lang('created_date'),lang('discovered_date'),lang('manufacturing_cost'),lang('selling_price'),lang('target'),lang('myipo_protection'),lang('keyword'),lang('team_member')."(".lang('name')."/".lang('no_kp')."/".lang('telp_no').")",lang('heir')."(".lang('name')."/".lang('no_kp')."/".lang('telp_no').")");
        if(count($contents) > 0){
            $fields = array_keys($contents[0]);

            $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
        }else{
            redirect(base_url().PATH_TO_ADMIN.'reports/submission_number_by_date');
        }
    }

    function export_submission_by_award_category($level){
        $this->layout = FALSE;

        $where = "mara_approval_status.status = ".INNO_STATUS_SENT_APPROVAL." AND mara_round_id = 1 AND level = ".$level;

        $data = $this->mara_approval_status->get_innovation_list($where)->result_array();
        $contents = $this->generate_innovation_data($data);

        $file_name = "Submission_Number_by_Date";
        $title = "Submission Number by Date";
        $columns = array("No",lang('innovator'),lang('mara_center'),lang('mara_address'),lang('mara_telp_no'),lang('mara_fax_no'), lang('application_category'), lang('category'),lang('no_kp'),lang('staff_id'),lang('email'),lang('telp_no'),lang('fax_no'),lang('instructor_name'),lang('instructor_staff_id'),lang('instructor_kp_no'),lang('innovation_product'),lang('inspiration'),lang('description'),lang('material'),lang('how_to_use'),lang('special_achievement'),lang('created_date'),lang('discovered_date'),lang('manufacturing_cost'),lang('selling_price'),lang('target'),lang('myipo_protection'),lang('keyword'),lang('team_member')."(".lang('name')."/".lang('no_kp')."/".lang('telp_no').")",lang('heir')."(".lang('name')."/".lang('no_kp')."/".lang('telp_no').")");
        if(count($contents) > 0){
            $fields = array_keys($contents[0]);

            $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
        }else{
            redirect(base_url().PATH_TO_ADMIN.'reports/submission_number_by_date');
        }
    }

    function top_score_by_award_category(){
        $data['award_categories'] = $this->get_award_category();
        $award_category = $this->input->post('award_category');
        $data['selected_award_category'] = $award_category;

        if($award_category != ""){
            $award_category = explode(',',$award_category);
            if($award_category[0] == 0){ //category
                $where = "category = 0 AND area = ".$award_category[1];
            }else{
                $where = "category = 1 AND level = ".$award_category[1];
            } 
        }else{
            $award_category = "0,0";
            $where = "category = 0 AND area = 0";
        }

        $data['submissions'] = $this->top_score_by_award_category_handler($where);

        if(isset($_POST['btn_export'])){
            $contents = $this->generate_top_score_data_by_award($data['submissions']);
            $category_name = "";
            foreach ($data['award_categories'] as $key => $value) {
                if($value['id'] == $data['selected_award_category']){
                    $category_name = $value['category'];
                }
            }
            
            $file_name = "Top_60_Scores_by_".$category_name;
            $title = "Top 60 Scores by ".$category_name;
            $columns = array(lang('ranking'),lang('project_name'),lang('team_leader'),lang('mara_center'),lang('email'),lang('telp_no'),lang('kp_no'),lang('staff_id'),lang('average_score'),lang('group_name'));
            $fields = array_keys($contents[0]);

            $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
        }
        
        $this->load->view(PATH_TO_ADMIN.'report/top_score_by_award_category', $data);
    }

    private function get_award_category(){
        $categories = unserialize(APPLICATION_CATEGORY);
        $categories_services_area = unserialize(APPLICATION_CATEGORY_SERVICES_AREA);
        $categories_produk_level = unserialize(APPLICATION_CATEGORY_PRODUK_LEVEL);

        $award_categories = array();
        foreach ($categories as $key => $value) {
            if($key == 0){
                $subcategory = $categories_services_area;
            }else if($key == 1){
                $subcategory = $categories_produk_level;
            }

            foreach ($subcategory as $i => $subcat) {
                $award_categories[] = array('id' => $key.",".$i, 'category' => $value." - ".$subcat);
            }
        }

        return $award_categories;
    }

    private function top_score_by_award_category_handler($where = NULL){
        $this->load->model('mara_evaluation');
        $this->load->model('mara_group_evaluator');
        $this->load->model('mara_group_expert');

        $submissions = $this->mara_evaluation->get_top_score_list($where)->result_array();
        foreach ($submissions as $key => $value) {
            $group_evaluator = $this->mara_group_evaluator->find_one("mara_evaluator_id = ".$value['mara_evaluator_id']);
            if($group_evaluator){
                $group = $this->mara_group_expert->find_one("id = ".$group_evaluator['mara_group_expert_id']);
                
                $submissions[$key]['group_name'] = ($group ? $group['name'] : "");
            }
            
        }

        return $submissions;
    }

    private function generate_top_score_data_by_award($data){
        $contents = array();
        $no = 1; 
        foreach ($data as $key => $value) {
            $contents[$key]['rangking'] = $no;
            $contents[$key]['innovation_name'] = $value['innovation_name'];
            $contents[$key]['innovator_name'] = $value['innovator_name'];
            $contents[$key]['mara_center_name'] = $value['mara_center_name'];
            $contents[$key]['email'] = $value['email'];
            $contents[$key]['telp_no'] = $value['telp_no'];
            $contents[$key]['kp_no'] = $value['kp_no'];
            $contents[$key]['staff_id'] = $value['staff_id'];
            $contents[$key]['score'] = $value['score'];
            $contents[$key]['group_name'] = $value['group_name'];
        $no++;}

        return $contents;
    }

    function top_score_by_evaluator_group(){
        $this->load->model('mara_group_expert');

        $data['groups'] = $this->mara_group_expert->find_all();
        $group = $this->input->post('group');
        $data['selected_group'] = $group;

        if($group != ""){
           $where = "mara_group_expert_id = ".$group;
        }else{
            $where = "mara_group_expert_id = ".$data['groups'][0]['id'];
        }

        $data['submissions'] = $this->top_score_by_evaluator_group_handler($where);
        
        if(isset($_POST['btn_export'])){
            $contents = $this->generate_top_score_data_by_group($data['submissions']);
            $group_data = $this->mara_group_expert->find_one("id = ".$data['selected_group']);
            
            $file_name = "Top_100_Scores_by_".$group_data['name'];
            $title = "Top 100 Scores by ".$group_data['name'];
            $columns = array(lang('ranking'),lang('project_name'),lang('team_leader'),lang('mara_center'),lang('email'),lang('telp_no'),lang('kp_no'),lang('staff_id'),lang('scores_by_individual_evaluator'),lang('average_score'));
            $fields = array_keys($contents[0]);

            $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
        }
        
        $this->load->view(PATH_TO_ADMIN.'report/top_score_by_evaluator_group', $data);
    }

    private function top_score_by_evaluator_group_handler($where = NULL){
        $this->load->model('mara_evaluation');
        $this->load->model('mara_group_evaluator');
        $this->load->model('mara_group_expert');
        $this->load->model('mara_evaluator');

        $submissions = $this->mara_evaluation->get_top_score_by_group_list($where)->result_array();
        foreach ($submissions as $key => $value) {
            $individual_eval = $this->mara_evaluation->find('innovation_id = '.$value['innovation_id']);
            $str_individual_score = "";
            foreach ($individual_eval as $i => $eval) {
                $evaluator = $this->mara_evaluator->find_one('evaluator_id = '.$eval['mara_evaluator_id']);
                $str_individual_score .= $evaluator['name']." : ".$eval['total']."<br>";
            }
            $submissions[$key]['score_by_individual'] = $str_individual_score;
        }

        return $submissions;
    }

    private function generate_top_score_data_by_group($data){
        $contents = array();
        $no = 1; 
        foreach ($data as $key => $value) {
            $contents[$key]['rangking'] = $no;
            $contents[$key]['innovation_name'] = $value['innovation_name'];
            $contents[$key]['innovator_name'] = $value['innovator_name'];
            $contents[$key]['mara_center_name'] = $value['mara_center_name'];
            $contents[$key]['email'] = $value['email'];
            $contents[$key]['telp_no'] = $value['telp_no'];
            $contents[$key]['kp_no'] = $value['kp_no'];
            $contents[$key]['staff_id'] = $value['staff_id'];
            $contents[$key]['score_by_individual'] = str_replace("<br>", "\n", $value['score_by_individual']);
            $contents[$key]['score'] = $value['score'];
        $no++;}

        return $contents;
    }

    /*Temporary function*/
    public function export_data_eapproved_submission(){
        $this->lang->load(PATH_TO_ADMIN.'application',$this->language);

        $this->load->helper('mystring_helper');
        $this->load->model('mara_innovation_evaluator');
        $this->load->model('mara_innovation_category');
        $this->load->model('mara_group_expert');
        
        $categories = unserialize(APPLICATION_CATEGORY);
        $categories_services_area = unserialize(APPLICATION_CATEGORY_SERVICES_AREA);
        $categories_product_area = unserialize(APPLICATION_CATEGORY_PRODUK_AREA);
        $categories_product_level = unserialize(APPLICATION_CATEGORY_PRODUK_LEVEL);

        $innovations = $this->mara_approval_status->find("status = ".INNO_STATUS_SENT_APPROVAL, NULL, NULL, NULL, 'innovation_id');
        $list = array();
        foreach ($innovations as $key => $value) {
            $innovation = $this->innovation->find_one("is_mara_data = ".MARA_FLAG." AND innovation_id = ".$value['innovation_id']);
            $innovation['is_evaluation_done'] = $this->mara_innovation_evaluator->is_evaluation_done($value['innovation_id']);
            $status = $this->mara_approval_status->get_innovation_status($value['innovation_id']);
            if($status == INNO_STATUS_EVALUATED){
                if(!$innovation['is_evaluation_done']){
                    $status = INNO_STATUS_ASSIGNED_TO_EVALUATOR;
                }
            }

            $stat = $this->mara_approval_status->get_innovation_last_status($value['innovation_id']);
            if($stat['mara_round_id'] > 1){
                $status = INNO_STATUS_APPROVED;
            }
            
            $innovation['status'] = $status;
            $innovation['innovator'] = $this->mara_innovator->get_innovator_name($innovation['analyst_creator_id']);
            
            $innovation['score'] = $this->get_average_score($value['innovation_id']);
            $i_category = $this->mara_innovation_category->find_one("innovation_id = ".$value['innovation_id']);
            if($i_category){
                $innovation['application_category'] = $categories_product_level[$i_category['level']];
                $innovation['category'] = $categories[$i_category['category']];
                $innovation['subcategory'] = $i_category['category'] == 0 ? $categories_services_area[$i_category['area']] : $categories_product_area[$i_category['area']];
            }
            
            if($status == INNO_STATUS_APPROVED){
                $list[] = $innovation;
            }
        }

        $content = array();
        $no = 1;
        foreach ($list as $key => $value) {
            $content[$key]['no'] = $no;
            $content[$key]['innovator'] = $value['innovator'];
            $content[$key]['title'] = $value['name_in_melayu'];
            $content[$key]['description'] = $value['description_in_melayu'];
            $content[$key]['status'] = $value['status'];
            $content[$key]['score'] = $value['score'];
            $content[$key]['application_category'] = $value['application_category'];
            $content[$key]['category'] = $value['category'];
            $content[$key]['subcategory'] = $value['subcategory'];
        $no++;}


        $contents = $content;
        $file_name = "Innovation List (Status - Assigned to Evaluator)";
        $title = "Innovation List (Status - Assigned to Evaluator)";
        $columns = array("No",lang('innovator'),lang('innovation_product'),lang('description'),lang('status'),lang('average_score'),lang('application_category'),lang('innovation_category'),lang('subcategory'));
        $fields = array_keys($contents[0]);

        $this->export_to_excel($file_name,$title,$columns,$fields,$contents);

    }

    private function get_average_score($innovation_id){
        $this->load->model('mara_evaluation');
        $data_score = $this->mara_evaluation->get_average_score("innovation_id = ".$innovation_id)->row_array();
        
        return $data_score['score'];
    }

    function business_plan_submission_number_by_date(){
        $application = $this->mara_approval_status->find("status = ".INNO_STATUS_SENT_APPROVAL." AND mara_round_id = 2", 1, 0,"id DESC");
        $data['from_date'] = date('Y-m-d',strtotime('-29 days',strtotime($application[0]['created_at'])));
        $data['to_date'] = date('Y-m-d',strtotime($application[0]['created_at']));
        $data['modal_detail'] = $this->load->view(PATH_TO_ADMIN.'report/modal_detail', array('action_url' => 'export_business_plan_submission_number_by_date'), TRUE);
        $this->load->view(PATH_TO_ADMIN.'report/business_plan_submission_number_by_date',$data);
    }

    function business_plan_submission_number_by_date_handler(){
        $this->layout = FALSE;
        
        $xAxis = array();
        $data = array();
        $date_from = $this->input->post('date_from');
        $date_to = $this->input->post('date_to');

        $days = $this->count_interval($date_from, $date_to);
        
        for ($i=0; $i <= $days; $i++) { 
            $raw_date = strtotime('+'.$i.' days',strtotime($date_from));
            $xAxis[] = date('d-m-Y',$raw_date);
            $date = date('Y-m-d', $raw_date);
            $data[] = $this->mara_approval_status->get_number("status = ".INNO_STATUS_SENT_APPROVAL." AND mara_round_id = 2 AND DATE( created_at ) =  '".$date."'");
        }

        $result = array('xAxis' => $xAxis, 'series' => array(array('name' => 'Business Plan Submission Number', 'data' => $data)));
        echo json_encode($result);
    }

    function export_business_plan_submission_number_by_date(){
        $this->layout = FALSE;

        $date = $this->input->post('date');
        $from = $this->input->post('date_from');
        $to = $this->input->post('date_to');

        if($date != ""){
            $where = "mara_approval_status.status = ".INNO_STATUS_SENT_APPROVAL." AND mara_round_id = 2 AND DATE( mara_approval_status.created_at ) =  '".date('Y-m-d',strtotime($date))."'";
        }else{
            $where = "mara_approval_status.status = ".INNO_STATUS_SENT_APPROVAL." AND mara_round_id = 2 AND DATE( mara_approval_status.created_at ) BETWEEN '".date('Y-m-d',strtotime($from))."' AND '".date('Y-m-d',strtotime($to))."'";
        }

        $data = $this->mara_approval_status->get_business_plan_list($where)->result_array();
        $contents = $this->generate_business_plan_data($data);

        $file_name = "Business_Plan_Submission_Number_by_Date";
        $title = "Business Plan Submission Number by Date";
        $columns = array("No",lang('innovator'),lang('mara_center'),lang('innovation_product'),lang('description'),lang('the_main_issue_addressed_by_innovation'), lang('target_user'), lang('target_buyer'),lang('cost_per_unit'),lang('competitor'),lang('competitive_advantage'),lang('innovation_impact'),lang('business_capital'),lang('source_of_income'),lang('url'));
        if(count($contents) > 0){
            $fields = array_keys($contents[0]);

            $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
        }else{
            redirect(base_url().PATH_TO_ADMIN.'reports/business_plan_submission_number_by_date');
        }
    }

    private function generate_business_plan_data($data){
        $this->load->model('mara_innovation_business_plan');
        $this->lang->load('administrator/business_plan',$this->language);

        $contents = array();
        $no = 1; 
        foreach ($data as $key => $value) {
            $contents[$key]['no'] = $no;
            $contents[$key]['innovator'] = $value['innovator_name'];
            $contents[$key]['mara_center'] = $value['mara_center'];
            $contents[$key]['innovation'] = $value['name_in_melayu'];
            $contents[$key]['description'] = $value['description'];
            $contents[$key]['problem'] = $value['problem'];
            $contents[$key]['target_user'] = $value['target_user'];
            $contents[$key]['target_buyer'] = $value['target_buyer'];
            $contents[$key]['manufacturing_cost'] = $value['manufacturing_cost'];
            $contents[$key]['competitor'] = $value['competitor'];
            $contents[$key]['competitive_advantage'] = $value['competitive_advantage'];
            $contents[$key]['impact'] = $value['impact'];
            $contents[$key]['business_capital'] = $value['business_capital'];
            $contents[$key]['source_of_income'] = $value['source_of_income'];
            $contents[$key]['link'] = $value['url'];
        $no++;}

        return $contents;
    }

    /*Temporary function*/
    function submission_by_mara_center_detail(){
        $this->load->model('mara_center');

        $data['mara_center'] = $this->mara_center->find_all();
        $data['selected_mara_center'] = $this->input->post('mara_center_id');
        if(isset($_POST['btn_report'])){
            $data['users'] = $this->submission_by_mara_center_detail_handler($data['selected_mara_center']);
        }else if(isset($_POST['btn_export'])){
            $this->lang->load('account',$this->language);
            $users = $this->submission_by_mara_center_detail_handler($data['selected_mara_center']);
            $data_content = array();
            
            $no=1;
            foreach ($users as $key => $value) {
                    $data_content[$key]['no'] = $no;
                    $data_content[$key]['name'] = $value['name'];
                    $data_content[$key]['submission'] = count($value['submissions']);
                    $data_content[$key]['product'] = count($value['product']);
                    $data_content[$key]['service'] = count($value['service']);
            $no++;}
            $contents = $data_content;

            $file_name = "Submissions_by_Mara_Center";
            $title = "Submissions by Mara Center";
            $columns = array("No",lang('mara_center'),"Submission","Produk", "Perkhidmatan");
            $fields = array_keys($contents[0]);

            $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
        }else{
            $data['users'] = $this->submission_by_mara_center_detail_handler();
        }
        $this->load->view(PATH_TO_ADMIN.'report/user_by_mara_center_detail',$data);
    }

    private function submission_by_mara_center_detail_handler($mara_center_id= 0, $export_btn = FALSE){
        $this->load->model('mara_center');

        if($export_btn){
            
        }else{
            if($mara_center_id != 0){
                $where = "id = ".$mara_center_id;
            }else{
                $where = "";
            }
            $mara_center = $this->mara_center->find($where);
            foreach ($mara_center as $key => $value) {
                $where_submission = "mara_approval_status.status = ".INNO_STATUS_DRAFT." AND mara_round_id = 1 AND mara_innovator.mara_center_id = ".$value['id'];
                $mara_center[$key]['submissions'] = $this->mara_approval_status->get_innovation_list($where_submission)->result_array();
                $mara_center[$key]['product'] = $this->mara_approval_status->get_innovation_list($where_submission." AND mara_innovation_category.category = 1")->result_array();
                $mara_center[$key]['service'] = $this->mara_approval_status->get_innovation_list($where_submission." AND mara_innovation_category.category = 0")->result_array();
            }
            return $mara_center;
        }
    }
    /*end*/

}
