<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Dashboard extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Dashboard";
		$this->menu = "dashboard";

		$this->load->model('innovation');
		$this->load->model('mara_innovation_category');

		$this->scripts[] = 'plugins/highcharts/highcharts';
		$this->scripts[] = 'administrator/dashboard';
    }

    public function index(){
        $this->load->model('mara_approval_status');

        $total = $this->mara_approval_status->get_number("status = ".INNO_STATUS_DRAFT." AND mara_round_id = 1");
        $submitted = $this->mara_approval_status->get_number("status = ".INNO_STATUS_SENT_APPROVAL." AND mara_round_id = 1");
        $data['draft_number'] = $total - $submitted;
		$this->load->view(PATH_TO_ADMIN.'dashboard/index', $data);
    }

    function report_number_by_day_handler(){
    	$this->layout = FALSE;
    	
    	$xAxis = array();
    	$data = array();
    	for ($i=6; $i >= 0; $i--) { 
    		$xAxis[] = date('d-m-Y',strtotime($i." days ago"));;
    		$date = date('Y-m-d',strtotime($i." days ago"));
    		$data[] = $this->innovation->get_number("DATE( submission_date ) =  '".$date."'");
    	}

    	$result = array('xAxis' => $xAxis, 'series' => array(array('name' => 'Application Number', 'data' => $data)));
    	echo json_encode($result);
    }

    function report_number_by_category_handler(){
    	$this->layout = FALSE;

    	$data = array();
    	$categories = unserialize(APPLICATION_CATEGORY);
    	foreach ($categories as $key => $value) {
    		$data[$key]['name'] = $value;
    		$data[$key]['y'] = $this->mara_innovation_category->get_number("category = ".$key);
    	}

    	$result = array("data" => $data);
    	echo json_encode($result);
    }
}
