<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Group_experts extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Manage Group";
		$this->menu = "group_expert";

		$this->load->model('mara_evaluator');
        $this->load->model('mara_group_expert');
        $this->load->model('mara_group_evaluator');

		$this->lang->load(PATH_TO_ADMIN.'group_expert',$this->language);

		$this->scripts[] = 'administrator/group_expert';
    }

    public function index(){
    	$data['alert'] = $this->session->flashdata('alert');
    	$data['groups'] = $this->mara_group_expert->find_all();

		$this->load->view(PATH_TO_ADMIN.'group_expert/list', $data);
    }

    function add(){
        $data['evaluators'] = $this->mara_evaluator->find_all();
        $data['form_action'] = 'store';
        $this->load->view(PATH_TO_ADMIN.'group_expert/form',$data);
    }

    function store(){
    	$this->layout = FALSE;

        $postdata = $this->postdata();
        if($postdata['name'] == ""){
            $this->session->set_flashdata('alert','Sorry, the fields are required.');
        }else{
            $data = array(
                "name" => $postdata['name'],
                "description" => $postdata['description']);
        
            if($id = $this->mara_group_expert->insert($data)){
                $this->session->set_flashdata('alert','New Group has been created');
            }else{
                $this->session->set_flashdata('alert','An error occured, please try again later');
            }
        }

    	redirect(base_url().PATH_TO_ADMIN.'group_experts');
    }

    public function edit($id = 0){
		$group = $this->mara_group_expert->find_one("id = ".$id);
        $data['form_action'] = 'update';
        $data['evaluators'] = $this->mara_evaluator->find_all();
		if($group){
			$data['group'] = $group;
		}
		
		$this->load->view(PATH_TO_ADMIN.'group_expert/form',$data);
	}

	function update(){
		$this->layout = FALSE;

        $postdata = $this->postdata();
        if($postdata['name'] == ""){
            $this->session->set_flashdata('alert','Sorry, the fields are required.');
        }else{
            $data = array(
                "name" => $postdata['name'],
                "description" => $postdata['description']);
        
            if($this->mara_group_expert->update($postdata['id'],$data)){
                $this->session->set_flashdata('alert','New Group has been created');
            }else{
                $this->session->set_flashdata('alert','An error occured, please try again later');
            }
        }

        redirect(base_url().PATH_TO_ADMIN.'group_experts');
	}

    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect(base_url().PATH_TO_ADMIN.'group_experts');
    }

    function delete($id){
        $this->layout = FALSE;
        if($this->mara_group_expert->delete($id)){
            $this->session->set_flashdata('alert','Group has been deleted.');
        }else{
            $this->session->set_flashdata('alert','Group can not be deleted.');
        }

        redirect(base_url().PATH_TO_ADMIN.'group_experts');
    }
}
