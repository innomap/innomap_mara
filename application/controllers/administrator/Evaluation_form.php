<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Evaluation_form extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Manage Evaluation Form";
		$this->menu = "evaluation_form";

        $this->load->model('mara_evaluation_form');
		$this->load->model('mara_evaluation_focus');

		$this->lang->load(PATH_TO_ADMIN.'evaluation_form',$this->language);

		$this->scripts[] = 'administrator/evaluation_form';
    }

    public function index(){
    	$data['alert'] = $this->session->flashdata('alert');
    	$data['evaluation_form'] = $this->mara_evaluation_form->find_all();
        foreach ($data['evaluation_form'] as $key => $value) {
            $data['evaluation_form'][$key]['evaluation_focus'] = $this->mara_evaluation_focus->find("mara_evaluation_form_id = ".$value['id']);
        }
        $data['categories'] = unserialize(APPLICATION_CATEGORY);
        $data['categories_services_area'] = unserialize(APPLICATION_CATEGORY_SERVICES_AREA);
        $data['categories_produk_level'] = unserialize(APPLICATION_CATEGORY_PRODUK_LEVEL);

        $data_modal['categories'] = unserialize(APPLICATION_CATEGORY);
        $data_modal['categories_services_area'] = unserialize(APPLICATION_CATEGORY_SERVICES_AREA);
        $data_modal['categories_produk_level'] = unserialize(APPLICATION_CATEGORY_PRODUK_LEVEL);
    	$data['form_view'] = $this->load->view(PATH_TO_ADMIN.'evaluation_form/form', $data_modal, TRUE);

		$this->load->view(PATH_TO_ADMIN.'evaluation_form/list', $data);
    }

    function store(){
    	$this->layout = FALSE;

        $postdata = $this->postdata();
        $data_form = array(
            "category" => $postdata['category'],
            "subcategory" => ($postdata['category'] == 0 ? $postdata['subcategory_service'] : $postdata['subcategory_product']));

        if($id = $this->mara_evaluation_form->insert($data_form)){
            $this->session->set_flashdata('alert','New Evaluation Form has been created');
        }else{
            $this->session->set_flashdata('alert','An error occured, please try again later');
        }

    	redirect(base_url().PATH_TO_ADMIN.'evaluation_form/edit/'.$id);
    }

    public function edit($id = 0){
    	$this->scripts[] = 'administrator/evaluation_focus';

		$evaluation_form = $this->mara_evaluation_form->find_by_id($id);
		if($evaluation_form){
			$data['evaluation_form'] = $evaluation_form;
		}

        $data['categories'] = unserialize(APPLICATION_CATEGORY);
        $data['categories_services_area'] = unserialize(APPLICATION_CATEGORY_SERVICES_AREA);
        $data['categories_produk_level'] = unserialize(APPLICATION_CATEGORY_PRODUK_LEVEL);
        $data['alert'] = $this->session->flashdata('alert');
        $data['evaluation_focus'] = $this->mara_evaluation_focus->find("mara_evaluation_form_id = ".$id);
        $data['form_view'] = $this->load->view(PATH_TO_ADMIN.'evaluation_focus/form', array('form_id' => $id), TRUE);
		
		$this->load->view(PATH_TO_ADMIN.'evaluation_focus/list', $data);
	}

	function update(){
		$this->layout = FALSE;

		$postdata = $this->postdata();
        
    	$data_form = array(
            "category" => $postdata['category'],
            "subcategory" => ($postdata['category'] == 0 ? $postdata['subcategory_service'] : $postdata['subcategory_product']));

        if($this->mara_evaluation_form->update($postdata['form_id'], $data_form)){
            echo $this->db->last_query();
            $this->session->set_flashdata('alert','New Evaluation Form has been updated');
        }else{
            $this->session->set_flashdata('alert','An error occured, please try again later');
        }

    	redirect(base_url().PATH_TO_ADMIN.'evaluation_form');
	}

    function delete($id){
        $this->layout = FALSE;
        if($this->mara_evaluation_form->delete($id)){
            $this->session->set_flashdata('alert','Evaluation Form has been deleted.');
        }else{
            $this->session->set_flashdata('alert','Evaluation Form can not be deleted.');
        }

        redirect(base_url().PATH_TO_ADMIN.'evaluation_form');
    }

    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect(base_url().PATH_TO_ADMIN.'evaluators');
    }
}
