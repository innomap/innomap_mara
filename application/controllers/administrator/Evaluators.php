<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Evaluators extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Manage Evaluator";
		$this->menu = "evaluator";

		$this->load->model('mara_evaluator');
        $this->load->model('user');
        $this->load->model('mara_group_expert');
        $this->load->model('mara_group_evaluator');

		$this->lang->load(PATH_TO_ADMIN.'evaluator',$this->language);

		$this->scripts[] = 'administrator/evaluator';
    }

    public function index(){
    	$data['alert'] = $this->session->flashdata('alert');
    	$data['evaluators'] = $this->mara_evaluator->get_with_user();
    	$data['form_view'] = $this->load->view(PATH_TO_ADMIN.'evaluator/form', array('groups' => $this->mara_group_expert->find_all()), TRUE);

		$this->load->view(PATH_TO_ADMIN.'evaluator/list', $data);
    }

    function store(){
    	$this->layout = FALSE;

        $postdata = $this->postdata();
        if($postdata['email'] != "" && $postdata['name'] != ""){
            if($this->user->is_email_exist($postdata['email'])){
                $this->session->set_flashdata('alert','Sorry, your email has been registered.');
            }else{
            	$username = $this->generate_username($postdata['name']);
                $data = array(
                    "username" => $username,
                    "email" => $postdata['email'],
                    "password" => $postdata['password'],
                    "status" => 1,
                    "is_mara_user" => 1);
            
                if($id = $this->user->insert_user($data, ROLE_MARA_EVALUATOR)){
                    $evaluator = array('evaluator_id' => $id, 'name' => $postdata['name']);
                    $this->mara_evaluator->insert($evaluator);

                    $group_evaluator = array('mara_group_expert_id' => $postdata['group_id'], 'mara_evaluator_id' => $id);
                    $this->mara_group_evaluator->insert($group_evaluator);

                    $this->session->set_flashdata('alert','New Evaluator has been created');
                }else{
                    $this->session->set_flashdata('alert','An error occured, please try again later');
                }
            }
        }else{
            $this->session->set_flashdata('alert','Some fields are required');
        }

    	redirect(base_url().PATH_TO_ADMIN.'evaluators');
    }

    public function edit($id = 0){
    	$this->layout = FALSE;

		$evaluator = $this->mara_evaluator->get_with_user("evaluator_id = ".$id);
		if($evaluator){
            $evaluator[0]['group'] = $this->mara_group_evaluator->find_one("mara_evaluator_id = ".$id); 
			$ret = array(
				'status' => '1',
				'data' => $evaluator[0]
			);
		}else{
			$ret = array(
				'status' => '0'
			);
		}
		
		echo json_encode($ret);
	}

	function update(){
		$this->layout = FALSE;

		$postdata = $this->postdata();

    	$username = $this->generate_username($postdata['name']);
        $data = array(
            "username" => $username,
            "email" => $postdata['email'],
            "password" => $postdata['password']);
    
        if($this->user->update_user($postdata['id'], $data)){
            if($postdata['email'] != "" && $postdata['name'] != ""){
                $evaluator = array('evaluator_id' => $postdata['id'], 'name' => $postdata['name']);
                $this->mara_evaluator->update($postdata['id'], $evaluator);

                if($this->mara_group_evaluator->delete_by_evaluator($postdata['id'])){
                    $group_evaluator = array('mara_group_expert_id' => $postdata['group_id'], 'mara_evaluator_id' => $postdata['id']);
                    $this->mara_group_evaluator->insert($group_evaluator);
                }

                $this->session->set_flashdata('alert','New Evaluator has been updated');
            }
        }else{
            $this->session->set_flashdata('alert','An error occured, please try again later');
        }

    	redirect(base_url().PATH_TO_ADMIN.'evaluators');
	}

    function check_email_exist(){
        $this->layout = FALSE;

        $email = $this->input->post('email');
        $user_id = $this->input->post('user_id');
        if($this->user->is_email_exist($email, $user_id)){
            $retval = false;
        }else{
            $retval = true;
        }

        echo json_encode(array('valid' => $retval));
    }

    private function generate_username($name){
        return str_replace(' ', '_', $name).rand();
    }

    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect(base_url().PATH_TO_ADMIN.'evaluators');
    }

    function delete($id){
        $this->layout = FALSE;
        
        if($this->user->delete($id)){
            $this->session->set_flashdata('alert','Evaluator has been deleted.');
        }else{
            $this->session->set_flashdata('alert','Evaluator can not be deleted.');
        }

        redirect(base_url().PATH_TO_ADMIN.'evaluators');
    }
}
