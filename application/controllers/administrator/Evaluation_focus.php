<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Evaluation_focus extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Manage Evaluation Focus";
		$this->menu = "evaluation_form";

		$this->load->model('mara_evaluation_focus');
        $this->load->model('mara_evaluation_criteria');

		$this->lang->load(PATH_TO_ADMIN.'evaluation_form',$this->language);

		$this->scripts[] = 'administrator/evaluation_focus';
    }

    public function index(){
    	$data['alert'] = $this->session->flashdata('alert');
    	$data['evaluation_focus'] = $this->mara_evaluation_focus->find_all();
    	$data['form_view'] = $this->load->view(PATH_TO_ADMIN.'evaluation_focus/form', '', TRUE);

		$this->load->view(PATH_TO_ADMIN.'evaluation_focus/list', $data);
    }

    function store(){
    	$this->layout = FALSE;

        $postdata = $this->postdata();
        $data_focus = array(
            "mara_evaluation_form_id" => $postdata['form_id'],
            "label" => $postdata['label'],
            "percentage" => $postdata['percentage']);

        if($id = $this->mara_evaluation_focus->insert($data_focus)){
            foreach ($postdata['question'] as $key => $value) {
                $data_criteria = array('mara_evaluation_focus_id' => $id,
                                        'question' => $value,
                                        'description' => $postdata['description'][$key],
                                        'guideline' => $postdata['guideline'][$key],
                                        'max_point' => $postdata['max_point'][$key]);
                $this->mara_evaluation_criteria->insert($data_criteria);
            }
            $this->session->set_flashdata('alert','New Evaluation Focus has been created');
        }else{
            $this->session->set_flashdata('alert','An error occured, please try again later');
        }

    	redirect(base_url().PATH_TO_ADMIN.'evaluation_form/edit/'.$postdata['form_id']);
    }

    public function edit($id = 0){
    	$this->layout = FALSE;

		$evaluation_focus = $this->mara_evaluation_focus->find_by_id($id);
		if($evaluation_focus){
            $evaluation_focus['criteria'] = $this->mara_evaluation_criteria->find("mara_evaluation_focus_id = ".$id);
			$ret = array(
				'status' => '1',
				'data' => $evaluation_focus
			);
		}else{
			$ret = array(
				'status' => '0'
			);
		}
		
		echo json_encode($ret);
	}

	function update(){
		$this->layout = FALSE;

		$postdata = $this->postdata();

    	$data_focus = array(
            "label" => $postdata['label'],
            "percentage" => $postdata['percentage']);

        if($this->mara_evaluation_focus->update($postdata['id'], $data_focus)){
            $this->mara_evaluation_criteria->delete_by_evaluation_focus($postdata['id']);
            foreach ($postdata['question'] as $key => $value) {
                $data_criteria = array('mara_evaluation_focus_id' => $postdata['id'],
                                        'question' => $value,
                                        'description' => $postdata['description'][$key],
                                        'guideline' => $postdata['guideline'][$key],
                                        'max_point' => $postdata['max_point'][$key]);
                $this->mara_evaluation_criteria->insert($data_criteria);
            }
            $this->session->set_flashdata('alert','New Evaluator has been updated');
        }else{
            $this->session->set_flashdata('alert','An error occured, please try again later');
        }

    	redirect(base_url().PATH_TO_ADMIN.'evaluation_form/edit/'.$postdata['form_id']);
	}

    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect(base_url().PATH_TO_ADMIN.'evaluation_focus');
    }

    function delete($id, $form_id){
        $this->layout = FALSE;
        if($this->mara_evaluation_focus->delete($id)){
            $this->session->set_flashdata('alert','Evaluation Focus has been deleted.');
        }else{
            $this->session->set_flashdata('alert','Evaluation Focus can not be deleted.');
        }

        redirect(base_url().PATH_TO_ADMIN.'evaluation_form/edit/'.$form_id);
    }
}
