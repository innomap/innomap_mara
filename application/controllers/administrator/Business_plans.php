<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Business_plans extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Business Plan";
		$this->menu = "business_plan";

        $this->lang->load(PATH_TO_ADMIN.'business_plan',$this->language);

        $this->load->model('mara_approval_status');
        $this->load->model('innovation');
		$this->load->model('mara_innovation_business_plan');
        $this->load->model('mara_innovation_business_plan_picture');
        $this->load->model('mara_innovation_business_plan_doc');
        $this->load->model('mara_innovation_business_plan_eval');
        $this->load->model('mara_innovation_business_plan_eval1');
        $this->load->model('mara_innovation_business_plan_eval2');
        $this->load->model('mara_evaluation');
        $this->load->model('mara_evaluator');

		$this->scripts[] = 'administrator/business_plan';
        $this->scripts[] = 'plugins/fancybox/jquery.fancybox.pack';

        $this->styles[] = 'fancybox/jquery.fancybox';
    }

    public function index(){
        $this->load->helper('mystring_helper');

        $data['alert'] = $this->session->flashdata('alert');
        $data['status'] = unserialize(INNO_STATUS_ADMIN);
        $innovations = $this->mara_approval_status->find("status = ".INNO_STATUS_SENT_APPROVAL." AND mara_round_id > 1", NULL, NULL, NULL, 'innovation_id');
        $list = array();
        foreach ($innovations as $key => $value) {
            $business_plan = $this->mara_innovation_business_plan->find_one("innovation_id = ".$value['innovation_id']);
            $innovation = $this->innovation->find_one("innovation_id = ".$value['innovation_id']);
            $business_plan['title'] = $innovation['name_in_melayu'];
            $status = $this->mara_approval_status->get_innovation_status($value['innovation_id']);
            $business_plan['status'] = $status;
            $business_plan['score'] = $this->get_score($business_plan['id']);
            $business_plan['first_score'] = $this->get_first_eval_score($value['innovation_id']);
            $list[] = $business_plan;
        }
        $data['business_plans'] = $list;
        $data['form_view'] = $this->load->view(PATH_TO_ADMIN.'business_plan/form', '', TRUE);
		$this->load->view(PATH_TO_ADMIN.'business_plan/list', $data);
    }

    function view($id, $view_mode = 1){
        $this->load->model('mara_round');

        $data['alert'] = $this->session->flashdata('alert');
        $data['alert_dialog'] = $this->session->flashdata('alert_dialog');
        $data['action_url'] = "innovations/business_plan_handler";
        $data['view_mode'] = $view_mode;
        $data['round'] = $this->mara_round->find_one("id = 2");
        $business_plan = $this->mara_innovation_business_plan->find_one("id = ".$id);
        if($business_plan){
            $data['innovation'] = $this->innovation->find_one("innovation_id = ".$business_plan['innovation_id']);
            $data['business_plan'] = $business_plan;
            $data['business_plan_picture'] = $this->mara_innovation_business_plan_picture->find('mara_innovation_business_plan_id = '.$business_plan['id']);
            $data['business_plan_doc'] = $this->mara_innovation_business_plan_doc->find('mara_innovation_business_plan_id = '.$business_plan['id']);
        }
        $this->load->view('innovation/business_plan_form',$data);
    }

    function evaluation($id, $is_offline_bp = 0){
        $this->layout = FALSE;

        if($is_offline_bp == 1){
            $business_plan = $this->mara_innovation_business_plan->find_one("innovation_id = ".$id);
            if(!$business_plan){
                $data = array('innovation_id' => $id, 'description' => "TIDAK DI ISI", 'created_at' => date('Y-m-d H:i:s'));
                if($id = $this->mara_innovation_business_plan->insert($data)){
                    $business_plan = $this->mara_innovation_business_plan->find_one("id = ".$id);
                    $this->mara_approval_status->insert(array('user_id' => $this->userdata['id'],'innovation_id' => $data['innovation_id'], 'mara_round_id' => 2, 'status' => INNO_STATUS_SENT_APPROVAL));
                }
            }else{
                $id = $business_plan['id'];
            }
        }else{
            $business_plan = $this->mara_innovation_business_plan->find_one("id = ".$id);
        }
    
        if($business_plan){
            $business_plan['evaluations'] = $this->mara_innovation_business_plan_eval->find("mara_innovation_business_plan_id = ".$id);
            $business_plan['evaluations1'] = $this->mara_innovation_business_plan_eval1->find("mara_innovation_business_plan_id = ".$id);
            $business_plan['evaluations2'] = $this->mara_innovation_business_plan_eval2->find("mara_innovation_business_plan_id = ".$id);
            $individual_eval = $this->mara_evaluation->find('innovation_id = '.$business_plan['innovation_id']);
            $str_individual_score = "";
            foreach ($individual_eval as $i => $eval) {
                $evaluator = $this->mara_evaluator->find_one('evaluator_id = '.$eval['mara_evaluator_id']);
                $str_individual_score .= '<div class="form-group">';
                $str_individual_score .=    '<div class="col-md-4 no-padding">';
                $str_individual_score .=        '<label>'.$evaluator['name'].'</label>';
                $str_individual_score .=    '</div>';
                $str_individual_score .=    '<div class="col-md-8">';
                $str_individual_score .=        '<input type="text" class="form-control" readonly value="'.$eval['total'].'">';
                $str_individual_score .=    '</div>';
                $str_individual_score .= '</div>';
            }
            $business_plan['score_by_individual'] = $str_individual_score;
            $business_plan['view_mode'] = 0;
            $ret = array(
                'status' => '1',
                'data' => $business_plan
            );
        }else{
            $ret = array(
                'status' => '0'
            );
        }
        
        echo json_encode($ret);
    }

    function evaluation_handler(){
        $this->layout = FALSE;

        $postdata = $this->postdata();

        foreach ($postdata['score_id'] as $key => $value) {
            $data['mara_innovation_business_plan_id'] = $postdata['id'];
            $data['score'] = $postdata['score_'.$key];
            if(isset($_FILES['score_attachment_'.$key])){
                if($_FILES['score_attachment_'.$key]['name'] != NULL){
                    if(isset($_POST['h_attachment_'.$key])){
                        $this->_remove($postdata['h_attachment_'.$key],PATH_TO_INNOVATION_BUSINESS_PLAN_EVALUATION);
                    }
                    $filename = $this->generate_filename($postdata['id'],$_FILES['score_attachment_'.$key]['name']);
                    $uploaded_file = $this->_upload($filename,'score_attachment_'.$key,PATH_TO_INNOVATION_BUSINESS_PLAN_EVALUATION, NULL, 1);
                    
                    if($uploaded_file != ""){
                        $data['attachment'] = $uploaded_file;
                    }else{
                        $this->session->set_flashdata('alert','Attachment cannot be uploaded.');
                        redirect(base_url().'business_plans');
                    }
                }
            }

            if($value == 0){
                $data['created_at'] = Date("Y-m-d H:i:s");
                $this->mara_innovation_business_plan_eval->insert($data);
            }else{
                $this->mara_innovation_business_plan_eval->update($value, $data);
            }
        }

        if(isset($_POST['deleted_score'])){
            foreach ($postdata['deleted_score'] as $key => $value) {
                $this->mara_innovation_business_plan_eval->delete($value);
            }
        }

        if($this->mara_innovation_business_plan_eval->find_one("mara_innovation_business_plan_id = ".$postdata['id'])){
            // $this->mara_innovation_business_plan->update($postdata['id'], array('score_3' => $postdata['score_3'],'score_4' => $postdata['score_4']));

            foreach ($postdata['score1_id'] as $key => $value) {
                $data1['mara_innovation_business_plan_id'] = $postdata['id'];
                $data1['score'] = $postdata['score1_'.$key];
                if(isset($_FILES['score1_attachment_'.$key])){
                    if($_FILES['score1_attachment_'.$key]['name'] != NULL){
                        if(isset($_POST['h1_attachment_'.$key])){
                            $this->_remove($postdata['h1_attachment_'.$key],PATH_TO_INNOVATION_BUSINESS_PLAN_EVALUATION1);
                        }
                        $filename = $this->generate_filename($postdata['id'],$_FILES['score1_attachment_'.$key]['name']);
                        $uploaded_file = $this->_upload($filename,'score1_attachment_'.$key,PATH_TO_INNOVATION_BUSINESS_PLAN_EVALUATION1, NULL, 1);
                        
                        if($uploaded_file != ""){
                            $data1['attachment'] = $uploaded_file;
                        }else{
                            $this->session->set_flashdata('alert','Attachment cannot be uploaded.');
                            redirect(base_url().'business_plans');
                        }
                    }
                }

                if($value == 0){
                    $data1['created_at'] = Date("Y-m-d H:i:s");
                    $this->mara_innovation_business_plan_eval1->insert($data1);
                }else{
                    $this->mara_innovation_business_plan_eval1->update($value, $data1);
                }
            }

            if(isset($_POST['deleted_score1'])){
                foreach ($postdata['deleted_score1'] as $key => $value) {
                    $this->mara_innovation_business_plan_eval1->delete($value);
                }
            }

            if($this->mara_innovation_business_plan_eval1->find_one("mara_innovation_business_plan_id = ".$postdata['id'])){
                foreach ($postdata['score2_id'] as $key => $value) {
                    $data1['mara_innovation_business_plan_id'] = $postdata['id'];
                    $data1['score'] = $postdata['score2_'.$key];
                    if(isset($_FILES['score2_attachment_'.$key])){
                        if($_FILES['score2_attachment_'.$key]['name'] != NULL){
                            if(isset($_POST['h2_attachment_'.$key])){
                                $this->_remove($postdata['h2_attachment_'.$key],PATH_TO_INNOVATION_BUSINESS_PLAN_EVALUATION2);
                            }
                            $filename = $this->generate_filename($postdata['id'],$_FILES['score2_attachment_'.$key]['name']);
                            $uploaded_file = $this->_upload($filename,'score2_attachment_'.$key,PATH_TO_INNOVATION_BUSINESS_PLAN_EVALUATION2, NULL, 2);
                            
                            if($uploaded_file != ""){
                                $data1['attachment'] = $uploaded_file;
                            }else{
                                $this->session->set_flashdata('alert','Attachment cannot be uploaded.');
                                redirect(base_url().'business_plans');
                            }
                        }
                    }

                    if($value == 0){
                        $data1['created_at'] = Date("Y-m-d H:i:s");
                        $this->mara_innovation_business_plan_eval2->insert($data1);
                    }else{
                        $this->mara_innovation_business_plan_eval2->update($value, $data1);
                    }
                }

                if(isset($_POST['deleted_score2'])){
                    foreach ($postdata['deleted_score2'] as $key => $value) {
                        $this->mara_innovation_business_plan_eval2->delete($value);
                    }
                }

                if($this->mara_innovation_business_plan_eval2->find_one("mara_innovation_business_plan_id = ".$postdata['id'])){
                    $mara_round = 2;
                    $is_evaluated = $this->mara_approval_status->find_one('innovation_id = '.$postdata['innovation_id'].' AND mara_round_id = '.$mara_round.' AND status = '.INNO_STATUS_EVALUATED);
                    if(!$is_evaluated){            
                        $this->mara_approval_status->insert(array('user_id' => $this->userdata['id'],'innovation_id' => $postdata['innovation_id'], 'mara_round_id' => $mara_round, 'status' => INNO_STATUS_EVALUATED));
                    }

                    $this->session->set_flashdata('alert','Data has been updated.');
                    redirect(base_url().PATH_TO_ADMIN.'business_plans');
                }else{
                     $this->session->set_flashdata('alert','An error occurred please try again later.');
                    redirect(base_url().PATH_TO_ADMIN.'business_plans');
                }
            }else{
                $this->session->set_flashdata('alert','An error occurred please try again later.');
                redirect(base_url().PATH_TO_ADMIN.'business_plans');
            }
        }else{
            $this->session->set_flashdata('alert','An error occurred please try again later.');
            redirect(base_url().PATH_TO_ADMIN.'business_plans');
        }
    }

    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect(base_url().PATH_TO_ADMIN.'business_plans');
    }

    private function _upload($name,$attachment,$upload_path, $thumb_path = NULL, $is_file = 0) {
        $this->load->library('upload');
        $config['file_name']        = $name;
        $config['upload_path']      = $upload_path;
        if($is_file == 0){
            $config['allowed_types']    = 'png|jpg|gif|bmp|jpeg';
        }else{
            $config['allowed_types']    = 'png|jpg|gif|bmp|jpeg|pdf|doc|docx|xls|xlsx';
        }
        $config['remove_spaces']    = TRUE;
        
        $this->upload->initialize($config);
        if(!$this->upload->do_upload($attachment,true)) {
            //echo $this->upload->display_errors();
            return "";
        }else{
            $upload_data = $this->upload->data();
            if($thumb_path != NULL){
                $this->create_thumbnail($upload_data['image_width'],$upload_data['image_height'],$upload_path,$upload_data['file_name'], $thumb_path);
            }
            return $upload_data['file_name'];
        }
    }

    private function generate_filename($id, $filename){
        $attachment_name = preg_replace('/\s+/', '_', $filename);
        $attachment_name = str_replace('.', '_', $attachment_name);
        $retval = $id."_".$attachment_name;

        return $retval;
    }

    private function _remove($file_name,$path){
        if (file_exists(realpath(APPPATH . '../'.$path) . DIRECTORY_SEPARATOR . $file_name)) {
            unlink("./".$path."/".$file_name);
        }
    }

    private function get_score($business_plan_id){
        $get = $this->mara_innovation_business_plan_eval->get_total_score("mara_innovation_business_plan_eval.mara_innovation_business_plan_id = ".$business_plan_id)->row_array();
        return $get['score'];
    }

    private function get_first_eval_score($innovation_id){
        $data_score = $this->mara_evaluation->get_average_score("innovation_id = ".$innovation_id)->row_array();
        
        return $data_score['score'];
    }
}


