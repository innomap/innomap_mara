<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Accounts extends Common {

	function __construct() {
		parent::__construct("account");

		$this->load->model('user');

		$this->lang->load('account',$this->language);

		$this->scripts[] = 'administrator/account';
    }

    public function index(){
		$this->user = $this->user_session->get_admin();
		if ($this->user) {
			redirect(base_url().PATH_TO_ADMIN.'dashboard');
		}else{
			$this->login();
		}
    }

	public function login(){
		$this->title = "Login";
		$data['alert'] = $this->session->flashdata('alert');
		$this->load->view(PATH_TO_ADMIN.'account/form_login',$data);
	}

	function login_auth(){
		$this->layout = FALSE;
		
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		
		$this->auth($username, $password);
	}

	private function auth($username, $password){
		$response = array();
		
		$user = $this->user->auth('username = "'.$username.'" AND is_mara_user = 1 AND role_id = '.ROLE_MARA_ADMINISTRATOR);
		if($user != NULL){
			if($this->check_password($user['username'], $password, $user['password'])){
				$this->user_session->set_admin(array('id' => $user['user_id'],'username' => $user['username'],'email' => $user['email'],'role_id' => ROLE_MARA_ADMINISTRATOR));
				redirect(base_url().PATH_TO_ADMIN.'accounts');
			}else{
				$this->session->set_flashdata('alert','The username or password you entered is incorrect.');
				redirect(base_url().PATH_TO_ADMIN);
			}
		}else{
			$this->session->set_flashdata('alert','The username or password you entered is incorrect.');
			redirect(base_url().PATH_TO_ADMIN);
		}
	}

	private function check_password($username, $password, $hash) {
		$password = $this->user->get_hash($username, $password);
		if($password == $hash){
			return true;
		}else{
			return false;
		}
	}

	public function logout() {
		$this->user_session->clear();
		redirect(base_url().PATH_TO_ADMIN.'login');
	}
}
