<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_ADMIN.'/Common.php');
class Applications extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Applications";
		$this->menu = "application";

        $this->lang->load(PATH_TO_ADMIN.'application',$this->language);

		$this->load->model('innovation');
        $this->load->model('mara_approval_status');
        $this->load->model('mara_innovator');
        $this->load->model('mara_evaluation');
        $this->load->model('mara_evaluation_focus');
        $this->load->model('mara_evaluation_criteria');
        $this->load->model('mara_evaluator');
        $this->load->model('mara_evaluation_detail');
        $this->load->model('mara_innovation_business_plan');

		$this->scripts[] = 'administrator/application';
        $this->scripts[] = 'plugins/fancybox/jquery.fancybox.pack';

        $this->styles[] = 'fancybox/jquery.fancybox';
    }

    public function index($generate_excel = 0){
        $this->load->helper('mystring_helper');
        $this->load->model('mara_innovation_evaluator');
        $this->load->model('mara_innovation_category');
        $this->load->model('mara_group_expert');
        
        $categories = unserialize(APPLICATION_CATEGORY);
        $categories_services_area = unserialize(APPLICATION_CATEGORY_SERVICES_AREA);
        $categories_product_area = unserialize(APPLICATION_CATEGORY_PRODUK_AREA);
        $categories_product_level = unserialize(APPLICATION_CATEGORY_PRODUK_LEVEL);

        $innovations = $this->mara_approval_status->find("status = ".INNO_STATUS_SENT_APPROVAL." AND mara_round_id = 1", NULL, NULL, NULL, 'innovation_id');
        $list = array();
        foreach ($innovations as $key => $value) {
            $innovation = $this->innovation->find_one("is_mara_data = ".MARA_FLAG." AND innovation_id = ".$value['innovation_id']);
            $innovation['is_evaluation_done'] = $this->mara_innovation_evaluator->is_evaluation_done($value['innovation_id']);
            $get_evaluation = $this->mara_evaluation->find_one("innovation_id = ".$value['innovation_id']);
            $innovation['has_evaluation'] = ($get_evaluation ? 1 : 0);
            $status = $this->mara_approval_status->get_innovation_status($value['innovation_id']);
            if($status == INNO_STATUS_EVALUATED){
                if(!$innovation['is_evaluation_done']){
                    $status = INNO_STATUS_ASSIGNED_TO_EVALUATOR;
                }
            }

            $stat = $this->mara_approval_status->get_innovation_last_status($value['innovation_id']);
            if($stat['mara_round_id'] > 1){
                $status = INNO_STATUS_APPROVED;
            }
            
            $innovation['status'] = $status;
            $innovation['innovator'] = $this->mara_innovator->get_innovator_name($innovation['analyst_creator_id']);
            
            $innovation['score'] = $this->get_average_score($value['innovation_id']);
            $i_category = $this->mara_innovation_category->find_one("innovation_id = ".$value['innovation_id']);
            if($i_category){
                $innovation['application_category'] = $categories_product_level[$i_category['level']];
                $innovation['category'] = $categories[$i_category['category']];
                $innovation['subcategory'] = $i_category['category'] == 0 ? $categories_services_area[$i_category['area']] : $categories_product_area[$i_category['area']];
            }

            $get_business_plan = $this->mara_approval_status->find_one("status = ".INNO_STATUS_SENT_APPROVAL." AND mara_round_id > 1 AND innovation_id = ".$value['innovation_id'], NULL, NULL, NULL, 'innovation_id');
            $innovation['last_status'] = $this->mara_approval_status->get_innovation_status($value['innovation_id']);
            if($get_business_plan){
                $business_plan = $this->mara_innovation_business_plan->find_one("innovation_id = ".$value['innovation_id']);
                if($business_plan){
                    $innovation['business_plan'] = $business_plan;
                }
            }
            
            if($status != INNO_STATUS_DRAFT){
                $list[] = $innovation;
            }
        }

        if($generate_excel == 0){
            $data['alert'] = $this->session->flashdata('alert');
            $data['innovations'] = $list;
            $data['status'] = unserialize(INNO_STATUS_ADMIN);
            $data_assign['groups'] = $this->mara_group_expert->find_all(); 
            $data['form_assign'] = $this->load->view(PATH_TO_ADMIN.'application/form_assign', $data_assign, TRUE);
            $data['form_bp_eval'] = $this->load->view(PATH_TO_ADMIN.'business_plan/form', NULL, TRUE);

    		$this->load->view(PATH_TO_ADMIN.'application/list', $data);
        }else{
            $contents = $this->generate_innovation_data($list);

            $file_name = "Maratex_2016_Application_List";
            $title = "Maratex 2016 Application List";
            $columns = array("No",lang('innovator'),lang('mara_center'),lang('mara_address'),lang('mara_telp_no'),lang('mara_fax_no'), lang('application_category'), lang('category'),lang('no_kp'),lang('staff_id'),lang('email'),lang('telp_no'),lang('fax_no'),lang('instructor_name'),lang('instructor_staff_id'),lang('instructor_kp_no'),lang('innovation_product'),lang('inspiration'),lang('description'),lang('material'),lang('how_to_use'),lang('special_achievement'),lang('created_date'),lang('discovered_date'),lang('manufacturing_cost'),lang('selling_price'),lang('target'),lang('myipo_protection'),lang('keyword'),lang('team_member')."(".lang('name')."/".lang('no_kp')."/".lang('telp_no').")",lang('heir')."(".lang('name')."/".lang('no_kp')."/".lang('telp_no').")", lang('score'));
            if(count($contents) > 0){
                $fields = array_keys($contents[0]);

                $this->export_to_excel($file_name,$title,$columns,$fields,$contents);
            }else{
                redirect(base_url().PATH_TO_ADMIN.'applications');
            }
        }
    }

    function view_evaluation_list($innovation_id){
        $this->load->model('mara_innovation_evaluator');

        $data['innovation'] = $this->innovation->find_by_id($innovation_id);
        $data['evaluations'] = $this->mara_innovation_evaluator->get_list_evaluation($innovation_id);

        if($data['evaluations'] > 0){
            foreach ($data['evaluations'] as $key => $value) {
                $evaluation = $this->mara_evaluation->find_one("innovation_id = ".$value['innovation_id']." AND mara_evaluator_id = ".$value['mara_evaluator_id']);
                if($evaluation){
                    $data['evaluations'][$key]['evaluation_id'] = $evaluation['id'];
                    $data['evaluations'][$key]['score'] = round($evaluation['total'],1);
                }else{
                    $data['evaluations'][$key]['evaluation_id'] = 0;
                    $data['evaluations'][$key]['score'] = 0;
                }
            }
        }

        $this->load->view(PATH_TO_ADMIN.'application/evaluation_list',$data);
    }

    function view_evaluation($evaluation_id){
        $this->load->model('mara_innovation_category');
        $data['alert'] = $this->session->flashdata('alert');

        $evaluation = $this->mara_evaluation->find_by_id($evaluation_id);
        $data['evaluation_focus'] = $this->get_evaluation_focus($evaluation['innovation_id']);
        $data['innovation'] = $this->innovation->find_by_id($evaluation['innovation_id']);
        $data['innovator'] = $this->mara_innovator->find_by_id($data['innovation']['analyst_creator_id']);
        $data['view_mode'] = 1;
        $evaluator_id = $evaluation['mara_evaluator_id'];
        $data['evaluator'] = $this->mara_evaluator->find_one('evaluator_id = '.$evaluator_id);

        if($evaluation){
            $data['evaluation'] = $evaluation;
            $data['evaluation_detail'] = $this->mara_evaluation_detail->find('mara_evaluation_id = '.$evaluation['id']);
            $evaluator_id = $evaluation['mara_evaluator_id'];
        }

        $this->load->view(PATH_TO_EVALUATOR.'innovation/evaluation_form',$data);
    }

    private function get_evaluation_focus($innovation_id){
        $this->load->model('mara_evaluation_form');

        $category = $this->mara_innovation_category->find_one('innovation_id = '.$innovation_id);
        if($category['category'] == 0){
            $subcategory = $category['area'];
        }else{
            $subcategory = $category['level'];
        }
        $form = $this->mara_evaluation_form->find_one('category = '.$category['category'].' AND subcategory = '.$subcategory);
        $evaluation_focus = $this->mara_evaluation_focus->find('mara_evaluation_form_id = '.$form['id']);
        foreach ($evaluation_focus as $key => $value) {
            $evaluation_focus[$key]['criteria'] = $this->mara_evaluation_criteria->find("mara_evaluation_focus_id = ".$value['id']);
        }

        return $evaluation_focus;
    }

    function view_innovation($id){
        $this->load->model('mara_innovation_category');
        $this->load->model('mara_innovation_team');
        $this->load->model('mara_innovation_info');

        $this->lang->load('innovation',$this->language);

        $data['data_sources'] = unserialize(DATA_SOURCES);
        $data['categories'] = unserialize(APPLICATION_CATEGORY);
        $data['categories_services_area'] = unserialize(APPLICATION_CATEGORY_SERVICES_AREA);
        $data['categories_produk_area'] = unserialize(APPLICATION_CATEGORY_PRODUK_AREA);
        $data['categories_produk_level'] = unserialize(APPLICATION_CATEGORY_PRODUK_LEVEL);
        $data['targets'] = unserialize(INNOVATION_TARGET);
        $data['action_url'] = 'innovations/update';
        $data['alert'] = $this->session->flashdata('alert');

        if($data['innovation'] = $this->innovation->find_by_id($id)){
            $data['i_categories'] = $this->mara_innovation_category->find_one("innovation_id = ".$id);
            $data['i_target'] = ($data['innovation']['target'] != NULL ? json_decode($data['innovation']['target']) : array());
            $data['i_keyword'] = ($data['innovation']['keyword'] != "" ? implode(",",json_decode($data['innovation']['keyword'])) : "");
            $data['i_picture'] = $this->innovation->get_innovation_picture($id);
            $data['i_team'] = $this->mara_innovation_team->find("innovation_id = ".$id);
            $data['i_info'] = $this->mara_innovation_info->find_one("innovation_id = ".$id);
            $data['innovator'] = $this->mara_innovator->get_one_join($data['innovation']['analyst_creator_id']);
            $data['i_status'] = $this->mara_approval_status->get_innovation_last_status($id);
        }

        $data['view_mode'] = true;

        $this->load->view('innovation/form', $data);
    }

    private function get_score($evaluation_id){
        $score = 0;
        $evaluation_detail = $this->mara_evaluation_detail->find("mara_evaluation_id = ".$evaluation_id);

        foreach ($evaluation_detail as $key => $value) {
            $score += $value['point']; 
        }

        return $score;
    }

    private function get_average_score($innovation_id){
        $data_score = $this->mara_evaluation->get_average_score("innovation_id = ".$innovation_id)->row_array();
        
        return $data_score['score'];
    }

    function list_submit_handler(){
        $this->layout = FALSE;

        $postdata = $this->postdata();
        if($postdata['action'] == "proceed"){
            $this->proceed_to_next_round($postdata);
        }

        redirect(base_url().PATH_TO_ADMIN.'applications');
    }

    private function proceed_to_next_round($postdata){
        $this->layout = FALSE;

        foreach ($postdata['ids'] as $key => $value) {
            $mara_round = 1;
            $this->mara_approval_status->insert(array('user_id' => $this->userdata['id'],'innovation_id' => $value, 'mara_round_id' => $mara_round, 'status' => INNO_STATUS_APPROVED));
        }

        $this->session->set_flashdata('alert', "Your data has been updated.");
    }

    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect(base_url().PATH_TO_ADMIN.'applications');
    }

    function assign_evaluation(){
        $this->layout = FALSE;
        $this->load->model("mara_innovation_evaluator");
        $this->load->model("mara_group_evaluator");

        $postdata = $this->postdata();
        $evaluators = $this->mara_group_evaluator->find("mara_group_expert_id = ".$postdata['group']);
        foreach ($evaluators as $evaluator) {
            $row = $this->mara_innovation_evaluator->find_one("innovation_id = ".$postdata['innovation_id']." AND mara_evaluator_id = ".$evaluator['mara_evaluator_id']);
            if(!$row){
                $this->mara_innovation_evaluator->insert(array('innovation_id' => $postdata['innovation_id'], 'mara_evaluator_id' => $evaluator['mara_evaluator_id']));
            }
        }

        //save approval status
        if(count($evaluators) > 0){
            $mara_round = 1;
            $this->mara_approval_status->insert(array('user_id' => $this->userdata['id'],'innovation_id' => $postdata['innovation_id'], 'mara_round_id' => $mara_round, 'status' => INNO_STATUS_ASSIGNED_TO_EVALUATOR));
        }

        $this->session->set_flashdata('alert', "Your data has been updated.");
        redirect(base_url().PATH_TO_ADMIN.'applications');
    }

    private function generate_filename($id, $filename){
        $attachment_name = preg_replace('/\s+/', '_', $filename);
        $attachment_name = str_replace('.', '_', $attachment_name);
        $retval = $id."_".rand()."_".$attachment_name;

        return $retval;
    }

    function generate_pdf($id, $auto_download = TRUE){
        $this->layout = FALSE;
        $this->load->helper(array('dompdf', 'file'));
        $this->load->model('mara_innovation_category');
        $this->load->model('mara_innovation_team');
        $this->load->model('mara_innovation_info');

        $this->lang->load('innovation',$this->language);

        $data['data_sources'] = unserialize(DATA_SOURCES);
        $data['categories'] = unserialize(APPLICATION_CATEGORY);
        $data['categories_services_area'] = unserialize(APPLICATION_CATEGORY_SERVICES_AREA);
        $data['categories_produk_area'] = unserialize(APPLICATION_CATEGORY_PRODUK_AREA);
        $data['categories_produk_level'] = unserialize(APPLICATION_CATEGORY_PRODUK_LEVEL);
        $data['targets'] = unserialize(INNOVATION_TARGET);

        if($data['innovation'] = $this->innovation->find_by_id($id)){
            $data['i_categories'] = $this->mara_innovation_category->find_one("innovation_id = ".$id);
            $data['i_target'] = ($data['innovation']['target'] != NULL ? json_decode($data['innovation']['target']) : array());
            $data['i_keyword'] = ($data['innovation']['keyword'] != "" ? implode(",",json_decode($data['innovation']['keyword'])) : "");
            $data['i_picture'] = $this->innovation->get_innovation_picture($id);
            $data['i_team'] = $this->mara_innovation_team->find("innovation_id = ".$id);
            $data['i_info'] = $this->mara_innovation_info->find_one("innovation_id = ".$id);
        }

        $data['innovator'] = $this->mara_innovator->get_one_join($data['innovation']['analyst_creator_id']);
        
        $filename = $id."_".str_replace(" ","_",$data['innovation']['name_in_melayu']).".pdf";
        $html = $this->load->view('innovation/form_pdf', $data, true);
        
        generate_pdf($html, $filename,$auto_download); 
    }

    /*Temporary function to update evaluation point with weightage*/
    function recalculate_evaluation_form($limit = 100, $offset = 0){
        $this->load->model('mara_evaluation');
        $this->load->model('mara_evaluation_detail');
        $this->load->model('mara_evaluation_focus');
        $this->load->model('mara_evaluation_criteria');

        $evaluations = $this->mara_evaluation->find(NULL,$limit,$offset);
        foreach ($evaluations as $key => $value) {
            $total = 0;
            $details = $this->mara_evaluation_detail->get_join_criteria("mara_evaluation_id = ".$value['id'], NULL, NULL, "mara_evaluation_focus_id");
            foreach ($details as $i => $detail) {
                $get_max_point = $this->mara_evaluation_criteria->get_max_point("mara_evaluation_focus_id = ".$detail['mara_evaluation_focus_id'])->row_array();
                $max_point = $get_max_point['max_point'];
                $get_total_point = $this->mara_evaluation_detail->get_score("mara_evaluation_focus_id = ".$detail['mara_evaluation_focus_id']." AND mara_evaluation_id = ".$value['id'])->row_array();
               
                $total_point = $get_total_point['total'];
                $total_by_focus = $total_point/$max_point*$detail['percentage'];
                $total += $total_by_focus;
            }
            $this->mara_evaluation->update($value['id'], array('total' => round($total,1)));
        }
    }

    private function generate_innovation_data($data){
        $this->load->model('mara_innovation_team');
        $this->load->model('mara_innovation_info');
        $this->lang->load('innovation',$this->language);
        $categories = unserialize(APPLICATION_CATEGORY);
        $categories_services_area = unserialize(APPLICATION_CATEGORY_SERVICES_AREA);
        $categories_produk_area = unserialize(APPLICATION_CATEGORY_PRODUK_AREA);
        $categories_produk_level = unserialize(APPLICATION_CATEGORY_PRODUK_LEVEL);
        $targets = unserialize(INNOVATION_TARGET);

        $contents = array();
        $no = 1; 
        foreach ($data as $key => $row) {
            $value = $this->mara_approval_status->get_innovation_list("innovation.innovation_id = ".$row['innovation_id'])->row_array();
            $contents[$key]['no'] = $no;
            $contents[$key]['innovator'] = $value['innovator_name'];
            $contents[$key]['mara_center'] = $value['mara_center'];
            $contents[$key]['mara_address'] = $value['mara_address'];
            $contents[$key]['mara_telp_no'] = $value['mara_telp_no'];
            $contents[$key]['mara_fax_no'] = $value['mara_fax_no'];
            $contents[$key]['application_category'] = $categories_produk_level[$value['level']];
            $contents[$key]['category'] = $categories[$value['category']]." - ".($value['category'] == 0 ? $categories_services_area[$value['area']] : $categories_produk_area[$value['area']]);
            $contents[$key]['kp_no'] = $value['kp_no'];
            $contents[$key]['staff_id'] = $value['staff_id'];
            $contents[$key]['email'] = $value['email'];
            $contents[$key]['telp_no'] = $value['telp_no'];
            $contents[$key]['fax_no'] = $value['fax_no'];
            $contents[$key]['instructor_name'] = $value['instructor_name'];
            $contents[$key]['instructor_staff_id'] = $value['instructor_staff_id'];
            $contents[$key]['instructor_kp_no'] = $value['instructor_kp_no'];
            $contents[$key]['innovation'] = $value['name_in_melayu'];
            $contents[$key]['inspiration'] = $value['inspiration'];
            $contents[$key]['description'] = $value['description'];
            $contents[$key]['material'] = $value['materials'];
            $contents[$key]['how_to_use'] = $value['how_to_use'];
            $contents[$key]['special_achievement'] = $value['special_achievement'];
            $contents[$key]['created_date'] = $value['created_date'];
            $contents[$key]['discovered_date'] = $value['discovered_date'];
            $contents[$key]['manufacturing_cost'] = "RM ".$value['manufacturing_costs'];
            $contents[$key]['selling_price'] = "RM ".$value['selling_price'];
            $target_arr = ($value['target'] != NULL ? json_decode($value['target']) : array());
            $target = "";
            foreach ($target_arr as $val) {
                $target .= "- ".$targets[$val]."\n";
            }
            $contents[$key]['target'] = $target;
            $contents[$key]['myipo_protection'] = ($value['myipo_protection'] == 1 ? lang('yes') : lang('no'));
            $keyword_arr = ($value['keyword'] != "" ? json_decode($value['keyword']) : array());
            $keyword = "";
            foreach ($keyword_arr as $val) {
                $keyword .= "- ".$val."\n";
            }
            $contents[$key]['keyword'] = $keyword;
            $team_member = $this->mara_innovation_team->find("innovation_id = ".$value['innovation_id']);
            $team_member_str = "";
            foreach ($team_member as $val) {
                $team_member_str = "- ".$val['name']."(".$val['kp_no']."/".$val['telp_no'].")";
            }

            $contents[$key]['team_member'] = $team_member_str;

            $info = $this->mara_innovation_info->find_one("innovation_id = ".$value['innovation_id']);
            $contents[$key]['heir'] = $info['heir_name']."/".$info['heir_kp_no']."/".$info['heir_telp_no'];
            $contents[$key]['score'] = $row['score'];
        $no++;}

        return $contents;
    }

    private function export_to_excel($file_name,$title,$columns,$fields,$contents){
        $this->load->library('PHPExcel');

        if(!$fields) {
            echo 'No result. <a href="'.base_url().PATH_TO_ADMIN.'">Back</a>'; die();
        }
        
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$file_name.'.xls"');
        header('Cache-Control: max-age=0');

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set properties
        $objPHPExcel->getProperties()->setCreator("Maratex2016");
        $objPHPExcel->getProperties()->setLastModifiedBy("Maratex2016");
        $objPHPExcel->getProperties()->setTitle($title);
        $objPHPExcel->getProperties()->setSubject("Maratex2016");
        
        #Column Width
        $objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(25);
        
        #Column Title Style
        $objPHPExcel->getActiveSheet()->getStyle("A1:AE1")->getFont()->setBold(true);
        $objPHPExcel->getActiveSheet()->getStyle("A1:AE1")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        
        #Wrap Text
        $objPHPExcel->getActiveSheet()->getDefaultStyle()->getAlignment()->setWrapText(true); 
        
        // Add some data
        $objPHPExcel->setActiveSheetIndex(0);
        $col = 0;
        $row = 1;

        foreach($columns as $column) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $column);
            $col++;
        }
        $col = 0;
        $row++;
        foreach($contents as $content) {
            foreach($fields as $field) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $content[$field]);
                $col++;
            }
            $col = 0;
            $row++;
        }
        // Rename sheet
        $objPHPExcel->getActiveSheet()->setTitle('Sheet 1');

        // Save it as an excel 2003 file
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        //$objWriter->save("nameoffile.xls");
        $objWriter->save('php://output');
        exit;
    }

    /*end*/
}


