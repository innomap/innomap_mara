<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends CI_Controller {

	function __construct($module = NULL) {
		parent::__construct();

		$this->layout = DEFAULT_LAYOUT;

		$this->language = LANGUAGE_MELAYU;
		$this->submenu = "";

		if($module != "account" && $module != "site"){
			$this->userdata = $this->user_session->get_user();
			if (!$this->userdata) {
				redirect(base_url().'login');
			}	
		}
    }

    function generate_pdf($id, $auto_download = TRUE){
        $this->layout = FALSE;
        $this->load->helper(array('dompdf', 'file'));
        $this->load->model('innovation');
        $this->load->model('mara_innovator');
        $this->load->model('mara_innovation_category');
        $this->load->model('mara_innovation_team');
        $this->load->model('mara_innovation_info');

        $this->lang->load('innovation',$this->language);

        $data['data_sources'] = unserialize(DATA_SOURCES);
        $data['categories'] = unserialize(APPLICATION_CATEGORY);
        $data['categories_services_area'] = unserialize(APPLICATION_CATEGORY_SERVICES_AREA);
        $data['categories_produk_area'] = unserialize(APPLICATION_CATEGORY_PRODUK_AREA);
        $data['categories_produk_level'] = unserialize(APPLICATION_CATEGORY_PRODUK_LEVEL);
        $data['targets'] = unserialize(INNOVATION_TARGET);

        if($data['innovation'] = $this->innovation->find_by_id($id)){
            $data['i_categories'] = $this->mara_innovation_category->find_one("innovation_id = ".$id);
            $data['i_target'] = ($data['innovation']['target'] != NULL ? json_decode($data['innovation']['target']) : array());
            $data['i_keyword'] = ($data['innovation']['keyword'] != "" ? implode(",",json_decode($data['innovation']['keyword'])) : "");
            $data['i_picture'] = $this->innovation->get_innovation_picture($id);
            $data['i_team'] = $this->mara_innovation_team->find("innovation_id = ".$id);
            $data['i_info'] = $this->mara_innovation_info->find_one("innovation_id = ".$id);
        }

        $data['innovator'] = $this->mara_innovator->get_one_join($data['innovation']['analyst_creator_id']);
        
        $filename = $id."_".str_replace(" ","_",$data['innovation']['name_in_melayu']).".pdf";
        $html = $this->load->view('innovation/form_pdf', $data, true);
        
        generate_pdf($html, $filename,$auto_download); 
    }

    function download_pdf($id, $download_id){
    	if($download_id == md5($id."_".$id)){
    		$this->generate_pdf($id);
    	}
    }

    function generate_approval_form_pdf($innovator_id, $innovation_id = 0){
        $this->layout = FALSE;
        $this->load->helper(array('dompdf', 'file'));
        $this->load->model('mara_innovator');
        $this->load->model('innovation');

        $data['innovator'] = $this->mara_innovator->get_one_join($innovator_id);
        if($innovation_id > 0){
            $innovation = $this->innovation->find_by_id($innovation_id);
            $data['innovation'] = $innovation['name_in_melayu'];
        }else{
            $data['innovation'] = "";
        }
        $html = $this->load->view('innovation/approval_form_pdf', $data, true);
        
        generate_pdf($html, $innovator_id."_Pengesahan_PMP.pdf");
    }

    function generate_business_plan_pdf($id, $auto_download = TRUE){
        $this->layout = FALSE;
        $this->load->helper(array('dompdf', 'file'));
        $this->load->model('innovation');
        $this->load->model('mara_innovation_business_plan');
        $this->load->model('mara_innovation_business_plan_picture');

        $business_plan = $this->mara_innovation_business_plan->find_one("id = ".$id);
        if($business_plan){
            $data['business_plan'] = $business_plan;
            $data['innovation'] = $this->innovation->find_one("innovation_id = ".$business_plan['innovation_id']);
            $data['business_plan_picture'] = $this->mara_innovation_business_plan_picture->find('mara_innovation_business_plan_id = '.$business_plan['id']);
        }
        
        $filename = "Business_plan_".$data['innovation']['innovation_id']."_".str_replace(" ","_",$data['innovation']['name_in_melayu']).".pdf";
        $html = $this->load->view('innovation/business_plan_form_pdf', $data, true);
        
        generate_pdf($html, $filename,$auto_download); 
    }
}