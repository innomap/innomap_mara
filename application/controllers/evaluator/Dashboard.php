<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_EVALUATOR.'/Common.php');
class Dashboard extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Dashboard";
		$this->menu = "dashboard";
    }

    public function index(){
		$this->load->view(PATH_TO_EVALUATOR.'dashboard/index');
    }
}

?>