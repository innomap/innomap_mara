<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/'.PATH_TO_EVALUATOR.'/Common.php');
class Innovations extends Common {

	function __construct() {
		parent::__construct();

		$this->title = "Innovations";
		$this->menu = "innovation";

		$this->load->model('innovation');
		$this->load->model('mara_approval_status');
        $this->load->model('mara_innovator');
        $this->load->model('mara_evaluation');
        $this->load->model('mara_evaluation_detail');
        $this->load->model('mara_evaluator');
        $this->load->model('mara_innovation_category');
        $this->load->model('mara_innovation_team');
        $this->load->model('mara_innovation_info');
        $this->load->model('mara_evaluation_focus');
        $this->load->model('mara_evaluation_criteria');

		$this->lang->load('innovation',$this->language);

        $this->scripts[] = 'plugins/fancybox/jquery.fancybox.pack';
        $this->scripts[] = PATH_TO_EVALUATOR.'innovation';

        $this->styles[] = 'fancybox/jquery.fancybox';
    }

    public function index(){
    	$this->load->helper('mystring_helper');
        $this->load->model('mara_innovation_evaluator');
        $this->load->model('mara_evaluation_detail');
    	
    	$innovations = $this->mara_innovation_evaluator->get_list_innovations("mara_evaluator_id = ".$this->userdata['id']." AND is_mara_data = ".MARA_FLAG);

    	$list = array();
    	foreach ($innovations as $key => $value) {
            $status = $this->mara_approval_status->get_innovation_last_status($value['innovation_id'],"mara_evaluation.mara_evaluator_id = ".$this->userdata['id']);
            $status = (count($status) > 0 ? $status['status'] : INNO_STATUS_ASSIGNED_TO_EVALUATOR);
        
            $value['status'] = $status;
            $value['innovator'] = $this->mara_innovator->get_innovator_name($value['analyst_creator_id']);
            $evaluation = $this->mara_evaluation->find_one("innovation_id = ".$value['innovation_id']." AND mara_evaluator_id = ".$this->userdata['id']);
            if($evaluation){
                $value['has_evaluation_id'] = true;
                $value['is_complete'] = $this->mara_evaluation_detail->find_one("mara_evaluation_id = ".$evaluation['id']);
            }else{
                $value['has_evaluation_id'] = false;
                $value['is_complete'] = false;
            }

            if($status != INNO_STATUS_DRAFT){
            	$list[] = $value;
            }
    	}

    	$data['alert'] = $this->session->flashdata('alert');
    	$data['innovations'] = $list;
    	$data['status'] = unserialize(INNO_STATUS_EVALUATOR);
		$this->load->view(PATH_TO_EVALUATOR.'innovation/list',$data);
    }

    function view($id){
        $data['data_sources'] = unserialize(DATA_SOURCES);
        $data['categories'] = unserialize(APPLICATION_CATEGORY);
        $data['categories_services_area'] = unserialize(APPLICATION_CATEGORY_SERVICES_AREA);
        $data['categories_produk_area'] = unserialize(APPLICATION_CATEGORY_PRODUK_AREA);
        $data['categories_produk_level'] = unserialize(APPLICATION_CATEGORY_PRODUK_LEVEL);
        $data['targets'] = unserialize(INNOVATION_TARGET);
        $data['action_url'] = 'innovations/update';
        $data['alert'] = $this->session->flashdata('alert');

        if($data['innovation'] = $this->innovation->find_by_id($id)){
            $data['i_categories'] = $this->mara_innovation_category->find_one("innovation_id = ".$id);
            $data['i_target'] = ($data['innovation']['target'] != NULL ? json_decode($data['innovation']['target']) : array());
            $data['i_keyword'] = ($data['innovation']['keyword'] != "" ? implode(",",json_decode($data['innovation']['keyword'])) : "");
            $data['i_picture'] = $this->innovation->get_innovation_picture($id);
            $data['i_team'] = $this->mara_innovation_team->find("innovation_id = ".$id);
            $data['i_info'] = $this->mara_innovation_info->find_one("innovation_id = ".$id);
            $data['innovator'] = $this->mara_innovator->get_one_join($data['innovation']['analyst_creator_id']);
            $data['i_status'] = $this->mara_approval_status->get_innovation_last_status($id);
        }

        $data['view_mode'] = true;

        $this->load->view('innovation/form', $data);
    }

    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect(base_url().PATH_TO_EVALUATOR.'innovations');
    }

    function view_evaluation($id){
        $this->evaluation($id, 1);
    }

    function evaluation($id, $view_mode = 0){  
        $data['evaluation_focus'] = $this->get_evaluation_focus($id);
        $data['alert'] = $this->session->flashdata('alert');
        $data['innovation'] = $this->innovation->find_by_id($id);
        $data['innovator'] = $this->mara_innovator->find_by_id($data['innovation']['analyst_creator_id']);
        $evaluator_id = $this->userdata['id'];
        $data['evaluator'] = $this->mara_evaluator->find_one('evaluator_id = '.$evaluator_id);
        $data['view_mode'] = $view_mode;
        $evaluation = $this->mara_evaluation->find_one("innovation_id = ".$id." AND mara_evaluator_id = ".$this->userdata['id']);

        if($evaluation){
            $data['evaluation'] = $evaluation;
            $data['evaluation_detail'] = $this->mara_evaluation_detail->find('mara_evaluation_id = '.$evaluation['id']);
            $evaluator_id = $evaluation['mara_evaluator_id'];
        }

        $this->load->view(PATH_TO_EVALUATOR.'innovation/evaluation_form',$data);
    }

    function evaluation_handler(){
        $this->layout = FALSE;

        $postdata = $this->postdata();
        $evaluation_id = $postdata['evaluation_id'];
        $mara_round = 1;

        $data = array('innovation_id' => $postdata['innovation_id'],
                      'mara_evaluator_id' => $this->userdata['id'],
                      'mara_round_id' => $mara_round,
                      'comment' => $postdata['comment'],
                      'total' => $postdata['sum_total']
                      );
        $row = $this->mara_evaluation->find_one("innovation_id = ".$data['innovation_id']." AND mara_evaluator_id = ".$this->userdata['id']);
        if($row){
            $evaluation_id = $row['id'];
        }

        if($evaluation_id > 0){
            if($this->mara_evaluation->update($evaluation_id, $data)){
                if($this->mara_evaluation_detail->delete_by_evaluation($evaluation_id)){
                    $this->insert_evaluation_detail($evaluation_id,$postdata);
                }
            }
        }else{
            $timestamp = date('Y-m-d G:i:s');
            $data['created_at'] = $timestamp;
            if($evaluation_id = $this->mara_evaluation->insert($data)){
               $this->insert_evaluation_detail($evaluation_id,$postdata);
            }
        }

        $get_detail = $this->mara_evaluation_detail->find_one('mara_evaluation_id = '.$evaluation_id);
        if($get_detail){
            $this->mara_approval_status->insert(array('user_id' => $this->userdata['id'],'innovation_id' => $data['innovation_id'], 'mara_round_id' => $mara_round, 'status' => INNO_STATUS_EVALUATED, 'mara_evaluation_id' => $evaluation_id));
            $this->session->set_flashdata('alert', "Your data has been saved.");
            redirect(base_url().PATH_TO_EVALUATOR.'innovations');
        }else{
            $this->session->set_flashdata('alert', "Please complete the evaluation form");
            redirect(base_url().PATH_TO_EVALUATOR.'innovations/evaluation/'.$data['innovation_id']);
        }
    }

    private function insert_evaluation_detail($id, $postdata){
        $evaluation_focus = $this->get_evaluation_focus($postdata['innovation_id']);

        foreach ($evaluation_focus as $key => $value) {
            foreach ($value['criteria'] as $i => $criteria){
                $input = $postdata['input_'.$value['id']."_".$criteria['id']];

                $data_criteria = array('mara_evaluation_id' => $id,'mara_eval_criteria_id' => $criteria['id'], 'point' => $input);
                $this->mara_evaluation_detail->insert($data_criteria);
            }
        }
    }

    private function get_evaluation_focus($innovation_id){
        $this->load->model('mara_evaluation_form');

        $category = $this->mara_innovation_category->find_one('innovation_id = '.$innovation_id);
        if($category['category'] == 0){
            $subcategory = $category['area'];
        }else{
            $subcategory = $category['level'];
        }
        $form = $this->mara_evaluation_form->find_one('category = '.$category['category'].' AND subcategory = '.$subcategory);
        $evaluation_focus = $this->mara_evaluation_focus->find('mara_evaluation_form_id = '.$form['id']);
        foreach ($evaluation_focus as $key => $value) {
            $evaluation_focus[$key]['criteria'] = $this->mara_evaluation_criteria->find("mara_evaluation_focus_id = ".$value['id']);
        }

        return $evaluation_focus;
    }

    /*Temporary function for testing purpose*/
    function test_evaluation_form($id, $view_mode = 0){ 
        $data['evaluation_focus'] = $this->get_evaluation_focus($id);
        $data['alert'] = $this->session->flashdata('alert');
        $data['innovation'] = $this->innovation->find_by_id($id);
        $data['innovator'] = $this->mara_innovator->find_by_id($data['innovation']['analyst_creator_id']);
        $evaluator_id = $this->userdata['id'];
        $data['evaluator'] = $this->mara_evaluator->find_one('evaluator_id = '.$evaluator_id);
        $data['view_mode'] = $view_mode;
        $evaluation = $this->mara_evaluation->find_one("innovation_id = ".$id." AND mara_evaluator_id = ".$this->userdata['id']);

        if($evaluation){
            $data['evaluation'] = $evaluation;
            $data['evaluation_detail'] = $this->mara_evaluation_detail->find('mara_evaluation_id = '.$evaluation['id']);
            $evaluator_id = $evaluation['mara_evaluator_id'];
        }

        $this->load->view(PATH_TO_EVALUATOR.'innovation/evaluation_form',$data);
    }
}

?>