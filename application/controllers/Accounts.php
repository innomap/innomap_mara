<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/Common.php');
class Accounts extends Common {

	function __construct() {
		parent::__construct("account");

		$this->load->model('user');
		$this->load->model('mara_innovator');

		$this->lang->load('account',$this->language);
		$this->menu = "account";
		$this->submenu = "home";	

		$this->scripts[] = 'site/account'; 
    }

    public function index(){
		$this->user = $this->user_session->get_user();
		if ($this->user) {
			redirect(base_url().'innovations');
		}else{
			$this->title = "Home";
			$this->load->view('dashboard/index');
		}
    }

	public function login(){
		$this->title = "Login";
		$data['alert'] = $this->session->flashdata('alert');
		$this->load->view('account/form_login',$data);
	}

/*	function register(){
		$this->title = "Register";
		$this->scripts[] = 'plugins/autocomplete/bootstrap3-typeahead.min';
		$this->styles[] = 'autocomplete/typeahead';

		$data['alert'] = $this->session->flashdata('alert');
		$this->load->view('account/form_register',$data);
	}*/

	function login_auth(){
		$this->layout = FALSE;

		$email = $this->input->post('email');
		$password = $this->input->post('password');

		$this->auth($email, $password);
	}

	private function auth($email, $password){
		$response = array();

		$user = $this->user->auth('email = "'.$email.'" AND is_mara_user = 1 AND (role_id = '.ROLE_MARA_INNOVATOR.' OR role_id = '.ROLE_MARA_EVALUATOR.')');
		if($user != NULL){
			if($this->check_password($user['username'], $password, $user['password'])){
				$data_session = array('id' => $user['user_id'],'username' => $user['username'],'email' => $user['email'], 'role_id' => $user['role_id']);
				
				if($user['role_id'] == ROLE_MARA_INNOVATOR){
					$this->user_session->set_user($data_session);
					redirect(base_url().'accounts');	
				}else{
					$this->user_session->set_evaluator($data_session);
					redirect(base_url().PATH_TO_EVALUATOR.'innovations');
				}
				
			}else{
				$this->session->set_flashdata('alert','The username or password you entered is incorrect.');
				redirect(base_url().'login');
			}
		}else{
			$this->session->set_flashdata('alert','The username or password you entered is incorrect.');
			redirect(base_url().'login');
		}
	}

	private function check_password($username, $password, $hash) {
		$password = $this->user->get_hash($username, $password);
		if($password == $hash){
			return true;
		}else{
			return false;
		}
	}

	function register_handler(){
		$this->layout = FALSE;
		$status = 0;

		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$name = $this->input->post('name');
		$mara_center = $this->input->post('mara_center');
		$mara_address = $this->input->post('mara_address');
		$mara_telp_no = $this->input->post('mara_telp_no');
		$mara_fax_no = $this->input->post('mara_fax_no');
		$staff_id = $this->input->post('staff_id');
		$kp_no = $this->input->post('kp_no');
		$telp_no = $this->input->post('telp_no');
		$fax_no = $this->input->post('fax_no');

		if($email != "" && $name != "" && $mara_center != ""){
			if($this->user->is_email_exist($email)){
				$this->session->set_flashdata('alert','Sorry, your email has been registered.');
			}else{
				$username = $this->generate_username($name);
				$data = array(
					"username" => ($username != "" ? $username : $email),
					"email" => $email,
					"password" => $password,
					"status" => 1,
					"is_mara_user" => 1);
			
				if($id = $this->user->insert_user($data)){
					$innovator = array("id" => $id,
										"name" => $name,
										"mara_center_id" => $mara_center, 
										"mara_address" => $mara_address,
										"mara_telp_no" => $mara_telp_no,
										"mara_fax_no" => $mara_fax_no,
										"staff_id" => $staff_id,
										"kp_no" => $kp_no,
										"telp_no" => $telp_no,
										"fax_no" => $fax_no);
					//save photo
		            if(isset($_FILES['innovator_photo'])){
		                if($_FILES['innovator_photo']['name'] != NULL){
		                    $photo_filename = $this->generate_filename($id,$_FILES['innovator_photo']['name']);
		                    $uploaded_photo = $this->_upload($photo_filename,'innovator_photo',PATH_TO_INNOVATOR_PHOTO, PATH_TO_INNOVATOR_PHOTO_THUMB);
		                
		                    $innovator['photo'] = $uploaded_photo;
		                }
		            }

					if($this->mara_innovator->insert_innovator($innovator)){
						$status = 1;
						$this->auth($email, $password);
					}
				}else{
					$this->session->set_flashdata('alert','Sorry, an error occurred, please try again later.');
				}
			}
		}

		if($status == 0){
			redirect(base_url().'register');
		}
	}

	private function generate_username($name){
		return str_replace(' ', '_', $name).rand();
	}

	public function logout() {
		$this->user_session->clear();
		redirect(base_url().'login');
	}

	function check_email_exist(){
		$this->layout = FALSE;

		$email = $this->input->post('email');
		$id = (isset($_POST['id']) ? $this->input->post('id') : 0);
		if($this->user->is_email_exist($email, $id)){
			$retval = false;
		}else{
			$retval = true;
		}

		echo json_encode(array('valid' => $retval));
	}

	function get_mara_list(){
		$this->layout = FALSE;
		$this->load->model('mara_center');
		$list = array();

		$mara_list = $this->mara_center->find_all();
		foreach ($mara_list as $key => $value) {
			$list[] = array(
					'id' => $value['id'],
					'name' => $value['name']
					);
		}

		echo json_encode($list);
	}

	function check_mara_center_exist(){
		$this->layout = FALSE;
		$this->load->model('mara_center');

		$keyword = $this->input->post('keyword');
		if($this->mara_center->find_one('name = "'.$keyword.'"')){
			$retval = true;
		}else{
			$retval = false;
		}

		echo json_encode(array('valid' => $retval));
	}

	private function _upload($name,$attachment,$upload_path, $thumb_path = NULL) {
        $this->load->library('upload');
        $config['file_name']        = $name;
        $config['upload_path']      = $upload_path;
        $config['allowed_types']    = 'png|jpg|gif|bmp|jpeg';
        $config['remove_spaces']    = TRUE;
        
        $this->upload->initialize($config);
        if(!$this->upload->do_upload($attachment,true)) {
            echo $this->upload->display_errors();
            return false;
        }else{
            $upload_data = $this->upload->data();
            if($thumb_path != NULL){
                $this->create_thumbnail($upload_data['image_width'],$upload_data['image_height'],$upload_path,$upload_data['file_name'], $thumb_path);
            }
            return $upload_data['file_name'];
        }
    }

    private function create_thumbnail($image_width,$image_height,$upload_path,$file_name,$new_path){
        $this->load->library('image_lib');
        
        if ($image_width > 1024 || $image_height > 1024) {
            $config['image_library'] = 'gd2';
            $config['source_image'] = $upload_path.$file_name;
            $config['new_image'] = $new_path.$file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 1024;
            $config['height'] = 1024;

            $this->image_lib->clear();
            $this->image_lib->initialize($config); 

            if ( ! $this->image_lib->resize()){
                echo $this->image_lib->display_errors();
                return false;
            }
        }else{
            $config['image_library'] = 'gd2';
            $config['source_image'] = $upload_path.$file_name;
            $config['new_image'] = $new_path.$file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = $image_width;
            $config['height'] = $image_height;

            $this->image_lib->clear();
            $this->image_lib->initialize($config); 

            if ( ! $this->image_lib->resize()){
                echo $this->image_lib->display_errors();
                return false;
            }
        }
    }

    private function generate_filename($id, $filename){
        $attachment_name = preg_replace('/\s+/', '_', $filename);
        $attachment_name = str_replace('.', '_', $attachment_name);
        $retval = $id."_".$attachment_name;

        return $retval;
    }

    function forgot_password(){
    	$this->title = "Reset Password";

		$data['alert'] = $this->session->flashdata('alert');
		$this->load->view('account/form_forgot_password',$data);
    }

    function forgot_password_handler(){
		$this->load->helper('phpmailer');
		$this->load->model('mara_forgot_password_token');

		$email = $this->input->post('email');

		$user = $this->user->auth('email = "'.$email.'" AND is_mara_user = 1 AND role_id = '.ROLE_MARA_INNOVATOR);
		if(!$user){
			$this->session->set_flashdata('alert','Email is not registered as Maratex2016 user');
			redirect('forgot_password');
		}

		$innovator_name = $this->mara_innovator->get_innovator_name($user['user_id']);
		$token = md5($user['user_id'].date("Y-m-d H:i:s"));
		
		$subject = "Reset your password on Maratex2016";
		$message = "Hi {$innovator_name}, 
                    <br/><br/>
                    We got a request to reset your Maratex2016 password. Click the following link to create a new password : 
                    <br/><br/>
                    ".base_url()."accounts/create_new_password/{$token}
                    <br/><br/><br/>
                    If you ignore this message, your password won't be changed.
                    <br/><br/><br/>
                    Thank you, <br/>
                    Maratex2016 Team";

        if($this->mara_forgot_password_token->insert_token(array('user_id' => $user['user_id'],'token' => $token))){
        	$mail_param = array(
	            'to' => $email,
	            'subject' => $subject,
	            'message' => $message,
	        );
        	$msg_email = phpmailer_send($mail_param, true);
	        $this->session->set_flashdata('alert','Email has been sent to '.$email.'.');
			redirect('forgot_password');
        }
	}

	function create_new_password($token){
		$this->title = "Create New Password";
		$this->load->model('mara_forgot_password_token');

		$data['alert'] = $this->session->flashdata('alert');
		$is_token_exist = $this->mara_forgot_password_token->find_one("token = '".$token."'");
		$data['is_token_exist'] = $is_token_exist;
		$data['token'] = $token;
		$this->load->view('account/form_change_password',$data);
	}

	function create_new_password_handler(){
		$this->load->model('mara_forgot_password_token');
		
		$password = $this->input->post('password');
		$token = $this->input->post('token');
		$data_token = $this->mara_forgot_password_token->find_one("token = '".$token."'");
		$user = $this->user->find_by_id($data_token['user_id']);
		if($this->user->update_password($user['user_id'], array("username" => $user['username'],"password" => $password))){
			$this->mara_forgot_password_token->delete_where("token = '".$token."'");
        	$this->session->set_flashdata('alert','New password has been created. You can login with your new password');
			redirect('login');
        }
	}

    function test_email(){
		$this->load->helper('phpmailer');

		$mail_param = array(
            'to' => "kiki@mobilus-interactive.com",
            'subject' => "Test Maratex2016",
            'message' => "Test email maratex2016",
            'debug' => 1
        );

        $msg_email = phpmailer_send($mail_param, true);
	}
}
