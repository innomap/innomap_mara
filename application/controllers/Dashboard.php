<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct() {
		parent::__construct();

		$this->layout = DEFAULT_LAYOUT;
		$this->load->model('user_session');

		$this->title = "Dashboard";
		$this->menu = "dashboard";
		$this->submenu = "";
		$this->user = $this->user_session->get_user();
		if (!$this->user) {
			redirect(base_url().'login');
		}
    }

    public function index(){
		$this->load->view('dashboard/index');
    }
}
