<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/Common.php');
class Profile extends Common {

	function __construct() {
		parent::__construct();

		$this->load->model('mara_innovator');
        $this->load->model('user');

		$this->title = "Edit Profile";
        $this->menu = "profile";

		$this->lang->load('account',$this->language);

        $this->scripts[] = 'plugins/autocomplete/bootstrap3-typeahead.min';
        $this->scripts[] = 'plugins/fancybox/jquery.fancybox.pack';
        $this->scripts[] = 'site/profile';

        $this->styles[] = 'autocomplete/typeahead';
        $this->styles[] = 'fancybox/jquery.fancybox';
    }

    public function index(){
    	$data['alert'] = $this->session->flashdata('alert');
        $data['innovator'] = $this->mara_innovator->get_one_join($this->userdata['id']);
		$this->load->view('profile/form',$data);
    }

    function save(){
        $this->layout = FALSE;
        $status = 0;

        $user_id = $this->userdata['id'];
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $name = $this->input->post('name');
        $mara_center = $this->input->post('mara_center');
        $mara_address = $this->input->post('mara_address');
        $mara_telp_no = $this->input->post('mara_telp_no');
        $mara_fax_no = $this->input->post('mara_fax_no');
        $staff_id = $this->input->post('staff_id');
        $kp_no = $this->input->post('kp_no');
        $telp_no = $this->input->post('telp_no');
        $fax_no = $this->input->post('fax_no');

        if($email != "" && $name != "" && $mara_center != ""){
            if($this->user->is_email_exist($email, $user_id)){
                $this->session->set_flashdata('alert','Sorry, your email has been registered.');
            }else{
                $data['email'] = $email;
                if($password != ""){
                    $data['username'] = $username;
                    $data['password'] = $password;
                }
            
                if($id = $this->user->update_user($user_id, $data)){
                    $innovator = array("name" => $name,
                                        "mara_center_id" => $mara_center, 
                                        "mara_address" => $mara_address,
                                        "mara_telp_no" => $mara_telp_no,
                                        "mara_fax_no" => $mara_fax_no,
                                        "staff_id" => $staff_id,
                                        "kp_no" => $kp_no,
                                        "telp_no" => $telp_no,
                                        "fax_no" => $fax_no);
                    //save photo
                    if(isset($_FILES['innovator_photo'])){
                        if($_FILES['innovator_photo']['name'] != NULL){
                            if(isset($_POST['h_innovator_photo'])){
                                $this->_remove($this->input->post('h_innovator_photo'), PATH_TO_INNOVATOR_PHOTO);
                            }
                            $photo_filename = $this->generate_filename($id,$_FILES['innovator_photo']['name']);
                            $uploaded_photo = $this->_upload($photo_filename,'innovator_photo',PATH_TO_INNOVATOR_PHOTO, PATH_TO_INNOVATOR_PHOTO_THUMB);
                        
                            $innovator['photo'] = $uploaded_photo;
                        }
                    }

                    if($this->mara_innovator->update($user_id, $innovator)){
                        $this->session->set_flashdata('alert','Data profile has been updated');
                    }
                }else{
                    $this->session->set_flashdata('alert','Sorry, an error occurred, please try again later.');
                }
            }
        }

        redirect(base_url().'profile');
    }

    private function _upload($name,$attachment,$upload_path, $thumb_path = NULL) {
        $this->load->library('upload');
        $config['file_name']        = $name;
        $config['upload_path']      = $upload_path;
        $config['allowed_types']    = 'png|jpg|gif|bmp|jpeg';
        $config['remove_spaces']    = TRUE;
        
        $this->upload->initialize($config);
        if(!$this->upload->do_upload($attachment,true)) {
            echo $this->upload->display_errors();
            return false;
        }else{
            $upload_data = $this->upload->data();
            if($thumb_path != NULL){
                $this->create_thumbnail($upload_data['image_width'],$upload_data['image_height'],$upload_path,$upload_data['file_name'], $thumb_path);
            }
            return $upload_data['file_name'];
        }
    }

    private function create_thumbnail($image_width,$image_height,$upload_path,$file_name,$new_path){
        $this->load->library('image_lib');
        
        if ($image_width > 1024 || $image_height > 1024) {
            $config['image_library'] = 'gd2';
            $config['source_image'] = $upload_path.$file_name;
            $config['new_image'] = $new_path.$file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 1024;
            $config['height'] = 1024;

            $this->image_lib->clear();
            $this->image_lib->initialize($config); 

            if ( ! $this->image_lib->resize()){
                echo $this->image_lib->display_errors();
                return false;
            }
        }else{
            $config['image_library'] = 'gd2';
            $config['source_image'] = $upload_path.$file_name;
            $config['new_image'] = $new_path.$file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = $image_width;
            $config['height'] = $image_height;

            $this->image_lib->clear();
            $this->image_lib->initialize($config); 

            if ( ! $this->image_lib->resize()){
                echo $this->image_lib->display_errors();
                return false;
            }
        }
    }

    private function generate_filename($id, $filename){
        $attachment_name = preg_replace('/\s+/', '_', $filename);
        $attachment_name = str_replace('.', '_', $attachment_name);
        $retval = $id."_".$attachment_name;

        return $retval;
    }

    private function _remove($file_name,$path){
        if (file_exists(realpath(APPPATH . '../'.$path) . DIRECTORY_SEPARATOR . $file_name)) {
            unlink("./".$path."/".$file_name);
        }
    }
}
