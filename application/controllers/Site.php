<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/Common.php');
class Site extends Common {

	function __construct() {
		parent::__construct("site");

		$this->layout = DEFAULT_LAYOUT;
		$this->load->model('user_session');

		$this->title = "Site";
		$this->menu = "site";
    }

    public function index(){
    }

    function about_us(){
    	$this->submenu = "about_us";
    	$this->load->view('site/about_us');
    }

    function news(){
    	$this->submenu = "news";
    	$this->load->view('site/news');
    }

    function competition(){
    	$this->submenu = "competition";
    	$this->load->view('site/competition');
    }

    function faq(){
    	$this->submenu = "faq";
    	$this->load->view('site/faq');
    }

    function contact_us(){
        $this->submenu = "contact_us";
        $this->load->view('site/contact_us');
    }
}
