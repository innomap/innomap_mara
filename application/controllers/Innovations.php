<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(APPPATH . 'controllers/Common.php');
class Innovations extends Common {

	function __construct() {
		parent::__construct();

		$this->load->model('user_session');
		$this->load->model('innovation');
		$this->load->model('mara_innovation_category');
        $this->load->model('mara_approval_status');
        $this->load->model('mara_innovator');
        $this->load->model('mara_innovation_team');
        $this->load->model('mara_innovation_info');

		$this->title = "Innovation";
        $this->menu = "innovation";

		$this->lang->load('innovation',$this->language);

        $this->scripts[] = 'plugins/bootstrap-datepicker/bootstrap-datepicker.min';
        $this->scripts[] = 'plugins/fancybox/jquery.fancybox.pack';
        $this->scripts[] = 'jquery.form';
        $this->scripts[] = 'site/innovation';

        $this->styles[] = 'bootstrap-datepicker/bootstrap-datepicker.min';
        $this->styles[] = 'fancybox/jquery.fancybox';
    }

    public function index(){
    	$this->load->helper("mystring_helper");

    	$data['alert'] = $this->session->flashdata('alert');
        $data['alert_dialog'] = $this->session->flashdata('alert_dialog');
    	$data['innovations'] = $this->innovation->find("analyst_creator_id = ".$this->userdata['id']." AND is_mara_data = ".MARA_FLAG);
        foreach ($data['innovations'] as $key => $value) {
            $stat = $this->mara_approval_status->get_innovation_last_status($value['innovation_id']);
            $data['innovations'][$key]['status'] = $stat['status'];
            $data['innovations'][$key]['round'] = $stat['title'];
            $data['innovations'][$key]['round_id'] = $stat['mara_round_id'];
        }
        $data['status'] = unserialize(INNO_STATUS);
		$this->load->view('innovation/list',$data);
    }

    function add(){
    	$data['data_sources'] = unserialize(DATA_SOURCES);
    	$data['categories'] = unserialize(APPLICATION_CATEGORY);
        $data['categories_services_area'] = unserialize(APPLICATION_CATEGORY_SERVICES_AREA);
        $data['categories_produk_area'] = unserialize(APPLICATION_CATEGORY_PRODUK_AREA);
        $data['categories_produk_level'] = unserialize(APPLICATION_CATEGORY_PRODUK_LEVEL);
    	$data['targets'] = unserialize(INNOVATION_TARGET);
    	$data['action_url'] = 'innovations/store';
        $data['alert'] = $this->session->flashdata('alert');
        $data['innovator'] = $this->mara_innovator->get_one_join($this->userdata['id']);
        $data['view_mode'] = 0;

    	$this->load->view('innovation/form', $data);
    }

    function store(){
        $this->layout = FALSE;

        $postdata = $this->postdata();

        $data_innovation = $this->generate_data_innovation($postdata);
        if($id = $this->innovation->insert($data_innovation)){
            //save approval status
            $mara_round = 1;
            $this->mara_approval_status->insert(array('user_id' => $this->userdata['id'],'innovation_id' => $id, 'mara_round_id' => $mara_round, 'status' => INNO_STATUS_DRAFT));
            
            //save category
            $data_category = $this->generate_data_category($id, $postdata);
            $this->mara_innovation_category->insert($data_category);

            //save team member
            $this->save_team_member($id, $postdata);

            //save info
            if(isset($_FILES['validation_attachment'])){
                if($_FILES['validation_attachment']['name'] != NULL){
                    $attach_filename = $this->generate_filename($id,$_FILES['validation_attachment']['name']);
                    $uploaded_attach = $this->_upload($attach_filename,'validation_attachment',PATH_TO_INNOVATION_ATTACHMENT, NULL, 1);
                    $postdata['str_validation_attachment'] = $uploaded_attach;
                }
            }

            $data_info = $this->generate_data_info($id, $postdata);
            $this->mara_innovation_info->insert($data_info);

            //save picture
            $pict_num = count($_FILES);
            for($i=0;$i<=$pict_num;$i++){
                if(isset($_FILES['picture_'.$i])){
                    if($_FILES['picture_'.$i]['name'] != NULL){
                        $filename         = $this->generate_filename($id, $_FILES['picture_'.$i]['name']);
                        $uploaded_picture = $this->_upload($filename,'picture_'.$i,PATH_TO_INNOVATION_PICTURE,PATH_TO_INNOVATION_PICTURE_THUMB);
                        $this->innovation->add_innovation_picture(array('innovation_id' => $id, 'picture' => $uploaded_picture));
                    }
                }
            }

            redirect(base_url().'innovations/edit/'.$id);
        }
    }

    private function generate_data_innovation($postdata){
        $data_innovation = array("analyst_creator_id" => $this->userdata['id'],
                                        "data_version" => 2,
                                        "name" => $postdata['innovation_name'],
                                        "name_in_melayu" => $postdata['innovation_name'],
                                        "created_date" => $postdata['created_date'],
                                        "discovered_date" => $postdata['discovered_date'],
                                        "inspiration" => $postdata['inspiration'],
                                        "inspiration_in_melayu" => $postdata['inspiration'],
                                        "description" => $postdata['description'],
                                        "description_in_melayu" => $postdata['description'],
                                        "materials" => $postdata['materials'],
                                        "materials_in_melayu" => $postdata['materials'],
                                        "manufacturing_costs" => $postdata['manufacturing_costs'],
                                        "selling_price" => $postdata['selling_price'],
                                        "how_to_use" => $postdata['how_to_use'],
                                        "how_to_use_in_melayu" => $postdata['how_to_use'],
                                        "target" => (isset($postdata['target']) ? json_encode($postdata['target']) : NULL),
                                        "myipo_protection" => $postdata['myipo_protection'],
                                        "special_achievement" => $postdata['special_achievement'],
                                        "special_achievement_in_melayu" => $postdata['special_achievement'],
                                        "keyword" => json_encode(explode(',', $postdata['keyword'])),
                                        "data_source" => 3,
                                        "is_mara_data" => MARA_FLAG
                                        );
        return $data_innovation;
    }

    private function generate_data_category($id, $postdata){
        $data_category = array('innovation_id' => $id,
                                'category' => $postdata['category'],
                                'area' => ($postdata['category'] == 0 ? $postdata['category_service_area'] : $postdata['category_produk_area']),
                                'level' => $postdata['category_produk_level'],
                                'instructor_name' => $postdata['instructor_name'],
                                'instructor_staff_id' => $postdata['instructor_staff_id'],
                                'instructor_kp_no' => $postdata['instructor_kp_no']);

        return $data_category;
    }

    private function generate_data_info($id, $postdata){
        $data_category = array('innovation_id' => $id,
                                'heir_name' => $postdata['heir_name'],
                                'heir_kp_no' => $postdata['heir_kp'],
                                'heir_telp_no' => $postdata['heir_telp'],
                                'validation_attachment' => (isset($postdata['str_validation_attachment']) ? $postdata['str_validation_attachment'] : ''));

        return $data_category;
    }

    private function save_team_member($id, $postdata){
        foreach ($postdata['team_member_id'] as $key => $value) {
            if(isset($postdata['i_deleted_team_member_'.$key])){
                $data_member = array('innovation_id' => $id,
                                'name' => $postdata['team_member_'.$key],
                                'kp_no' => $postdata['team_member_kp_'.$key],
                                'telp_no' => $postdata['team_member_telp_'.$key]);

                if(isset($_FILES['team_member_pic_'.$key])){
                    if($_FILES['team_member_pic_'.$key]['name'] != NULL){
                        if($postdata['h_team_member_pic_'.$key] != ""){
                            $this->_remove($postdata['h_team_member_pic_'.$key],PATH_TO_TEAM_MEMBER_PHOTO);
                            $this->_remove($postdata['h_team_member_pic_'.$key],PATH_TO_TEAM_MEMBER_PHOTO);
                        }
                        $photo_filename = $this->generate_filename($id,$_FILES['team_member_pic_'.$key]['name']);
                        $uploaded_photo = $this->_upload($photo_filename,'team_member_pic_'.$key,PATH_TO_TEAM_MEMBER_PHOTO, PATH_TO_TEAM_MEMBER_PHOTO_THUMB);
                        
                        $data_member['photo'] = $uploaded_photo;
                    }
                }
                if($postdata['team_member_id'][$key] > 0){
                    $this->mara_innovation_team->update($postdata['team_member_id'][$key], $data_member);
                }else{
                    $this->mara_innovation_team->insert($data_member);
                }
            }else{
                if($postdata['team_member_id'][$key] > 0){
                    $team_member = $this->mara_innovation_team->find_by_id($postdata['team_member_id'][$key]);
                    $this->_remove($team_member['photo'],PATH_TO_TEAM_MEMBER_PHOTO);
                    $this->mara_innovation_team->delete($postdata['team_member_id'][$key]);
                }
            }
        }
    }

    private function postdata(){
        if($post = $this->input->post()){
            return $post;
        }
        redirect('innovations');
    }

    private function _upload($name,$attachment,$upload_path, $thumb_path = NULL,$is_file = 0) {
        $this->load->library('upload');
        $config['file_name']        = $name;
        $config['upload_path']      = $upload_path;
        if($is_file == 0){
            $config['allowed_types']    = 'png|jpg|gif|bmp|jpeg';    
        }else{
            $config['allowed_types']    = 'png|jpg|gif|bmp|jpeg|pdf|doc|docx|xls|xlsx';
        }
        
        $config['remove_spaces']    = TRUE;
        
        $this->upload->initialize($config);
        if(!$this->upload->do_upload($attachment,true)) {
            echo $this->upload->display_errors();
            return false;
        }else{
            $upload_data = $this->upload->data();
            if($thumb_path != NULL){
                $this->create_thumbnail($upload_data['image_width'],$upload_data['image_height'],$upload_path,$upload_data['file_name'], $thumb_path);
            }
            return $upload_data['file_name'];
        }
    }
    
    private function _remove($file_name,$path){
        if (file_exists(realpath(APPPATH . '../'.$path) . DIRECTORY_SEPARATOR . $file_name)) {
            unlink("./".$path."/".$file_name);
        }
    }

    private function create_thumbnail($image_width,$image_height,$upload_path,$file_name,$new_path){
        $this->load->library('image_lib');
        
        if ($image_width > 1024 || $image_height > 1024) {
            $config['image_library'] = 'gd2';
            $config['source_image'] = $upload_path.$file_name;
            $config['new_image'] = $new_path.$file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 1024;
            $config['height'] = 1024;

            $this->image_lib->clear();
            $this->image_lib->initialize($config); 

            if ( ! $this->image_lib->resize()){
                echo $this->image_lib->display_errors();
                return false;
            }
        }else{
            $config['image_library'] = 'gd2';
            $config['source_image'] = $upload_path.$file_name;
            $config['new_image'] = $new_path.$file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = $image_width;
            $config['height'] = $image_height;

            $this->image_lib->clear();
            $this->image_lib->initialize($config); 

            if ( ! $this->image_lib->resize()){
                echo $this->image_lib->display_errors();
                return false;
            }
        }
    }

    function edit($id, $view_mode = 0){
        $this->load->model('mara_round');

        $data['data_sources'] = unserialize(DATA_SOURCES);
        $data['categories'] = unserialize(APPLICATION_CATEGORY);
        $data['categories_services_area'] = unserialize(APPLICATION_CATEGORY_SERVICES_AREA);
        $data['categories_produk_area'] = unserialize(APPLICATION_CATEGORY_PRODUK_AREA);
        $data['categories_produk_level'] = unserialize(APPLICATION_CATEGORY_PRODUK_LEVEL);
        $data['targets'] = unserialize(INNOVATION_TARGET);
        $data['action_url'] = 'innovations/update';
        $data['alert'] = $this->session->flashdata('alert');

        if($data['innovation'] = $this->innovation->find_by_id($id)){
            $data['i_categories'] = $this->mara_innovation_category->find_one("innovation_id = ".$id);
            $data['i_target'] = ($data['innovation']['target'] != NULL ? json_decode($data['innovation']['target']) : array());
            $data['i_keyword'] = ($data['innovation']['keyword'] != "" ? implode(",",json_decode($data['innovation']['keyword'])) : "");
            $data['i_picture'] = $this->innovation->get_innovation_picture($id);
            $data['i_team'] = $this->mara_innovation_team->find("innovation_id = ".$id);
            $data['i_info'] = $this->mara_innovation_info->find_one("innovation_id = ".$id);
            $data['i_status'] = $this->mara_approval_status->get_innovation_last_status($id);
        }

        $data['innovator'] = $this->mara_innovator->get_one_join($this->userdata['id']);
        $data['view_mode'] = $view_mode;

        $mara_round = 1;
        $round = $this->mara_round->find_by_id($mara_round);
        $data['days_left'] = ($this->day_countdown($round['deadline']) > 0 ? $this->day_countdown($round['deadline']) : 0);

        $this->load->view('innovation/form', $data);
    }

    function update(){        
        $this->layout = FALSE;

        if(isset($_POST['btn_send_for_approval'])){
            $status = INNO_STATUS_SENT_APPROVAL;
        }else{
            $status = INNO_STATUS_DRAFT;
        }
        $postdata = $this->postdata();

        if($postdata['innovation_name'] != "" || $postdata['inspiration'] != "" || $postdata['description'] != "" || $postdata['created_date'] != "" || $postdata['discovered_date'] != ""){
            $data_innovation = $this->generate_data_innovation($postdata);
            $id = $postdata['id'];
            if($this->innovation->update($postdata["id"], $data_innovation)){
                //save category
                $category = $this->mara_innovation_category->find_one("innovation_id = ".$id);
                $data_category = $this->generate_data_category($id, $postdata);
                $this->mara_innovation_category->update($category['id'],$data_category);

                //save team member
                $this->save_team_member($id, $postdata);

                //save picture
                if(isset($postdata['deleted_picture'])){
                    $deleted_pict = $postdata['deleted_picture'];
                    $deleted_pict_name = $postdata['deleted_picture_name'];
                    foreach ($deleted_pict as $key => $value) {
                        if($this->innovation->delete_innovation_picture($value)){
                            $this->_remove($deleted_pict_name[$key],PATH_TO_INNOVATION_PICTURE);
                            $this->_remove($deleted_pict_name[$key],PATH_TO_INNOVATION_PICTURE_THUMB);
                        }
                    }
                }
                
                //save info
                $postdata['str_validation_attachment'] = $postdata['h_validation_attachment'];
                if(isset($_FILES['validation_attachment'])){
                    if($_FILES['validation_attachment']['name'] != NULL){
                        $this->_remove($postdata['h_validation_attachment'],PATH_TO_INNOVATION_ATTACHMENT);
                        $attachment_name = preg_replace('/\s+/', '_', $_FILES['validation_attachment']['name']);
                        $attach_filename = $id."_".$attachment_name;
                        $uploaded_attach = $this->_upload($attach_filename,'validation_attachment',PATH_TO_INNOVATION_ATTACHMENT, NULL, 1);
                        $postdata['str_validation_attachment'] = $uploaded_attach;
                    }
                }

                $data_info = $this->generate_data_info($id, $postdata);
                $this->mara_innovation_info->update_by_innovation($id, $data_info);

                $pict_num = count($_FILES);
                for($i=0;$i<=$pict_num;$i++){
                    if(isset($_FILES['picture_'.$i])){
                        if($_FILES['picture_'.$i]['name'] != NULL){
                            $picture_name = preg_replace('/\s+/', '_', $_FILES['picture_'.$i]['name']);
                            $filename         = $id."_".$picture_name;
                            $uploaded_picture = $this->_upload($filename,'picture_'.$i,PATH_TO_INNOVATION_PICTURE,PATH_TO_INNOVATION_PICTURE_THUMB);
                            $this->innovation->add_innovation_picture(array('innovation_id' => $id, 'picture' => $uploaded_picture));
                        }
                    }
                }

                if($status == INNO_STATUS_SENT_APPROVAL){
                    $mara_round = 1;
                    $this->mara_approval_status->insert(array('user_id' => $this->userdata['id'],'innovation_id' => $id, 'mara_round_id' => $mara_round, 'status' => INNO_STATUS_SENT_APPROVAL));
                    $this->session->set_flashdata('alert',lang('application_submitted_msg'));
                    $this->session->set_flashdata('alert_dialog',base_url()."innovations/generate_pdf/".$id);
                }else{
                    $this->session->set_flashdata('alert','Application has been updated.');
                }                
            }else{
                $this->session->set_flashdata('alert','Appication cannot be updated.');
            }
        }else{
            $this->session->set_flashdata('alert','Some fields are required.');
        }

        redirect(base_url().'innovations');
    }

    function delete($id){
        $this->layout = FALSE;
        if($this->innovation->delete($id)){
            $this->session->set_flashdata('alert','Innovation has been deleted.');
        }else{
            $this->session->set_flashdata('alert','Innovation can not be deleted.');
        }

        redirect(base_url().'innovations');
    }

    function view($id){
        $this->edit($id, 1);
    }

    private function day_countdown($to){
        $now = time(); // or your date as well
        $to_date = strtotime($to);
        $datediff = $to_date - $now;
        return floor($datediff/(60*60*24));
    }

    private function generate_filename($id, $filename){
        $attachment_name = preg_replace('/\s+/', '_', $filename);
        $attachment_name = str_replace('.', '_', $attachment_name);
        $retval = $id."_".rand()."_".$attachment_name;

        return $retval;
    }

    function business_plan($innovation_id, $view_mode = 0){
        $this->load->model('mara_round');
        $this->load->model('mara_innovation_business_plan');
        $this->load->model('mara_innovation_business_plan_picture');
        $this->load->model('mara_innovation_business_plan_doc');

        $data['alert'] = $this->session->flashdata('alert');
        $data['alert_dialog'] = $this->session->flashdata('alert_dialog');
        $data['action_url'] = "innovations/business_plan_handler";
        $data['view_mode'] = $view_mode;
        $data['round'] = $this->mara_round->find_one("id = 2");
        $data['innovation'] = $this->innovation->find_one("innovation_id = ".$innovation_id);
        $business_plan = $this->mara_innovation_business_plan->find_one("innovation_id = ".$innovation_id);
        if($business_plan){
            $data['business_plan'] = $business_plan;
            $data['business_plan_picture'] = $this->mara_innovation_business_plan_picture->find('mara_innovation_business_plan_id = '.$business_plan['id']);
            $data['business_plan_doc'] = $this->mara_innovation_business_plan_doc->find('mara_innovation_business_plan_id = '.$business_plan['id']);
        }
        $this->load->view('innovation/business_plan_form',$data);
    }

    function business_plan_handler(){
        $this->layout = FALSE;
        $this->load->model('mara_innovation_business_plan');
        $this->load->model('mara_innovation_business_plan_picture');
        $this->load->model('mara_innovation_business_plan_doc');

        $postdata = $this->postdata();
        $data_business_plan = array('innovation_id' => $postdata['innovation_id'],
                                    'description' => $postdata['description'],
                                    'problem' => $postdata['problem'],
                                    'target_user' => $postdata['target_user'],
                                    'target_buyer' => $postdata['target_buyer'],
                                    'manufacturing_cost' => $postdata['manufacturing_cost'],
                                    'competitor' => $postdata['competitor'],
                                    'competitive_advantage' => $postdata['competitive_advantage'],
                                    'impact' => $postdata['impact'],
                                    'business_capital' => $postdata['business_capital'],
                                    'source_of_income' => $postdata['source_of_income'],
                                    'url' => $postdata['url']);

        $is_exist = $this->mara_innovation_business_plan->find_one('innovation_id = '.$postdata['innovation_id']);
        if($postdata['business_plan_id'] == 0 && !$is_exist){
            $data_business_plan['created_at'] = date('Y-m-d H:i:s');
            //add business plan
            $id = $this->mara_innovation_business_plan->insert($data_business_plan);
        }else{
            $id = $postdata['business_plan_id'];
            //edit business plan
            $this->mara_innovation_business_plan->update($id, $data_business_plan);
        }

        if($id != 0){
            //save status
            if(isset($_POST['btn_send_for_approval'])){
                $status = INNO_STATUS_SENT_APPROVAL;
                $allow_submit = false;
                if($postdata['description'] != "" && $postdata['problem'] != "" && $postdata['target_user'] != "" && $postdata['target_buyer'] != "" && $postdata['manufacturing_cost'] != "" && $postdata['competitor'] != "" && $postdata['competitive_advantage'] != "" && $postdata['impact'] != "" && $postdata['business_capital'] != "" && $postdata['source_of_income'] != ""){
                    $allow_submit = true;
                }
            }else{
                $status = INNO_STATUS_DRAFT;
                $allow_submit = true;
            }

            if($allow_submit){
                $get_status = $this->mara_approval_status->find_one("innovation_id = ".$postdata['innovation_id']." AND mara_round_id = ".$postdata['round_id']." AND status = ".$status);
                if(!$get_status){
                    $this->mara_approval_status->insert(array('user_id' => $this->userdata['id'],'innovation_id' => $postdata['innovation_id'], 'mara_round_id' => $postdata['round_id'], 'status' => $status));
                    $this->session->set_flashdata('alert','Business Plan has been saved.');
                    if($status == INNO_STATUS_SENT_APPROVAL){
                        $this->session->set_flashdata('alert',lang('business_plan_submitted_msg'));
                        $this->session->set_flashdata('alert_dialog',base_url()."innovations/generate_business_plan_pdf/".$id);
                    }
                }
            }else{
                $this->session->set_flashdata('alert','Please fill required field.');
            }

            //save picture
            if(isset($postdata['deleted_picture'])){
                $deleted_pict = $postdata['deleted_picture'];
                $deleted_pict_name = $postdata['deleted_picture_name'];
                foreach ($deleted_pict as $key => $value) {
                    if($this->mara_innovation_business_plan_picture->delete($value)){
                        $this->_remove($deleted_pict_name[$key],PATH_TO_INNOVATION_BUSINESS_PLAN_PICTURE);
                        $this->_remove($deleted_pict_name[$key],PATH_TO_INNOVATION_BUSINESS_PLAN_PICTURE_THUMB);
                    }
                }
            }

            $pict_num = count($_FILES);
            for($i=0;$i<=$pict_num;$i++){
                if(isset($_FILES['picture_'.$i])){
                    if($_FILES['picture_'.$i]['name'] != NULL){
                        $filename         = $this->generate_filename($id, $_FILES['picture_'.$i]['name']);
                        $uploaded_picture = $this->_upload($filename,'picture_'.$i,PATH_TO_INNOVATION_BUSINESS_PLAN_PICTURE,PATH_TO_INNOVATION_BUSINESS_PLAN_PICTURE_THUMB);
                        $this->mara_innovation_business_plan_picture->insert(array('mara_innovation_business_plan_id' => $id, 'picture' => $uploaded_picture));
                    }
                }
            }

            //save document
            if(isset($postdata['deleted_doc'])){
                $deleted_doc = $postdata['deleted_doc'];
                $deleted_doc_name = $postdata['deleted_doc_name'];
                foreach ($deleted_doc as $key => $value) {
                    if($this->mara_innovation_business_plan_doc->delete($value)){
                        $this->_remove($deleted_doc_name[$key],PATH_TO_INNOVATION_BUSINESS_PLAN_DOCUMENT);
                    }
                }
            }

            $doc_num = count($_FILES);
            for($i=0;$i<=$doc_num;$i++){
                if(isset($_FILES['doc_'.$i])){
                    if($_FILES['doc_'.$i]['name'] != NULL){
                        $filename         = $this->generate_filename($id, $_FILES['doc_'.$i]['name']);
                        $uploaded_picture = $this->_upload($filename,'doc_'.$i,PATH_TO_INNOVATION_BUSINESS_PLAN_DOCUMENT,NULL, 1);
                        $this->mara_innovation_business_plan_doc->insert(array('mara_innovation_business_plan_id' => $id, 'filename' => $uploaded_picture));
                    }
                }
            }
        }

        redirect(base_url().'innovations');
    }

    function view_business_plan($innovation_id){
        $this->business_plan($innovation_id, 1);
    }

    /*Temporary function for testing purpose*/
    public function list_testing($user_id){
        $this->load->helper("mystring_helper");

        $data['alert'] = $this->session->flashdata('alert');
        $data['alert_dialog'] = $this->session->flashdata('alert_dialog');
        $data['innovations'] = $this->innovation->find("analyst_creator_id = ".$user_id." AND is_mara_data = ".MARA_FLAG);
        foreach ($data['innovations'] as $key => $value) {
            $stat = $this->mara_approval_status->get_innovation_last_status($value['innovation_id']);
            $data['innovations'][$key]['status'] = $stat['status'];
            $data['innovations'][$key]['round'] = $stat['title'];
            $data['innovations'][$key]['round_id'] = $stat['mara_round_id'];
        }
        $data['status'] = unserialize(INNO_STATUS);
        $this->load->view('innovation/list',$data);
    }
}
